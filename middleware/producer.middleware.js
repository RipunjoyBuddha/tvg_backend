const roles = require('../util/roles');

exports.authorize = (req, res, next)=>{

    if(process.env.MODE==='development'){
        return next()
    }
    
    if(!req.session.isLoggedIn || !req.session.user){
        req.flash('errMessage', 'Please login as a producer to continue');
        return res.redirect('/login');
    }
    
    const {role} = req.session.user;
    const chckRole = role.toString() == roles['producer'].toString();
    if(!chckRole){
        req.flash('errMessage', 'Please login as a producer to continue');
        return res.redirect('/login');
    }

    next()
}