const roles = require('../util/roles');
const guestperts = require('../models/guestperts.models');

exports.loggedIn = (req, res, next)=>{

    if(!req.session.isLoggedIn || !req.session.user){
        req.flash('errMessage', 'Please login as a guestpert to continue');
        return res.redirect('/login');
    }
    next()
}

exports.authorize = (req, res, next)=>{
    
    if(!req.session.isLoggedIn|| !req.session.user){
        req.flash('errMessage', 'Please login as a guestpert to continue');
        return res.redirect('/login');
    }
    
    const {role} = req.session.user;
    const chckRole = role.toString() == roles['guestpert'].toString();
    
    if(!chckRole){
        req.flash('errMessage', 'Please login as a guestpert to continue');
        return res.redirect('/login');
    }

    next()
}


exports.AdminGuestpertauthorize = (req, res, next)=>{
    
    if(!req.session.isLoggedIn|| !req.session.user){
        req.flash('errMessage', 'You are not authorized');
        return res.redirect('/login');
    }
    
    const {role} = req.session.user;
    const chckRole = role.toString() == roles['guestpert'].toString()?true:role==roles['subAdmin']?true:role==roles['godMode']?true:false;
    
    if(!chckRole){
        req.flash('errMessage', 'You are not authorized');
        return res.redirect('/login');
    }
    
    next()
}


exports.authorizeRest = (req, res, next)=>{
    
    if(!req.session.isLoggedIn|| !req.session.user){
        return res.status(401).json({msg: 'You are not authorized'});
    }
    
    const {role} = req.session.user;
    const chckRole = role.toString() == roles['guestpert'].toString()?true:role==roles['subAdmin']?true:role==roles['godMode']?true:false;
    if(!chckRole){
        return res.status(401).json({msg: 'You are not authorized'});
    }

    next()
}


exports.dashboardVerification = async (req, res, next)=>{

    try {
        
        if(req.session.verificationStats){
          return next()
        }

        const guestpertId = req.session.user._id;
        const user = await guestperts.fetchAGuestpert(guestpertId);
        if(!user){
            throw new Error('guestpert not found')
        }
        req.session.verificationStats = user.verified;
        next()

    } catch (error) {
        next(error)
    }
}

exports.verification = async (req, res, next)=>{
    try {
        
        const guestpertId = req.session.user._id;
        const user = await guestperts.fetchAGuestpert(guestpertId);
        if(!user){
            throw new Error('guestpert not found')
        }

        if(!user.verified){
            req.session.verificationStats = false;
            return res.redirect('/guestpert/dashboard')
        }
        req.session.verificationStats = true;
        next()

    } catch (error) {
        next (error)
    }
}