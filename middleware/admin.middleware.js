const roles = require('../util/roles');

exports.authorize = (req, res, next)=>{
    
    if(!req.session.isLoggedIn || !req.session.user){
        req.flash('errMessage', 'Please login as a admin to continue');
        return res.redirect('/admin_login');
    }
    
    const {role} = req.session.user;
    let chckRole;
    
    if(role.toString() == roles['subAdmin'].toString() || role.toString() == roles['godMode'].toString()){
        chckRole = true
    }
    if(!chckRole){
        req.flash('errMessage', 'Please login as a admin to continue');
        return res.redirect('/admin_login');
    }

    next()
}

exports.authorizeGodMode = (req, res, next)=>{
    
   

    if(!req.session.isLoggedIn || !req.session.user){
        req.flash('errMessage', 'Please login as a admin to continue');
        return res.redirect('/admin/dashboard');
    }
    
    const {role} = req.session.user;
    const chckRole = role.toString() == roles['godMode'].toString();
    if(!chckRole){
        req.flash('errMessage', 'You are not authorize to visit this part of site');
        return res.redirect('/admin/dashboard');
    }

    next()
}


exports.restAuthorize = (req, res, next)=>{

    if(process.env.MODE==='development'){
        return next()
    }
    
    if(!req.session.isLoggedIn || !req.session.user){
        return res.status(401).json({msg: 'You are not authorized to access'});
    }
    
    const {role} = req.session.user;
    let chckRole;
    
    if(role == roles['subAdmin'] || role == roles['godMode']){
        chckRole = true
    }
    if(!chckRole){
        return res.status(401).json({msg: 'You are not authorized to access'});
    }

    next()
}