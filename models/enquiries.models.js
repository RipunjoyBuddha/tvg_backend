const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'enquiries'
const roles = require('../util/roles')

class enquiries {

    constructor({ media, show, topic, airDate, location, rating, guestpertId, producerId }) {
        this.media = media;
        this.show = show;
        this.topic = topic;
        this.location = location;
        this.airDate = new Date(airDate);
        this.rating = rating;
        this.guestpertId = mongo.ObjectID(guestpertId);
        this.producerId = mongo.ObjectID(producerId);
        this.unread = 1;
        this.guestpertView = true;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    // Used for producer pannel 
    static async getEnquiriesOfProducers(pageNum, guestpertId, producerId) {

        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { guestpertId: mongo.ObjectID(guestpertId), producerId: mongo.ObjectID(producerId) }
                },
                {
                    $project: {
                        media: 1,
                        show: 1,
                        topic: 1,
                        rating: 1,
                        airDate: 1,
                        location: 1,
                        createdAt: 1
                    }
                },
                {
                    $sort: {
                        createdAt: -1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray()

            return res

        } catch (err) {
            throw err;
        }
    }

    static async getCountForProducerPannel(guestpertId, producerId) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ guestpertId: mongo.ObjectID(guestpertId), producerId: mongo.ObjectID(producerId) }).count()
            return res;

        } catch (err) {
            throw err;
        }
    }


    /***** Inquiries Admin Pannel *****/
    static async getEnquiriesGuestpertsForAdminPannel(pageNum, keyword) {

        try {
            const db = getDb();

            const objPerPage = 24;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        role: roles.guestpert
                    }
                },
                {
                    $project: {
                        unreadInquiries:1,
                        imgSrc: 1,
                        fullName: {
                            $concat: [
                                '$firstName', ' ',
                                '$lastName'
                            ]
                        },
                        title: 1
                    }
                },
                {
                    $match: {
                        'fullName': { $regex: keyword, $options: 'i' }
                    }
                },
                {
                    $sort: {
                        unreadInquiries: -1,
                        fullName: 1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray()

            return res;

        } catch (err) {
            throw err;
        }
    }

    

    static async getEnquiriesForAdminPannel(pageNum, guestpertId) {

        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { guestpertId: mongo.ObjectID(guestpertId) }
                },
                {
                    $project: {
                        media: 1,
                        show: 1,
                        topic: 1,
                        rating: 1,
                        airDate: 1,
                        location: 1,
                        guestpertView: 1,
                        createdAt: 1
                    }
                },
                {
                    $sort: {
                        createdAt: -1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray()

            return res

        } catch (err) {
            throw err;
        }
    }


    static async getCountForGuestpertPannel(guestpertId) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ guestpertId: mongo.ObjectID(guestpertId) }).count()
            return res

        } catch (err) {
            throw err;
        }
    }

    /**** Make unread 0 ****/
    static async setToRead(guestpertId) {

        try {
            const db = getDb();
            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guestpertId) }, { $set: { unreadInquiries: 0 } })
            
            return res

        } catch (err) {
            throw err;
        }
    }
    /**************************************************/

    static async updateEnquiry(id, obj) {

        try {
            const db = getDb();

            const res = db.collection(dbName).updateOne({ _id: mongo.ObjectID(id) }, { $set: obj })
            return res

        } catch (err) {
            throw err;
        }
    }


    static async getEnquiryById(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).findOne({ _id: mongo.ObjectID(id) })
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async getEnquiriesByGuestpertId(id, pageNum) {

        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = db.collection(dbName).find({ guestpertId: mongo.ObjectID(id), guestpertView: true })
                .project({
                    media: 1,
                    show: 1,
                    topic: 1,
                    rating: 1,
                    rating: 1,
                    airDate: 1,
                    createdAt: 1
                })
                .sort({ createdAt: -1 })
                .skip(leave)
                .limit(objPerPage)
                .toArray()
            return res

        } catch (err) {
            throw err;
        }
    }

    static async getCountEnquiriesGuestpertId(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).find({ guestpertId: mongo.ObjectID(id), guestpertView: true }).count()
            return res

        } catch (err) {
            throw err;
        }
    }

}
module.exports = enquiries;