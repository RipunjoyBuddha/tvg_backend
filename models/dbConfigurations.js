const getDb = require('../util/database').getDb;

const configure = async () => {

    try {
        const db = getDb();
        console.log('configure database started')
        await db.collection('tempLinks').createIndex({ createdAt: 1 }, { expireAfterSeconds: 600 });
        await db.collection('tempOrders').createIndex({ createdAt:1 }, {expireAfterSeconds: 60*60})
        await db.collection('admin').createIndex({ userName: 1 }, { unique: true });
        await db.collection('hotTopicCategory').createIndex({ title: 1 }, { unique: true });
        await db.collection('newsletterSubscribers').createIndex({ email: 1 }, { unique: true });
        await db.collection('settings').createIndex({ title: 1 }, { unique: true });
        await db.collection('tags').createIndex({ keyword: 1 }, { unique: true });
        await db.collection('users').createIndex({ email: 1 }, { unique: true });
        await db.collection('users').createIndex({ username: 1 }, { unique: true });
        await db.collection('users').createIndex({ membershipOrder: 1 });
        console.log('configure finished')

    } catch (err) {
        throw err;
    }
}

module.exports = configure;