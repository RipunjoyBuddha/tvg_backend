const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'orders';

class orders {

    constructor({title, amount, paymentStatus, purchaseDate, address, userId, imgSrc}) {
        this.title = title;
        this.amount = amount;
        this.paymentStatus = paymentStatus;
        this.purchaseDate = purchaseDate;
        this.address = address;
        this.userId = mongo.ObjectID(userId);
        this.imgSrc = imgSrc;
        this.createdAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchOrderByUserId(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ userId: mongo.ObjectID(id) }).sort({createdAt:-1}).toArray()
            return res

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllLatestOrders( ) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).find({}).sort({createdAt:-1}).toArray()
            return res;
    
        } catch (err) {
                throw err;
            }
        }
    
}
module.exports = orders;