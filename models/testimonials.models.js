const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'testimonials';

class testimonials {

    constructor({ imgArr, imageUrl, role, name, title, content, active, vidLink }) {
        this.name = name;
        this.imgSrc = imageUrl;
        this.title = title;
        this.role = role;
        this.content = content;
        this.active = active ? true : false;
        this.vidLink = vidLink;
        this.imgArr = imgArr;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection('testimonials').insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async updateTestimonial(id, obj) {

        try {
            const db = getDb();

            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }

            const res = await db.collection('testimonials').updateOne(
                {
                    _id: mongo.ObjectID(id)
                },
                {
                    $set: finalObj
                }
            );

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchATestimonial(id) {

        try {
            const db = getDb();

            const res = await db.collection('testimonials')
                .findOne({ _id: mongo.ObjectID(id) })

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async removeTestimonial(id) {

        try {
            const db = getDb();

            const res = await db.collection('testimonials').deleteOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllTestimonials(pageNum = 1) {

        try {
            const db = getDb();
            const objPerPage = 7;
            const leave = (+pageNum - 1) * objPerPage;

            const result = await db.collection('testimonials')
                .find({})
                .sort({ updatedAt: -1 })
                .skip(leave)
                .limit(objPerPage)
                .project({
                    name: 1,
                    title: 1,
                    role: 1,
                    content: 1,
                    imgSrc: 1,
                    vidLink: 1,
                }).toArray()

            return result;

        } catch (err) {
            throw err;
        }
    }

    // GuestpertTestimonial for Home page
    static async fetchTestimonialBykeywords(pageNum, keyword = '') {

        try {
            const db = getDb();

            const objPerPage = 8;
            const leave = (+pageNum - 1) * objPerPage;

            if (!keyword) { keyword = '' };

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { imgArr: { $size: 0 }, vidLink: { $size: 0 }, active: true, role: "10101011001010", name: { $regex: keyword, $options: 'i' } }
                },
                {
                    $project: {
                        imgSrc: 1,
                        fullName: '$name',
                        content: 1,
                        title: 1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async homepageTestimonialCount() {

        try {
            const db = getDb();

            const res = db.collection(dbName).find({ imgArr: { $size: 0 }, vidLink: { $size: 0 }, active: true, role: "10101011001010" }).count()
            return res

        } catch (err) {
            throw err;
        }
    }


    static async fetchProducerTestimonialBykeywords(pageNum, keyword = '') {

        try {
            const db = getDb();

            const objPerPage = 16;
            const leave = (+pageNum - 1) * objPerPage;

            if (!keyword) { keyword = '' };

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { imgArr: { $size: 0 }, vidLink: { $size: 0 }, active: true, role: "10111010000101", name: { $regex: keyword, $options: 'i' } }
                },
                {
                    $project: {
                        imgSrc: 1,
                        fullName: '$name',
                        content: 1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async getProducerTestimonialCount() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ imgArr: { $size: 0 }, vidLink: { $size: 0 }, active: true, role: "10111010000101" }).count()
            return res

        } catch (err) {
            throw err;
        }
    }

    static async testimonialsCount() {

        try {
            const db = getDb();

            const result = await db.collection('testimonials').find().count();
            return result;

        } catch (err) {
            throw err;
        }
    }

    // For frontent page

    static async fetchActiveTestimonials(pageNum) {

        try {
            const db = getDb();
            const objPerPage = 7;
            const leave = (+pageNum - 1) * objPerPage;

            const result = await db.collection('testimonials')
                .find({active:true})
                .sort({ updatedAt: -1 })
                .skip(leave)
                .limit(objPerPage)
                .project({
                    name: 1,
                    title: 1,
                    role: 1,
                    content: 1,
                    imgSrc: 1,
                    vidLink: 1,
                }).toArray()
            return result;

        } catch (err) {
            throw err;
        }
    }

    static async fetchActiveTestimonialsCount() {

        try {
            const db = getDb();

            const result = await db.collection('testimonials').find({ active: true }).count();
            return result;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = testimonials;