const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'newsletterSubscribers';

class newsletterSubscribers {

    constructor(email) {
        this.email = email;
        this.activeStats = true;
        this.createdAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchSubscriber(email) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ email });
            return res;

        } catch (err) {
            throw err;
        }
    }


    // Used to send templates to subscribers
    static async fetchAllActiveSubs( ) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).find({activeStats: true}).project({email:1, _id:0}).toArray();
            return res
    
        } catch (err) {
                throw err;
            }
        }
}
module.exports = newsletterSubscribers;