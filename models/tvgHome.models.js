const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'tvgHome';

class tvgHome {

    static async fetchAllSlides() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({}).project({ _id: 0, slides: 1 }).toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllCompanyTags() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({}).project({ _id: 0, companyTags: 1 }).toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchTagLines() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({})
                .project({
                    _id: 0,
                    companyTags: 1,
                    upcomingAppearanceTag: 1,
                    hotTopicTag: 1
                })
                .toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchGuestpertsOnHome() {

        try {
            const db = getDb();
            const res = await db.collection(dbName).find({})
                .project({
                    _id: 0,
                    homepageGuestperts: 1
                })
                .toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchHotTopicsOnHome() {

        try {
            const db = getDb();
            const res = await db.collection(dbName).find({})
                .project({
                    _id: 0,
                    homepageHotTopics: 1
                })
                .toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchTestimonialsOnHome() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({})
                .project({
                    _id: 0,
                    homepageTestimonials: 1
                }).toArray();

            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchBooksOnHome() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({})
                .project({
                    _id: 0,
                    homepageBooks: 1
                }).toArray();

            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchHomePage( ) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).findOne({});
            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async updateHome(obj) {

        try {
            const db = getDb();
            const finalObj = {
                ...obj,
                updatedAt: new Date().toDateString()
            }

            const res = db.collection(dbName).updateOne({}, { $set: finalObj });
            return res

        } catch (err) {
            throw err;
        }
    }

    static async addGuestpertToHome(obj) {

        try {
            const db = getDb();
            const res = db.collection(dbName).updateOne({}, { $push: { homepageGuestperts: obj } })
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async addTestimonialsToHome(obj) {

        try {
            const db = getDb();

            const res = db.collection(dbName).updateOne({}, { $push: { homepageTestimonials: obj } });
            return res;

        } catch (err) {
            throw err;
        }
    }
    static async addProductsToHome(obj) {

        try {
            const db = getDb();

            const res = db.collection(dbName).updateOne({}, { $push: { homepageBooks: obj } });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async addTopicsToHome(obj) {

        try {
            const db = getDb();
            const res = db.collection(dbName).updateOne({}, { $push: { homepageHotTopics: obj } })
            return res;

        } catch (err) {
            throw err;
        }
    }
}
module.exports = tvgHome;