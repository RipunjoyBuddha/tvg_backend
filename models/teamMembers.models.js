const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

// ----- Creating TVG Team Members ----- //
class createTeam {
    constructor(firstName, lastName, title, memberType, description = 'null', imageUrl = 'null') {

        this.firstName = firstName;
        this.lastName = lastName;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.memberType = memberType;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        const db = getDb();
        // console.log(this)
        try {
            const res = await db.collection('tvgMembers').insertOne(this);
            return res;
        }
        catch (err) {
            throw err
        }
    }


    static async fetchMember(id) {
        const db = getDb();

        // Fetch all details of the member

        try {

            const res = await db.collection('tvgMembers').findOne({ _id: mongo.ObjectID(id)});
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getAllMember() {
        const db = getDb();

        try {

            const res = await db.collection('tvgMembers').find({}).project({createdAt: 0, updatedAt: 0}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    
    static async deleteMember(id) {
        const db = getDb();
        
        // Delete member from database
        try {
            
            const res = await db.collection('tvgMembers').deleteOne({_id: mongo.ObjectID(id)});
            return res;
            
        } catch (err) {
            
            throw err;
        }
    }


    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString()
            };
            for (const c in obj) {
                if (obj[c]) {
                    updateObj[c] = obj[c];
                }
            }

            // Make update in DB
            const res = await db.collection('tvgMembers').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = createTeam;