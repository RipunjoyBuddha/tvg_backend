const getDb = require('../util/database').getDb;

const mongo = require('mongodb');
const uniqid = require('uniqid')

class founder {

    constructor() {
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    static async createAppearance(obj) {

        try {
            const db = getDb();

            const finalObj = {
                id: uniqid(),
                ...obj
            }

            const res = db.collection('founders').updateOne({ _id: mongo.ObjectID('5f87191b804f27014fc11576') }, {
                $push: { appearances: finalObj }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getFounder() {

        try {
            const db = getDb();

            const res = await db.collection('founders').aggregate([
                {
                    $lookup: {
                        from: 'blogs',
                        localField: 'blogs',
                        foreignField: '_id',
                        as: 'blog'
                    }
                }
            ]).toArray()
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchFounderImgArr(id) {
    
        try {
            const db = getDb();
    
            const res = await db.collection('founders').find({})
                .project({imgArr:1, _id: 0})
                .toArray()

                return res[0]
    
        } catch (err) {
                throw err;
            }
        }

    static async editAppearance(id, obj) {

        try {
            const db = getDb();
            const res = db.collection('founders').updateOne(
                {
                    'appearances.id': id
                },
                {
                    $set: {
                        'appearances.$.videoLink': obj.videoLink,
                        'appearances.$.videoTitle': obj.videoTitle,
                        updatedAt: new Date().toISOString()
                    }
                }
            )
            return res;


        } catch (err) {
            throw err;
        }
    }

    static async fetchAppearances() {

        try {
            const db = getDb();

            const res = await db.collection('founders')
                .find()
                .project({ appearances: 1, _id: 0 })
                .toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async deleteAppearance(arr) {

        try {
            const db = getDb();

            const res = await db.collection('founders')
                .updateOne(
                    { _id: mongo.ObjectID('5f87191b804f27014fc11576') },
                    { $set: { appearances: arr } }
                )

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getFounderDetails() {

        try {
            const db = getDb();

            const res = await db.collection('founders')
                .find()
                .project({ details: 1, _id: 0 })
                .toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async createFounderDetails(obj) {

        try {
            const db = getDb();

            const finalObj = {
                id: uniqid(),
                ...obj
            }

            const res = db.collection('founders').updateOne({ _id: mongo.ObjectID('5f87191b804f27014fc11576') }, {
                $push: { details: finalObj }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async editFounderDetails(finalObj) {

        try {
            const db = getDb();

            const res = await db.collection('founders').updateOne({},
                {
                    $set: finalObj
                }
            )
            return res;
            
        } catch (err) {
            throw err;
        }
    }

    static async fetchDetails() {

        try {
            const db = getDb();

            const res = await db.collection('founders')
                .find()
                .project({ details: 1, _id: 0 })
                .toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async deleteDetail(arr) {

        try {
            const db = getDb();

            const res = await db.collection('founders')
                .updateOne(
                    { _id: mongo.ObjectID('5f87191b804f27014fc11576') },
                    { $set: { details: arr } }
                )

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async createBlog(blogId) {

        try {
            const db = getDb();

            const res = db.collection('founders').updateOne({ _id: mongo.ObjectID('5f87191b804f27014fc11576') }, {
                $push: { blogs: blogId }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getBlogsArr() {

        try {
            const db = getDb();

            const blogsArr = await db.collection('founders').find().project({ blogs: 1 }).toArray()
            return blogsArr

        } catch (err) {

            throw err;
        }
    }


    static async getBlogsCount() {

        try {
            const db = getDb();

            const res = await db.collection('blogs').find({ authorId: 'jacquie jordan' }).count()
            return res

        } catch (err) {

            throw err;
        }
    }

    static async editBlogs(arr) {

        try {
            const db = getDb();

            const res = await db.collection('founders').updateOne({},
                {
                    $set: { blogs: arr }
                }
            )

            return res;

        } catch (err) {

            throw err;
        }
    }

    static async lookupBlogs(pageNum) {

        try {

            const db = getDb();
            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const result = await db.collection('founders').aggregate([
                {
                    $lookup: {
                        from: 'blogs',
                        localField: 'blogs',
                        foreignField: '_id',
                        as: 'allBlogs'
                    }
                },
                {
                    $project: {
                        _id: 0,
                        allBlogs: 1
                    }
                },
                {
                    $unwind: '$allBlogs'
                },
                {
                    $project: { allBlogs: 1, _id: 0 }
                },
                {
                    $sort: { 'allBlogs.updatedAt': -1 }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }

            ]).toArray();

            return result;

        } catch (err) {
            return err
        }
    }
}
module.exports = founder;