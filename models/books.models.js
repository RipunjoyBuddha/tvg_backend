const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'books';

class books {

    constructor({ title, description, published, active, authorId, imageUrl }) {
        this.title = title;
        this.published = published;
        this.description = description;
        this.active = active ? true : false;
        this.authorId = authorId;
        this.imageUrl = imageUrl;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async editBook(id, obj) {
    
        try {
            const db = getDb();
    
            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }

            const res  = await db.collection(dbName).updateOne({_id: mongo.ObjectID(id)}, {$set: finalObj});
            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchABook(bookId) {

        try {
            const db = getDb();
            const res = await db.collection(dbName).findOne({ _id: mongo.ObjectID(bookId) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllBooks(pageNum, keywords) {

        try {

            const db = getDb();
            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { 'title': { $regex: keywords, $options: 'i' } }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }

            ]).toArray();

            return res;


        } catch (err) {
            throw err;
        }
    }

    static async allBooksCount( ) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).find().count()
            return res
    
        } catch (err) {
                throw err;
            }
        }

    
    static async deleteBook(bookId) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).deleteOne({ _id: mongo.ObjectID(bookId) });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAuthorBooks(authorId) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ authorId: mongo.ObjectID(authorId) }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }
}
module.exports = books;