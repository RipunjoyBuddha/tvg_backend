const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class tags {

    constructor(tag, active){
        this.keyword = tag.toLowerCase(),
        this.active = active
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{

            const res = await db.collection('tags').insertOne(this);
            return res;

        }catch(err){
            throw err
        }
    }


    static async fetchParticularTags(pageNum, keywords){
        
        try{
            const db = getDb();
            const objPerPage = 24;
            const leave = ( +pageNum - 1)* objPerPage;

            const res = await db.collection('tags').aggregate([
                {
                    $match: {'keyword': {$regex: keywords , $options: 'i'}}
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
                
            ]).toArray();

            return res;

        }catch(err){
            throw err;
        }
    }

    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            
            const res = await db.collection('tags').updateOne(
                { _id: mongo.ObjectID(id) }, 
                { $set: updateObj }
            );
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async getCount(){

        try{
            const db = getDb();

            const res = await db.collection('tags')
            .find({}).count();
            
            return res;
        }
        catch(err){
            throw err
        }
    }

    static async delete(id){

        try{
            const db = getDb();

            const res = await db.collection('tags')
            .deleteOne({_id: mongo.ObjectID(id)});
            
            return res;
        }
        catch(err){
            throw err
        }
    }



    static async fetchTag(tag){
        const db = getDb();
        
        // Fetch all details of the member
        try {
            const res = await db.collection('tags').findOne({ keyword: tag});
            return res;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = tags