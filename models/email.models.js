const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class faq {

    constructor(segment, subject, emailContent){
        this.segment = segment;
        this.subject = subject;
        this.emailContent = emailContent;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{

            const res = await db.collection('email').insertOne(this);
            return res;

        }catch(err){
            throw err
        }
    }

    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('email').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAEmail(id){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('email').findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchEmailsWithId(idArr) {
    
        try {
            const db = getDb();
    
            const finalArr = idArr.map(c=>({_id: mongo.ObjectID(c)}));

            const res = await db.collection('email').find({$or: finalArr}).toArray();
            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchAllEmail(){

        const db = getDb();

        // Fetch all details of the member

        try {

            // const res = await db.collection('tvgMembers').find({}).project({description:0, createdAt: 0, updatedAt: 0}).toArray();
            const res = await db.collection('email').find({}).sort({createdAt:-1}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = faq;