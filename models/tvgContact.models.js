const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'tvgContacts';

class tvgContact {

    constructor() {
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    static async addTestimonialToContact(obj) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).updateOne({}, {
                $push: { testimonials: obj }
            });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getContactPageImage() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({})
                .project({ imageUrl: 1 })
                .toArray()

            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async updateContactPage(obj) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).updateOne({}, { $set: obj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getContactPage() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({})
                .toArray()

            return res[0];

        } catch (err) {
            throw err;
        }
    }
}
module.exports = tvgContact;