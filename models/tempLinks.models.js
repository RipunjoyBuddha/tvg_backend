const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'tempLinks';

class tempLinks {

    constructor({ user, hash, firstName='', email='' , lastName='', username='', role='' }) {
        this.user = user;
        this.role=role;
        this.hash = hash;
        this.email = email;
        this.firstName = firstName,
        this.lastName = lastName,
        this.username = username
        this.createdAt = new Date();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchHash(hash) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ hash });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delLink(id) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).deleteOne({_id: mongo.ObjectID(id)});
            return res;
    
        } catch (err) {
                throw err;
            }
        }
}
module.exports = tempLinks;