const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'users';

class producers {

    constructor({ firstName, lastName, prefix = '', imgSrc = '/images/user.png', suffix = '', title = '', address = '', phone = '', mobile = '', company = '', website = '', knowingUs = '', bookingMandate = '', fax = '', username, email, password, verified = false }) {
        this.role = "10111010000101";
        this.firstName = firstName;
        this.lastName = lastName;
        this.prefix = prefix;
        this.suffix = suffix;
        this.title = title;
        this.address = address;
        this.phone = phone;
        this.mobile = mobile;
        this.company = company;
        this.imgSrc = imgSrc;
        this.website = website;
        this.fax = fax;
        this.username = username;
        this.knowingUs = knowingUs;
        this.bookingMandate = bookingMandate;
        this.email = email;
        this.password = password;
        this.verified = verified;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }


    // Used to find if such user exist
    static async fetchProducerByUsername(username) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ username })
            return res

        } catch (err) {
            throw err;
        }
    }

    static async fetchProducerByEmail(email) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ email })
            return res

        } catch (err) {
            throw err;
        }
    }


    // ---- Fetch producer for Admin Pannel and ProducerPannel---- //
    static async fetchProducerById(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ _id: mongo.ObjectID(id) })
            return res

        } catch (err) {
            throw err;
        }
    }


    // ---- Fetch all producers for Admin Pannel---- //
    static async fetchProducerAllInfo(pageNum, keyword) {

        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection(dbName).aggregate([
                {
                    $match: {
                        role: "10111010000101"
                    }
                },
                {
                    $project: {
                        fullName: { $concat: ["$firstName", " ", "$lastName"] },
                        firstName: 1,
                        lastName: 1,
                        prefix: 1,
                        suffix: 1,
                        imgSrc: 1,
                        title: 1,
                        role: 1,
                        address: 1,
                        phone: 1,
                        mobile: 1,
                        company:1,
                        username: 1,
                        email: 1,
                        createdAt: 1,
                        fax: 1,
                        website: 1
                    }
                },
                {
                    $match: { 'fullName': { $regex: keyword, $options: 'i' } }
                },
                {
                    $sort: {
                        lastName: 1,
                        firstName: 1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray()


            return res;

        } catch (err) {
            throw err;
        }
    }


    static async countTotal(keyword) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).aggregate([
                {
                    $match: {
                        role: "10111010000101"
                    }
                },
                {
                    $project: {
                        fullName: { $concat: ["$firstName", " ", "$lastName"] }
                    }
                },
                {
                    $match: { 'fullName': { $regex: keyword, $options: 'i' } }
                }
            ]).toArray()
            return res.length

        } catch (err) {
            throw err;
        }
    }


    static async updateProducer(id, obj) {

        try {
            const db = getDb();

            const finalObj = { ...obj, updatedAt: new Date().toISOString() }

            const res = db.collection(dbName).updateOne({ _id: mongo.ObjectID(id) }, { $set: finalObj })
            return res

        } catch (err) {
            throw err;
        }
    }

    static async deleteProducer(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).deleteOne({ _id: mongo.ObjectID(id) })
            return res

        } catch (err) {
            throw err;
        }
    }

}



module.exports = producers;