const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class products {

    constructor(imageUrl, { title, thirdpartyUrl, paymentMode, productType, description, active, bestSeller, currency, price, audioLink }) {
        this.title = title;
        this.imgSrc = imageUrl;
        this.thirdpartyUrl = thirdpartyUrl ? thirdpartyUrl : null;
        this.audioLink = audioLink ? audioLink : null;
        this.productType = productType.toLowerCase();
        this.paymentMode = paymentMode;
        this.description = description;
        this.active = active ? true : false;
        this.bestSeller = bestSeller ? true : false;
        this.currency = currency.toUpperCase();
        this.price = +price;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {

            const db = getDb();
            const res = await db.collection('products').insertOne(this);
            return res;

        } catch (err) {
            throw err
        }
    }

    static async fetchAllActiveProds(pageNum, objPerPage) {

        try {
            const db = getDb();
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection('products').find({ active: true }).sort({createdAt: -1}).skip(leave).limit(objPerPage).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async activeProdCount() {

        try {
            const db = getDb();

            const res = await db.collection('products').find({ active: true }).count();
            return res;

        } catch (err) {
            throw err;
        }
    }
    

    
    static async fetchAllProds(pageNum, keywords) {

    try {

        const db = getDb();
        const objPerPage = 12;
        const leave = (+pageNum - 1) * objPerPage;

        const res = await db.collection('products').aggregate([
            {
                $match: { 'title': { $regex: keywords, $options: 'i' } }
            },
            {
                $skip: leave
            },
            {
                $limit: objPerPage
            }

        ]).toArray();

        return res;


    } catch (err) {
        throw err;
    }
}

    static async allProdsCount() {

    try {
        const db = getDb();

        const res = db.collection('products').find({}).count()
        return res

    } catch (err) {
        throw err;
    }
}


    //----- For HomePage books -------//
    static async fetchAllProdsBooks(pageNum, keywords) {

    try {

        const db = getDb();
        const objPerPage = 24;
        const leave = (+pageNum - 1) * objPerPage;

        const res = await db.collection('products').aggregate([
            {
                $match: { $or: [{ productType: 'book' }, { productType: 'books' }], active: true, 'title': { $regex: keywords, $options: 'i' } }
            },
            {
                $project: {
                    imgSrc: 1,
                    title: 1
                }
            },
            {
                $skip: leave
            },
            {
                $limit: objPerPage
            }

        ]).toArray();

        return res;


    } catch (err) {
        throw err;
    }
}

    static async fetchProdBookCount() {

    try {
        const db = getDb();

        const res = await db.collection('products').find({ $or: [{ productType: 'book' }, { productType: 'books' }], active: true }).count();
        return res

    } catch (err) {
        throw err;
    }
}


    static async updateProd(id, obj) {

    try {
        const db = getDb();

        const finalObj = {
            ...obj,
            updatedAt: new Date().toISOString()
        }

        const res = await db.collection('products').updateOne(
            {
                _id: mongo.ObjectID(id)
            },
            {
                $set: finalObj
            }
        )
        return res;

    } catch (err) {
        throw err;
    }
}

    
    static async removeProd(id) {
    try {
        const db = getDb();

        const res = await db.collection('products').deleteOne({ _id: mongo.ObjectID(id) });
        return res;

    } catch (err) {
        throw err;
    }
}


    static async fetchAProduct(id) {

    try {
        const db = getDb();

        const res = db.collection('products').findOne({ _id: mongo.ObjectID(id) });
        return res;

    } catch (err) {
        throw err;
    }
}

    static async fetchAnActiveProduct(id) {

    try {
        const db = getDb();

        const res = db.collection('products').findOne({ _id: mongo.ObjectID(id), active: true });
        return res;

    } catch (err) {
        throw err;
    }
}
}
module.exports = products;