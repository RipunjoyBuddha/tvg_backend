const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class faq {

    constructor(question, answerTitle, videoLink, answer, activeStats){
        this.question = question,
        this.answerTitle = answerTitle,
        this.videoLink = videoLink, 
        this.answer = answer,
        this.activeStats = activeStats?true:false,
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{

            const res = await db.collection('tvgFaq').insertOne(this);
            return res;

        }catch(err){
            throw err
        }
    }

    static async fetchAllFaq(){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('tvgFaq').find({})
            .sort({createdAt:-1})
            .project({createdAt: 0, updatedAt: 0})
            .toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllActiveFaq(){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('tvgFaq').find({activeStats: true})
            .project({createdAt: 0, updatedAt: 0})
            .toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchFaq(id){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('tvgFaq').findOne({ _id: mongo.ObjectID(id)}, {createdAt:0 , updatedAt:0});
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('tvgFaq').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delete(id) {
        const db = getDb();
        
        // Delete service from database
        try {
            
            const res = await db.collection('tvgFaq').deleteOne({_id: mongo.ObjectID(id)});
            return res;
            
        } catch (err) {
            throw err;
        }
    }
}

module.exports = faq;