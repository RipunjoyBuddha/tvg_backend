const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

const dbName = 'hotTopicCategory';

class createCategory {
    constructor(title, imageUrl, active) {
        this.title = title.toLowerCase();
        this.imageUrl = imageUrl;
        this.active = active,
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        const db = getDb();

        try {
            const res = await db.collection(dbName).insertOne(this);
            return res;
        }
        catch (err) {
            throw err
        }
    }

    static async fetchCategory(id) {

        const db = getDb();

        // Fetch all details of the member

        try {
            const res = await db.collection(dbName).findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAllActiveCategories() {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({ active: true }).sort({title:1}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    
    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString()
            };
            for (const c in obj) {
                if (obj[c] !== 'dontExist') {
                    updateObj[c] = obj[c];
                }
            }

            // Make update in DB
            const res = await db.collection(dbName).updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async getAllMember() {
        const db = getDb();

        // Fetch all details of the member

        try {

            // const res = await db.collection('tvgMembers').find({}).project({description:0, createdAt: 0, updatedAt: 0}).toArray();
            const res = await db.collection(dbName).find({}).project({ createdAt: 0, updatedAt: 0 }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

}

module.exports = createCategory;