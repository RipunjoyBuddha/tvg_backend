const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class admin {
    constructor(role, firstName, lastName, imageUrl, address, phone, email, userName, password, activeStats){
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.activeStats = activeStats;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{

            const res = await db.collection('admin').insertOne(this);
            return res;

        }catch(err){
            throw err
        }
    }

    static async fetchAllSubAdmins(){

        const db = getDb();

        // Fetch all details of the member
        try {
            // const res = await db.collection('tvgMembers').find({}).project({description:0, createdAt: 0, updatedAt: 0}).toArray();
            const res = await db.collection('admin').find({role: '10101011010101'}).project({updatedAt: 0}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAdmin(userName){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('admin').findOne({ userName});
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAdminById(id){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('admin').findOne({ _id: mongo.ObjectID(id)});
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async deleteAdmin(id){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('admin').deleteOne({_id: mongo.ObjectID(id)});
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('admin').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }
}


module.exports = admin;