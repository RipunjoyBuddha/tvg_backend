const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class membership {

    constructor(featureTitle, std, elite, platinum){
        this.featureTitle = featureTitle.toLowerCase();
        this.standard = std?true:false;
        this.elite = elite?true:false;
        this.platinum = platinum?true:false;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{
            const res = await db.collection('memberships').insertOne(this);
            return res;

        }catch(err){
            
            throw err
        }
    }

    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('memberships').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchFeatures(){
        const db = getDb();
        
        try{
            const res = await db.collection('memberships')
            .find({featureTitle: {$exists: true}})
            .sort({elite:-1})
            .toArray()

            return res;

        }catch(err){
            throw err
        }
    }

    static async editMembership(id, obj){
        const db = getDb();
        
        try{
             // Make the new update object
             const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('memberships').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        }catch(err){
            throw err
        }
    }

    static async fetchMembership(id){
        const db = getDb();
        
        try{
            const res = await db.collection('memberships')
            .findOne({membershipTitle: {$exists: true}, _id: mongo.ObjectID(id)});
            return res;

        }catch(err){
            throw err
        }
    }

    static async fetchMembershipByTitle(title) {
    
        try {
            const db = getDb();
    
            const res = await db.collection('memberships')
            .findOne({
                membershipTitle: {$exists: true}, 
                membershipTitle: title
            })

            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchMemberships(){
        const db = getDb();
        
        try{
            const res = await db.collection('memberships')
            .find({membershipTitle: {$exists: true}})
            .toArray()

            return res;

        }catch(err){
            throw err
        }
    }
}

module.exports = membership;