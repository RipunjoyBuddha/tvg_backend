const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class users {

    static async fetchUserLogin(username) {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: { username }
                },
                {
                    $project: {
                        firstName: 1,
                        lastName: 1,
                        role: 1,
                        email: 1,
                        username: 1,
                        membership: 1,
                        membershipExpiryDate: 1,
                        password: 1
                    }
                }
            ]).toArray()

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAUser(user) {

        try {
            const db = getDb();

            const res = await db.collection('users').findOne({ $or: [{ email: user }, { username: user }] });
            return res

        } catch (err) {
            throw err;
        }
    }

    static async updatePassword(id, pass) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(id) }, { $set: { password: pass } });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchUserById(id) {

        try {
            const db = getDb();

            const res = db.collection('users').findOne({ _id: mongo.ObjectID(id) })
            return res;

        } catch (err) {
            throw err;
        }
    }


    // Fetch member expiring after a week
    static async fetchGuestpertWitTime(date) {

        try {
            const db = getDb();
            const res = db.collection('users').find({ role: '10101011001010', membershipExpiryDate: date, membershipOrder: {$ne: null} }).project({ email: 1 , membership: 1, firstName: 1, lastName:1}).toArray()
            return res

        } catch (err) {
            throw err;
        }
    }
}
module.exports = users;