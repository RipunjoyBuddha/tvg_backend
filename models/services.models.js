const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class services {
    constructor(title, shortDescription, description, active){
        this.title = title;
        this.shortDescription = shortDescription;
        this.description = description;
        this.active = active;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save(){
        const db = getDb();
        
        try{

            const res = await db.collection('tvgServices').insertOne(this);
            return res;

        }catch(err){
            throw err
        }
    }

    static async fetchAllService(){

        const db = getDb();

        // Fetch all details of the member

        try {

            // const res = await db.collection('tvgMembers').find({}).project({description:0, createdAt: 0, updatedAt: 0}).toArray();
            const res = await db.collection('tvgServices').find({}).project({createdAt: 0, updatedAt: 0}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }
    static async fetchAllActiveService(){

        const db = getDb();

        // Fetch all details of the member

        try {

            const res = await db.collection('tvgServices').find({active: true}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }



    static async fetchService(id){
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('tvgServices').findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }


    
    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('tvgServices').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

   

    static async delete(id) {
        const db = getDb();
        
        // Delete service from database
        try {
            
            const res = await db.collection('tvgServices').deleteOne({_id: mongo.ObjectID(id)});
            return res;
            
        } catch (err) {
            throw err;
        }
    }
}



module.exports = services;