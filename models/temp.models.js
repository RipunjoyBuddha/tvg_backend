const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

class temp {

    constructor(id, img) {
        this._id = id;
        this.imgArr = [img];
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection('temp').insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async getTemp(id) {

        try {
            const db = getDb();

            const res = await db.collection('temp')
                .findOne(
                    {
                        _id: id
                    }
                )
            return res;
        } catch (err) {
            throw err;
        }
    }

    static async updateTemp(id, imgUrl) {

        try {
            const db = getDb();
            const updatedAt = new Date().toISOString();

            const res = await db.collection('temp').updateOne({ _id: id }, { $push: { imgArr: imgUrl }, $set: { updatedAt } })

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delTemp(id) {

        try {
            const db = getDb();

            const res = await db.collection('temp').deleteOne({ _id: id });
            return res

        } catch (err) {
            throw err;
        }
    }


    static async fetchLTDate(date) {

        try {
            const db = getDb();

            const res = await db.collection('temp').find({updatedAt: {$lt: date}}).toArray()
            return res

        } catch (err) {
            throw err;
        }
    }

    static async delLtDate(date) {
    
        try {
            const db = getDb();
    
            const res = await db.collection('temp').deleteMany({updatedAt: {$lt: date}})
            return res
    
        } catch (err) {
                throw err;
            }
        }
}
module.exports = temp;