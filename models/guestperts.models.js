const getDb = require('../util/database').getDb;
const mongo = require('mongodb');

const guestpertRole = "10101011001010";

class guestpert {

    constructor({ role = "10101011001010", imgSrc = '/images/user.png', firstName = '', lastName = '', prefix = '', suffix = '', title = '', tagLine = '', primaryExpertise = '', expandedExpertise = '', address = '', zipCode = '', phone = '', city = '', state = '', country = '', website = '', fax = '', rssfeed = '', membership, membershipExpiryDate = '', membershipOrder, email = '', username, password, verified = false }) {
        this.role = role;
        this.imgSrc = imgSrc;
        this.firstName = firstName;
        this.lastName = lastName;
        this.prefix = prefix;
        this.suffix = suffix;
        this.title = title;
        this.tagLine = tagLine;
        this.primaryExpertise = primaryExpertise;
        this.expandedExpertise = expandedExpertise,
        this.address = address;
        this.zipCode = zipCode;
        this.country = country;
        this.phone = phone;
        this.city = city;
        this.state = state;
        this.website = website;
        this.fax = fax;
        this.rssfeed = rssfeed;
        this.membership = membership;
        this.membershipExpiryDate = membershipExpiryDate;
        this.membershipOrder = membershipOrder;
        this.email = email;
        this.username = username;
        this.password = password;
        this.biography = '';
        this.hotTopics = [];
        this.books = [];
        this.demoReels = [];
        this.media = [];
        this.blogs = [];
        this.orders = [];
        this.premium = false;
        this.verified = verified;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection('users').insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }


    /******Get Total Guestperts Count Active+NonActive -- Admin producer inquiries page */

    static async getAllGuestpertsCount( ) {
    
        try {
            const db = getDb();
            const count = await db.collection('users').find({role: guestpertRole}).count()
            return count
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchAGuestpert(id, type = 'id') {

        try {
            const db = getDb();
            let res;

            if (type === 'id') {
                res = await db.collection('users').findOne({ _id: mongo.ObjectID(id) });

            } else if (type === 'username') {
                res = await db.collection('users').findOne({ username: id });

            } else if (type === 'email') {
                res = await db.collection('users').findOne({ email: id });

            }

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAGuestpertWithUsernameOrEmail(username, email) {

        try {
            const db = getDb();

            const res = db.collection('users').find({ $or: [{ username }, { email }] }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async createBlog(guesptertId, blogId) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guesptertId) }, {
                $push: { blogs: blogId }
            });

            return res;

        } catch (err) {

            throw err;
        }
    }

    static async createHotTopic(guestpertId, hotTopicId) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guestpertId) }, {
                $push: { hotTopics: hotTopicId }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async createBook(guestpertId, bookId) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guestpertId) }, {
                $push: { books: bookId }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async createReel(guestpertId, reelId) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guestpertId) }, {
                $push: { demoReels: reelId }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async createMedia(guestpertId, mediaId) {

        try {
            const db = getDb();

            const res = await db.collection('users').updateOne({ _id: mongo.ObjectID(guestpertId) }, {
                $push: { media: mediaId }
            });

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async delAGuestpert(id) {

        try {
            const db = getDb();

            const res = await db.collection('users').deleteOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    // Used in Admin pannel to search guestperts
    static async fetchAllGuestpert(pageNum, keyword) {

        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            if (!keyword) { keyword = '' };

            const res = await db.collection('users').aggregate([
                {
                    $project: {
                        fullName: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        premium: 1,
                        username: 1,
                        imgSrc: 1,
                        role: 1,
                        firstName: 1,
                        lastName: 1,
                        prefix: 1,
                        suffix: 1,
                        title: 1,
                        tagLine: 1,
                        phone: 1,
                        primaryExpertise: 1,
                        address: 1,
                        city: 1,
                        state: 1,
                        email: 1,
                        website: 1,
                        verified:1,
                        fax: 1,
                        createdAt: 1,
                        membership: 1,
                        membershipExpiryDate: 1,
                        membershipOrder:1
                    }
                },
                {
                    $match: { 'fullName': { $regex: keyword, $options: 'i' }, role: "10101011001010"}
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        lastName: 1,
                        firstName: 1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray();
           
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchGuestpertwithIdLimitedInfo(id) {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        _id: mongo.ObjectID(id),
                        role: "10101011001010",
                        verified: true,
                        membershipOrder: { $ne: null }
                    }
                },
                {
                    $project: {
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        title: 1,
                        imgSrc: 1,
                        expertise: '$primaryExpertise',
                        tagLine: 1,
                        premium: 1,
                        package: '$membership'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchGuestpertFilterAll() {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        role: "10101011001010",
                        verified: true,
                        membershipOrder: { $ne: null }
                    }
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        lastName: 1,
                        firstName: 1,
                    }
                },
                {
                    $project: {
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        title: 1,
                        imgSrc: 1,
                        expertise: '$primaryExpertise',
                        tagLine: 1,
                        premium: 1,
                        package: '$membership'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchGuestpertFilterFunc(filter) {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        role: "10101011001010",
                        verified: true,
                        membershipOrder: { $ne: null },
                        membership: filter
                    }
                },
                {
                    $sort: {
                        lastName: 1,
                        firstName: 1,
                    }
                },
                {
                    $project: {
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        title: 1,
                        imgSrc: 1,
                        expertise: '$primaryExpertise',
                        tagLine: 1,
                        premium: 1,
                        package: '$membership'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchGuestpertFilterReels() {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        role: "10101011001010",
                        verified: true,
                        membershipOrder: { $ne: null },
                        $expr: { $gte: [{ $size: "$demoReels" }, 1] }
                    }
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        lastName: 1,
                        firstName: 1,
                    }
                },
                {
                    $project: {
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        title: 1,
                        imgSrc: 1,
                        tagLine: 1,
                        premium: 1,
                        expertise: '$primaryExpertise',
                        package: '$membership'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchGuestpertFilterExpertise() {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        role: "10101011001010",
                        membershipOrder: { $ne: null },
                        verified: true
                    }
                },
                {
                    $sort: {
                        primaryExpertise: 1,
                        lastName: 1,
                        firstName: 1,
                    }
                },
                {
                    $project: {
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        title: 1,
                        imgSrc: 1,
                        primaryExpertise: 1,
                        tagLine: 1,
                        premium: 1,
                        expertise: '$primaryExpertise',
                        package: '$membership'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    // Used in FrontEnd /guestperts to fetch guestperts alphabetically
    static async fetchAllGuestpertUltraLimited() {

        try {
            const db = getDb();

            const res = db.collection('users').aggregate([
                {
                    $match: {
                        role: "10101011001010",
                        membershipOrder: { $ne: null },
                        verified: true
                    }
                },
                {
                    $project: {
                        name: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        }
                    }
                },
                {
                    $match: {expiryDate: { "$gte": new Date() }}
                },
                {
                    $project:{
                        name:1
                    }
                },
                {
                    $sort: {
                        name: 1
                    }
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }


    // --- Guestperts on home page And ProducerPage Enquiries-----//

    static async fetchAllGuestpertLimitedInfo(pageNum, keyword) {

        try {
            const db = getDb();

            const objPerPage = 24;
            const leave = (+pageNum - 1) * objPerPage;

            if (!keyword) { keyword = '' };

            const res = await db.collection('users').aggregate([
                {
                    $project: {
                        fullName: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        imgSrc: 1,
                        package: '$membership',
                        membershipOrder: 1,
                        title: 1,
                        role: 1,
                        verified: 1
                    }
                },
                {
                    $match: { 'fullName': { $regex: keyword, $options: 'i' }, role: "10101011001010", membershipOrder: { $ne: null }, verified: true }
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        lastName: 1,
                        firstName: 1,
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async createGuestpertUnreadInquiries(guestpertId) {
    
        try {
            const db = getDb();
    
            const usrInq = await db.collection('users').findOne({_id: mongo.ObjectID(guestpertId)})
            let {unreadInquiries} = usrInq
            
            unreadInquiries?unreadInquiries++:unreadInquiries=1
            
            await db.collection('users').updateOne({_id: mongo.ObjectID(guestpertId)}, {$set: {unreadInquiries}})
            return

        } catch (err) {
                throw err;
            }
        }

    /////////////////////////////////////////////////


    static async fetchAllGuestpertbyKeyword({ keywords }) {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        verified: true,
                        membershipOrder: { $ne: null }
                    }
                },
                {
                    $project: {
                        name: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        membershipOrder: 1,
                        role: 1,
                        id: '$_id',
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        }
                    }
                },
                {
                    $match: { name: { $regex: keywords, $options: 'i' }, role: "10101011001010", expiryDate: { "$gte": new Date() } }
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        name: 1
                    }
                }
            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllGuestpertbyKeywordLimitedInfo(keywords) {

        try {
            const db = getDb();
            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        membershipOrder: { $ne: null },
                        verified: true
                    }
                },
                {
                    $project: {
                        role: 1,
                        membershipOrder: 1,
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        name: {
                            $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"]
                        },
                        fullName: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        title: 1,
                        imgSrc: 1,
                        primaryExpertise: 1,
                        tagLine: 1,
                        premium: 1,
                        expertise: '$primaryExpertise',
                        package: '$membership'
                    }
                },
                {
                    $match: { 'fullName': { $regex: keywords, $options: 'i' }, role: "10101011001010", expiryDate: { "$gte": new Date() } }
                },
                {
                    $sort: {
                        membershipOrder: -1,
                        fullName: 1
                    }
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAGuestpertLimitedInfo(id) {

        try {
            const db = getDb();

            const res = await db.collection('users').aggregate([
                {
                    $match: {
                        _id: mongo.ObjectID(id),
                        role: "10101011001010"
                    }
                },
                {
                    $project: {
                        fullName: {
                            $concat: ["$firstName", " ", "$lastName"]
                        },
                        imgSrc: 1,
                        title: 1,
                        package: '$membership',

                    }
                }
            ]).toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }


    static async totalItems() {

        try {
            const db = getDb();

            const res = await db.collection('users').find({ role: "10101011001010", verified: true }).count();
            return res

        } catch (err) {
            throw err;
        }
    }


    static async activeGuestpertCount() {

        try {

            const db = getDb();

            const res = await db.collection('users').find({ role: "10101011001010", membershipOrder:{$ne: null}, verified: true }).count();
            return res

        } catch (err) {
            throw err;
        }
    }



    static async updateGuestpert(id, obj) {

        try {
            const db = getDb();
            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }
            const res = await db.collection('users').updateOne(
                { _id: mongo.ObjectID(id) },
                {
                    $set: finalObj
                }
            )
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchAGuestpertWithIdLongList(id) {

        try {
            const db = getDb();

            const res = db.collection('users').aggregate([
                {
                    $match: {
                        _id: mongo.ObjectID(id),
                        role: guestpertRole,
                        verified: true,
                        membershipOrder: { $ne: null }
                    }
                },
                {
                    $lookup: {
                        from: 'hotTopics',
                        localField: 'hotTopics',
                        foreignField: '_id',
                        as: 'allHotTopics'
                    }
                },
                {
                    $lookup: {
                        from: 'books',
                        localField: 'books',
                        foreignField: '_id',
                        as: 'allBooks'
                    }
                },
                {
                    $lookup: {
                        from: 'reels',
                        localField: 'demoReels',
                        foreignField: '_id',
                        as: 'allReels'
                    }
                },
                {
                    $lookup: {
                        from: 'blogs',
                        localField: 'blogs',
                        foreignField: '_id',
                        as: 'allBlogs'
                    }
                },
                {
                    $lookup: {
                        from: 'media',
                        localField: 'media',
                        foreignField: '_id',
                        as: 'allMedia'
                    }
                },
                {
                    $project: {
                        imgSrc: 1,
                        name: { $concat: ["$prefix", " ", "$firstName", " ", "$lastName", " ", "$suffix"] },
                        title: 1,
                        primaryExpertise: 1,
                        expandedExpertise: 1,
                        expiryDate: {
                            $toDate: '$membershipExpiryDate'
                        },
                        allHotTopics: 1,
                        allReels: 1,
                        allBlogs: 1,
                        allMedia: 1,
                        allBooks: 1,
                        membership: 1,
                        biography: 1
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() }
                    }
                }
            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    // - Verify guestpert ----------

    static async verifyGuestpert(id) {

        try {
            const db = getDb();

            const res = db.collection('users').updateOne({ _id: mongo.ObjectID(id) }, { $set: { verified: true } })
            return res;

        } catch (err) {
            throw err;
        }
    }


    // ---- Buy GuestpertMembership

    static async fetchGuestpertMembershipInfo(id) {

        try {
            const db = getDb();

            const res = await db.collection('users').find({ _id: mongo.ObjectID(id) })
                .project({
                    membership: 1,
                    membershipExpiryDate: 1,
                    membershipOrder: 1
                }).toArray()

            return res[0];

        } catch (err) {
            throw err;
        }
    }

}
module.exports = guestpert;