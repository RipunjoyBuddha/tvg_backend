const getDb = require('../util/database').getDb;
const mongo = require('mongodb');


class settings {
    constructor() { };

    static async updateGensettings(title, obj) {

        try {
            const db = getDb();

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('settings').updateOne({ title }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async updateSeoSettings(obj) {
        try {
            const db = getDb();
            const title = 'seo_settings'

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('settings').updateOne({ title }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async updatePaypalSettings(obj) {
        try {
            const db = getDb();
            const title = 'paypal_settings'

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('settings').updateOne({ title }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAll() {
        try {
            const db = getDb();

            const res = await db.collection('settings').find({}).project({ createdAt: 0, updatedAt: 0 }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchSiteSettings() {

        try {
            const db = getDb();

            const res = await db.collection('settings').findOne({ title: 'site_settings' });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchSeoSettings() {

        try {
            const db = getDb();

            const res = await db.collection('settings').findOne({ title: 'seo_settings' });
            return res;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = settings;