const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'hotTopics';

class hotTopics {

    constructor({ authorId, category, title, tags, active, openingSpeakingPoint, content, imgArr }) {
        this.authorId = authorId;
        this.category = category;
        this.title = title;
        this.tags = tags;
        this.active = active ? true : false;
        this.openingSpeakingPoint = openingSpeakingPoint;
        this.content = content;
        this.imgArr = imgArr;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchAllTopics() {

        try {
            const db = getDb();

            const res = db.collection(dbName).find({}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }
    static async fetchAuthorTopics(authorId) {

        try {
            const db = getDb();

            const res = db.collection(dbName).find({$or: [{authorId: mongo.ObjectID(authorId)}, {authorId}] }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async updateHotTopic(id, obj) {

        try {
            const db = getDb();

            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }

            const res = await db.collection(dbName).updateOne(
                {
                    _id: mongo.ObjectID(id)
                },
                {
                    $set: finalObj
                }
            );

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchById(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delHotTopic(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).deleteOne({ _id: mongo.ObjectID(id) })
            return res;

        } catch (err) {
            throw err;
        }
    }

    // Hot Topics on Home Page
    static async fetchAllTopicsLimitedInfo(pageNum, keyword, deactivatePage = null) {

        try {
            const db = getDb();

            let objPerPage = 24;
            let leave = (+pageNum - 1) * objPerPage;

            if (deactivatePage) {
                objPerPage = Infinity;
                leave = 0
            }

            if (!keyword) { keyword = '' };

            const res = await db.collection(dbName).aggregate([
                {
                    $project: {
                        authorId: 1,
                        category: 1,
                        topicTitle: '$title',
                        active: 1
                    }
                },
                {
                    $match: { active: true, topicTitle: { $regex: keyword, $options: 'i' } }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $match: {
                        author: {
                            $size: 1
                        }
                    }
                }
            ]).toArray();
            
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async homePageHotTopicsCount( ) {
    
        try {
            const db = getDb();
    
            const res = await db.collection(dbName).aggregate([
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $match: {
                        active: true,
                        author: {
                            $size: 1
                        }
                    }
                }
            ]).toArray()
            return res.length
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchAllHotTopicWithKeywordLimitedInfo(keyword) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { active: true, $or: [{ title: { $regex: keyword, $options: 'i' } }, { tags: keyword }] }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        openingSpeakingPoint: 1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $match: {
                        'authDetails.membershipOrder':{$ne: null}
                    }
                },
                {
                    $project: {
                        title: 1,
                        verified: '$authDetails.verified',
                        expiryDate: {
                            $toDate: '$authDetails.membershipExpiryDate'
                        },
                        openingSpeakingPoint: 1,
                        authFirstName: '$authDetails.firstName',
                        authLastName: '$authDetails.lastName',
                        authPrefix: '$authDetails.prefix',
                        authSuffix: '$authDetails.suffix',
                        authImgSrc: '$authDetails.imgSrc',
                        authExpandedExpertise: '$authDetails.expandedExpertise',
                        authPrimaryExpertise: '$authDetails.primaryExpertise'
                    }
                },
                {
                    $match: {
                        verified: true,
                        expiryDate: { "$gte": new Date() }
                    }
                },
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchTopicByTag(tag) {
        try {
            const tagRegex = new RegExp(`^${tag}$`,'i')
            const db = getDb();

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { active: true, tags: tagRegex }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        openingSpeakingPoint: 1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $project: {
                        title: 1,
                        openingSpeakingPoint: 1,
                        expiryDate: {
                            $toDate: '$authDetails.membershipExpiryDate'
                        },
                        authVerified: '$authDetails.verified',
                        authFirstName: '$authDetails.firstName',
                        authLastName: '$authDetails.lastName',
                        authPrefix: '$authDetails.prefix',
                        authSuffix: '$authDetails.suffix',
                        authImgSrc: '$authDetails.imgSrc',
                        authExpandedExpertise: '$authDetails.expandedExpertise',
                        authPrimaryExpertise: '$authDetails.primaryExpertise'
                    }
                },
                {
                    $match: {
                        authVerified: true,
                        expiryDate: { "$gte": new Date() }
                    }
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchAllTopicbyKeyword({ keywords }) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).aggregate([
                {
                    $match:{
                        title: { $regex: keywords, $options: 'i' }, 
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        active: 1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $match: {
                        'authDetails.membershipOrder':{$ne: null}
                    }
                },
                {
                    $project: {
                        title: 1,
                        active: 1,
                        verified: '$authDetails.verified',
                        expiryDate: {
                            $toDate: '$authDetails.membershipExpiryDate'
                        },
                    }
                },
                {
                    $match: { 
                        active: true,
                        verified: true,
                        expiryDate: { "$gte": new Date() }
                    }
                },
                {
                    $sort: {
                        title: 1
                    }
                },
                {
                    $project: {
                        _id: 0,
                        name: '$title',
                        id: '$_id'
                    }
                }
            ]).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchATopicLimitedInfo(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).aggregate([
                {
                    $match: { _id: mongo.ObjectID(id) }
                },
                {
                    $project: {
                        authorId: 1,
                        category: 1,
                        topicTitle: '$title',
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $match: {
                        author: {
                            $size: 1
                        }
                    }
                }
            ]).toArray();

            return res[0];

        } catch (err) {
            throw err;
        }
    }


    static async fetchAllTopicLimitedInfo(cat) {

        try {
            const db = getDb();
            
            const res = await db.collection(dbName).aggregate([
                {
                    $match: { category: {$regex: cat, $options:'i'}, active: true }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        openingSpeakingPoint: 1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $match:{
                        'authDetails.membershipOrder': {$ne: null}
                    }
                },
                {
                    $project: {
                        title: 1,
                        openingSpeakingPoint: 1,
                        expiryDate: {
                            $toDate: '$authDetails.membershipExpiryDate'
                        },
                        authVerified: '$authDetails.verified',
                        authFirstName: '$authDetails.firstName',
                        authLastName: '$authDetails.lastName',
                        authPrefix: '$authDetails.prefix',
                        authSuffix: '$authDetails.suffix',
                        authImgSrc: '$authDetails.imgSrc',
                        authExpandedExpertise: '$authDetails.expandedExpertise',
                        authPrimaryExpertise: '$authDetails.primaryExpertise'
                    }
                },
                {
                    $match: {
                        expiryDate: { "$gte": new Date() },
                        authVerified: true
                    }
                }
            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchReadTopic(id) {

        try {
            const db = getDb();
            const res = await db.collection(dbName).aggregate([
                {
                    $match: { _id: mongo.ObjectID(id), active: true }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        category: 1,
                        tags: 1,
                        content: 1,
                        openingSpeakingPoint: 1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $match:{
                        'authDetails.membershipOrder': {$ne: null}
                    }
                },
                {
                    $project: {
                        title: 1,
                        category: 1,
                        tags: 1,
                        content: 1,
                        openingSpeakingPoint: 1,
                        expiryDate: {
                            $toDate: '$authDetails.membershipExpiryDate'
                        },
                        authVerified: '$authDetails.verified',
                        authFirstName: '$authDetails.firstName',
                        authLastName: '$authDetails.lastName',
                        authPrefix: '$authDetails.prefix',
                        authSuffix: '$authDetails.suffix',
                        authImgSrc: '$authDetails.imgSrc',
                        authTagLine: '$authDetails.tagLine'
                    }
                },
                {
                    $match:{
                        authVerified: true,
                        expiryDate: { "$gte": new Date() }
                    }
                }
            ]).toArray();
            
            return res[0];

        } catch (err) {
            throw err;
        }
    }
}
module.exports = hotTopics;