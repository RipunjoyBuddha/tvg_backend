const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'reels';

class reels {

    constructor({ title, videoLink, active, authorId }) {
        this.title = title;
        this.videoLink = videoLink;
        this.active = active ? true : false;
        this.authorId = authorId;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchReelsByAuthor(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).find({$or : [{ authorId: mongo.ObjectID(id) }, {authorId:id}]}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchReelById(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delReelById(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).deleteOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async editReel( id, obj) {
    
        try {
            const db = getDb();
    
            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }

            const res = await db.collection(dbName).updateOne({_id: mongo.ObjectID(id)}, {$set: finalObj});
            return res;
    
        } catch (err) {
                throw err;
            }
        }
}
module.exports = reels;