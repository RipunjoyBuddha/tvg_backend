const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'tempOrders'

class tempProducts {

    constructor({ name, currency, productId, price, quantity, imgSrc }) {
        this.name = name;
        this.productId = mongo.ObjectID(productId);
        this.currency = currency;
        this.price = price;
        this.quantity = quantity;
        this.imgSrc = imgSrc;
        this.createdAt = new Date();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async delOrder(id) {
        try {
            const db = getDb();

            const res = db.collection(dbName).deleteOne({ _id: mongo.ObjectID(id) })
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchOrder(id) {
    
        try {
            const db = getDb();
    
            const res = db.collection(dbName).findOne({_id: mongo.ObjectID(id)})
            return res;
    
        } catch (err) {
                throw err;
            }
        }
}
module.exports = tempProducts;