const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'blogs';

class blogs {

    constructor(id, title, content, active, imageUrl, imgArr) {
        this.title = title;
        this.authorId = id==='jacquie jordan'?id:mongo.ObjectID(id);
        this.content = content;
        this.activeStats = active ? true : false;
        this.imageUrl = imageUrl;
        this.imgArr = imgArr;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchLatestTwo( ) {
    
        try {
            const db = getDb();
    
            const res = db.collection(dbName).aggregate([
                {
                    $match: {
                        authorId: 'jacquie jordan',
                        activeStats: true
                    }  
                },
                {
                    $sort: {
                        updatedAt: -1
                    }
                },
                {
                    $limit: 2
                },
                {
                    $project: {
                        title: 1,
                        content:1
                    }
                }
            ]).toArray();

            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchABlog(id) {

        try {
            const db = getDb();
            const res = await db.collection(dbName).aggregate([
                {
                    $match: {
                        _id: mongo.ObjectID(id),
                        activeStats: true
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        content: 1,
                        createdAt: 1,
                        imageUrl:1,
                        authorId:1,
                        activeStats:1,
                        imgArr:1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $project: {
                        title: 1,
                        content: 1,
                        createdAt: 1,
                        imageUrl:1,
                        authorId:1,
                        activeStats:1,
                        imgArr:1,
                        firstName: '$authDetails.firstName',
                        lastName: '$authDetails.lastName',
                        prefix: '$authDetails.prefix',
                        suffix: '$authDetails.suffix',
                    }
                }
            ]).toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchABlogForEditing(id) {

        try {
            const db = getDb();
            const res = await db.collection(dbName).aggregate([
                {
                    $match: {
                        _id: mongo.ObjectID(id)
                    }
                },
                {
                    $lookup: {
                        from: 'users',
                        localField: 'authorId',
                        foreignField: '_id',
                        as: 'author'
                    }
                },
                {
                    $project: {
                        title: 1,
                        content: 1,
                        createdAt: 1,
                        imageUrl:1,
                        authorId:1,
                        activeStats:1,
                        imgArr:1,
                        authDetails: { $arrayElemAt: ["$author", 0] }
                    }
                },
                {
                    $project: {
                        title: 1,
                        content: 1,
                        createdAt: 1,
                        imageUrl:1,
                        authorId:1,
                        activeStats:1,
                        imgArr:1,
                        firstName: '$authDetails.firstName',
                        lastName: '$authDetails.lastName',
                        prefix: '$authDetails.prefix',
                        suffix: '$authDetails.suffix',
                    }
                }
            ]).toArray();
            return res[0];

        } catch (err) {
            throw err;
        }
    }

    static async fetchBlogsWithAuthorId(id) {

        try {
            const db = getDb();
            const res = await db.collection(dbName).find({ authorId: mongo.ObjectID(id) }).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async updateBlog(id, obj) {

        try {
            const db = getDb();

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            const res = db.collection(dbName).updateOne(
                {
                    _id: mongo.ObjectID(id)
                },
                {
                    $set: updateObj
                }
            )
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delBlog(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).deleteOne({
                _id: mongo.ObjectID(id)
            })
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async delBlogAuthorId(id) {

        try {
            const db = getDb();

            const res = await db.collection(dbName).deleteMany({
                authorId: id
            })

            return res;

        } catch (err) {
            throw err;
        }
    }
}
module.exports = blogs;