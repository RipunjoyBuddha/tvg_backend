const getDb = require('../util/database').getDb;
const mongo = require('mongodb');


class createCategory {
    constructor({ title, active = true, template }) {
        this.title = title;
        this.active = active;
        this.template = template;
        this.updatedAt = new Date().toISOString();
    }

    async save() {

        try {
            const db = getDb();

            const res = await db.collection('newsletterTemplate').insertOne(this);
            return res;

        } catch (err) {
            throw err
        }
    }


    // ----- Used for showing Newsletters in testimonial page
    static async fetchActiveNewsletters() {

        try {
            const db = getDb();

            const res = await db.collection('newsletterTemplate')
                .find({ active: true })
                .sort({ createdAt: -1 })
                .project({ title: 1 })
                .toArray()

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async update(id, obj) {
        const db = getDb();

        try {

            // Make the new update object
            const updateObj = {
                updatedAt: new Date().toISOString(),
                ...obj
            };

            // Make update in DB
            const res = await db.collection('newsletterTemplate').updateOne({ _id: mongo.ObjectID(id) }, { $set: updateObj });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async fetchTemplates(pageNum) {

        try {
            const db = getDb();
            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection('newsletterTemplate')
                .find({})
                .sort({ createdAt: -1 })
                .skip(leave)
                .limit(objPerPage)
                .project({ _id: 1, active: 1, title: 1 })
                .toArray();

            return res
        }
        catch (err) {
            throw err
        }
    }


    static async getNewsletterSubscribers(pageNum, keywords) {

        const db = getDb();

        try {
            const objPerPage = 24;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection('newsletterSubscribers').aggregate([
                {
                    $match: { 'email': { $regex: keywords, $options: 'i' } }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }

            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async updateSubscribers(id, obj) {
        const db = getDb();

        // Delete service from database
        try {

            const res = await db.collection('newsletterSubscribers')
                .updateOne({ _id: mongo.ObjectID(id) }, { $set: obj });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async deleteSubscribers(id) {
        const db = getDb();

        // Delete service from database
        try {

            const res = await db.collection('newsletterSubscribers').deleteOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async getByHistory(pageNum) {
        try {
            const db = getDb();

            const objPerPage = 12;
            const leave = (+pageNum - 1) * objPerPage;

            const res = await db.collection('newsletterTemplate').aggregate([
                {
                    $unwind: '$history'
                },
                {
                    $project: {
                        subject: 1,
                        content: 1,
                        history: 1,
                        _id: 0
                    }
                },
                {
                    $sort: {
                        history: -1
                    }
                },
                {
                    $skip: leave
                },
                {
                    $limit: objPerPage
                }
            ]).toArray();

            return res;

        } catch (err) {
            throw err;
        }
    }


    static async getCount() {

        try {
            const db = getDb();

            const res = await db.collection('newsletterTemplate')
                .find({}).count();

            return res;
        }
        catch (err) {
            throw err
        }
    }


    static async delete(id) {
        const db = getDb();

        // Delete service from database
        try {

            const res = await db.collection('newsletterTemplate').deleteOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchNewsletter(id) {
        const db = getDb();

        // Fetch all details of the member
        try {
            const res = await db.collection('newsletterTemplate').findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }
}

module.exports = createCategory;