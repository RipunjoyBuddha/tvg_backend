const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'media'

class media {

    constructor({ appearanceDate, topic, title, tags, active, videoLink, authorId }) {
        this.appearanceDate = appearanceDate;
        this.topic = topic;
        this.title = title;
        this.videoLink = videoLink;
        this.tags = tags;
        this.active = active ? true : false;
        this.authorId = authorId;
        this.createdAt = new Date().toISOString();
        this.updatedAt = new Date().toISOString();
    }

    async save() {
        try {
            const db = getDb();
            const res = await db.collection(dbName).insertOne(this);
            return res;
        } catch (err) {
            throw err
        }
    }

    static async fetchMediaById(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).findOne({ _id: mongo.ObjectID(id) });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async editMedia( id, obj) {
    
        try {
            const db = getDb();
    
            const finalObj = {
                ...obj,
                updatedAt: new Date().toISOString()
            }

            const res = db.collection(dbName).updateOne({_id: mongo.ObjectID(id)}, {$set: finalObj});
            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async delMedia( id) {
    
        try {
            const db = getDb();
    
            const res = db.collection(dbName).deleteOne({_id: mongo.ObjectID(id)});
            return res;
    
        } catch (err) {
                throw err;
            }
        }

    static async fetchMediaByAuthorId(id) {

        try {
            const db = getDb();

            const res = db.collection(dbName).find({$or: [{ authorId: mongo.ObjectID(id)}, {authorId: id}]}).toArray();
            return res;

        } catch (err) {
            throw err;
        }
    }
}
module.exports = media;