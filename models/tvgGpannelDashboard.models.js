const getDb = require('../util/database').getDb;
const mongo = require('mongodb');
const dbName = 'tvgGPannelDashboard'
const producerPannelDb = 'tvgPPannelDashboard'

class tvgDashboard {

    constructor() { }

    static async fetchGPannelDashboard() {

        try {
            const db = getDb();

            const res = db.collection(dbName).findOne({});
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async editGuestpertPannelDashboard(obj) {
        try {
            const db = getDb();

            const finalObj = { ...obj, updatedAt: new Date() };

            const res = db.collection(dbName).updateOne({}, { $set: finalObj });
            return res;

        } catch (err) {
            throw err;
        }
    }


    static async editProducerPannelDashboard(obj) {
        try {
            const db = getDb();

            const finalObj = { ...obj, updatedAt: new Date() };

            const res = db.collection(producerPannelDb).updateOne({}, { $set: finalObj });
            return res;

        } catch (err) {
            throw err;
        }
    }

    static async fetchPPannelDashboard(){
        try {
            
            const db = getDb()
            const res = db.collection(producerPannelDb).findOne({})
            return res

        } catch (error) {
            throw error
        }
    }
}
module.exports = tvgDashboard;