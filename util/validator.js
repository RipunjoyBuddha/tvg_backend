const validator = require('validator');
const dateFormat = require('dateformat')

const vidLinkRegex = (val) => {
    const regex = /^(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))$/;
    let result = regex.test(val);
    if (!val.includes('src')) { result = false };

    return result;
}


exports.videoLinkValidator = (value = '') => {

    // Return true if no value is provided
    if (value.length === 0) { return true };

    const regex = /^(?:<iframe[^>]*)(?:(?:\/>)|(?:>.*?<\/iframe>))$/;
    let result = regex.test(value);
    if (!value.includes('src')) { result = false };

    if (!result) {
        throw new Error('video link failed to validate');
    }
    return true;
}


exports.videoLinkArrValidator = (value) => {

    if (!value) return true;
    if (!Array.isArray(value)) {
        // Return true if no value is provided
        if (value.length === 0) { return true };
        
        const result = vidLinkRegex(value);

        if (!result) {
            throw new Error('video link failed to validate');
        }
    } else {
        value.forEach(c => {
            const result = vidLinkRegex(c);
            if (!result) {
                throw new Error('video link failed to validate');
            }
        })
    }

    return true;
}


exports.membershipValidation = (value)=>{

    if(!value || value.length===0){return true};
    
    const memberships = ['elite', 'standard', 'platinum'];
    
    if(!memberships.includes(value)){
        throw new Error('Membership does not exist')
    }
    
    return true;
}


exports.dateValidation = (value='')=>{

    if(!value)return true;
    if(value.length === 0)return true;
    
    const val = validator.isDate(value);
    if(!val){
        throw new Error('Invalid Date');
    }
    const today = new Date(dateFormat(new Date(), 'yyyy-mm-dd'));
    const incomingDate = new Date(value);
    
    if(incomingDate < today){
        throw new Error('Date already expired')
    }

    return true;
}


exports.nonEmpty = (value = '') => {

    // Return error if length is zero
    if (!value.length > 0 || !value) {
        throw new Error('This is an empty string');
    }
    return true;
}


exports.validMobile = (value= '') =>{
    if(!value || value.length === 0){
        return true
    }
    const chck = validator.isMobilePhone(value);
    if(!chck){
        throw new Error('Invalid Phone number')
    }
    return true
}

exports.numeric = (value= '') =>{
    if(!value || value.length === 0){
        return true
    }
    const chck = validator.isNumeric(value);
    if(!chck){
        throw new Error('Invalid value')
    }
    return true
}


exports.alphaNumericSpace = (value = '') => {

    if(value.length===0)return true;

    if (!value) {
        throw new Error('value is not defined')
    }

    const regex = /^[a-z\d\-_,.?!@#$%^&*'":;{}\s]+$/i;
    
    const res = regex.test(value);

    if (!res) {
        throw new Error('Validatin failed')
    }

    return true;
}

                            

exports.urlCheck =(value='')=>{

    if(value.length===0)return true;

    const val = validator.isURL(value);

    if (!val) { throw new Error('value is not an url') };
    return true;
}


exports.EmailArray = (value) => {
    if (!value) return true;

    if (Array.isArray(value)) {
        value.forEach(c => {
            const val = validator.isEmail(c);
            if (!val) { throw new Error('value is not an email') }
        })
        return true;
    } else {
        if (typeof (value) == 'string') {
            const val = validator.isEmail(value);
            if (!val) { throw new Error('value is not an email') };
            return true;
        }

        throw new Error('Invalid Email')
    }
}

exports.stringArray = (value, max=Infinity) => {

    if (!value) return true;

    if (Array.isArray(value)) {
        value.forEach(c => {
            const val = typeof (c) === 'string';
            if (!val) { throw new Error('value is not a string') }
        })
        return true;
    } else {
        if (typeof (value) == 'string') {
            return true;
        }

        throw new Error('Invalid Mail address')
    }
}


exports.checkPassword = (value, { req }) => {
    if (value !== req.body.password) {
        throw new Error('Confirm Password does not match password');
    }

    // Indicates the success of this synchronous custom validator
    return true;
}

exports.checkbox = (value) => {

    if (!value || value === 'active') { return true }
    else { throw new Error('invalid check box') }
}

exports.validUrl = (value) => {
    if (!value) return true;
    
    const val = validator.isURL(value);
    if (!val) {
        throw new Error('not an url');
    }
    else {
        return true;
    }
}

exports.tiaiMongoIdCheck = (value)=>{
    if (!value || value.length==0) return true;

    const id = value.split(':')[0];

    const val = validator.isMongoId(id);
    if (!val) {
        throw new Error('not a mongoId');
    }
    else {
        return true;
    }
    
}