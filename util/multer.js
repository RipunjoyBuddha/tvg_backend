const multer = require('multer');
const uniqid = require('uniqid');

const multerSettings = (storageFolder ='general', maxSize)=> {

    return {
        fileStorage: multer.diskStorage({
            destination: (req, file, cb)=>{
                cb(null, `images/${storageFolder}`)
            },
            filename: (req, file, cb)=> {
                cb(null, uniqid() + '-' + file.originalname.replace(/[\s,!@#$%^&*()<>\/?"']/g, '_'));
            }
        }),
    
        fileFilter: (req, file, cb)=>{

            if(file.mimetype ==='image/jpeg' || file.mimetype ==='image/JPEG' || file.mimetype ==='image/png' || file.mimetype ==='image/PNG' || file.mimetype ==='image/jpg' || file.mimetype ==='image/JPG'){
                cb(null, true);
            }else{
                req.flash('passOnMsg', 'Invalid mimetype')
                cb(null, false);
            }
        },

        fileStoragePdf: multer.diskStorage({
            destination: (req, file, cb)=>{
                cb(null, `data/newsletters`)
            },
            filename: (req, file, cb)=> {
                cb(null, uniqid() + '-' + file.originalname);
            }
        }),

        fileFilterPdf: (req, file, cb)=>{
            if(file.mimetype ==='application/pdf' || file.mimetype ==='application/PDF'){
                cb(null, true);
            }else{
                req.flash('passOnMsg', 'Invalid mimetype')
                cb(null, false);
            }
        },
    } 
};

module.exports = multerSettings;