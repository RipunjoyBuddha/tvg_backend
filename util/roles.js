roles = {
    subAdmin: '10101011010101',
    guestpert: '10101011001010',
    '10101011001010': 'guestpert',
    godMode: '10101101001110',
    '10101101001110': 'godMode',
    producer: '10111010000101',
    '10111010000101': 'producer',
    other: '1101001011011001',
    '1101001011011001': 'other'
}

module.exports = roles;