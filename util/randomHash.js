const crypto = require('crypto');

const createHash = async ()=>{
    let token = await crypto.randomBytes(32).toString('hex')
    return token
}

module.exports = createHash;