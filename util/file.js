const fs = require('fs');
const path = require('path');



const delFile = (filePath)=>{
    if(!filePath)return
    if(filePath.includes('/user.png'))return;
    
    const absPath = path.join(__dirname , '../' , filePath);
    fs.unlink(absPath, err=>{
        if(err){
            return {
                error: true,
                err
            }
        }
    })
}

exports.delFile = delFile;