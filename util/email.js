const email_Ids = {
    newGuestpertNotificationToAdmin : '5f8377f6e664c90b007f084d',
    newProducerNotificationToAdmin : '5fe1bb96b33aa04f7428a614',
    welcomeNewGuestpert : '5f837df89647d507e4773fcc',
    guestpertVerification: '5f8421094b750454d0952fa8',
    guestpertPasswordRecovery: '5f86e8b0fdaed232b441295d',
    userProductPurchase: '5fd382be4757856bb80b2410',
    userMembershipPurchase: '5fd3b4a8c44076707412ad6a',
    userProductPurchaseToAdmin: '5fd397076178c35e5c28ef95',
    updateEmailAddress: '5fee0ce48adce68074c7866b',
    contactFormMessage : '6072881df982f21ab4a8c574',
    orderReceived: '6072e32ba0054f79d4da874a',
    membershipRenewNotification: '6073e3ae92ce196a0016ef55',
    membershipExpirationNotification: '6073f32d3fbba61c8c0dfa2d'
}

module.exports = email_Ids;