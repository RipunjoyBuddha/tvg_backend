const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;


let _db;

exports.getDb = ()=>{
    if(_db){
        return _db; 
    }
    throw ('No database found!');
}

const configureDb = require('../models/dbConfigurations');
const mongoConnect = (callback)=>{
    MongoClient.connect(process.env.MONGODBURI)
    .then(async client=>{
        _db = client.db('tvGuestpert')
        if(process.env.DATABASE_CONFIGURE==='true'){ 
            await configureDb()
        }
        console.log('Database connected');
        callback()
    })
    .catch(err =>{
        console.log('error in mongodbconnection' + err);
        throw err;
    })
}

exports.mongoConnect = mongoConnect;