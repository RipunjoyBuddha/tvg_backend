const fs = require('fs')
const path = require('path')

const dateFormat = require('dateformat');
const { validationResult } = require('express-validator');
const sgMail = require('@sendgrid/mail');
const validator = require('validator')

const email_ids = require('../util/email');

const tvgHome = require('../models/tvgHome.models');
const guestpert = require('../models/guestperts.models');
const hotTopics = require('../models/hotTopics.models');
const blogs = require('../models/blogs.models');
const hotTopicCategory = require('../models/hotTopicCategory.models');
const teamMembers = require('../models/teamMembers.models');
const founders = require('../models/founder.models');
const services = require('../models/services.models');
const faqs = require('../models/faq.models');
const products = require('../models/products.models');
const contact = require('../models/tvgContact.models');
const testimonials = require('../models/testimonials.models')
const newsletters = require('../models/newsletter.models')
const emails = require('../models/email.models');

sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);

///////////////////////////
////--- Get Pdf --- ////
/////////////////////////

exports.readNewsletter = async (req, res, next) => {
    try {

        const { id } = req.params;
        const newsletter = await newsletters.fetchNewsletter(id)
        if (!newsletter) {
            return res.redirect('/testimonials')
        }

        const file = fs.createReadStream(path.join(__dirname, '..', newsletter.template))
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader(
            'Content-Disposition',
            `inline; filename="${newsletter.title}.pdf"`
        )
        file.pipe(res)

    } catch (err) {
        next(err)
    }
}


///////////////////////////
////--- Get Pages --- ////
/////////////////////////

exports.getTestimonialsPage = async (req, res, next) => {
    try {

        const allNewsletters = await newsletters.fetchActiveNewsletters()

        res.render('templates/front/testimonial', {
            allNewsletters
        })

    } catch (err) {
        next(err)
    }
}

exports.getTvgHomePage = async (req, res, next) => {
    try {

        const homePage = await tvgHome.fetchHomePage();
        const blog = await blogs.fetchLatestTwo();

        blog.forEach(c => {
            c.content = c.content.replace(/<[^>]*>/ig, ' ')
                .slice(0, 200)
                .replace(/<img.*?src="(.*?)"[^\>]+>/g, '')
                .replace(/<\/[^>]*>/ig, ' ')
                .replace(/&nbsp;|&#160;/gi, ' ')
                .replace(/\s+/ig, ' ')
                .trim()
        })

        const slides = {};
        homePage.slides.forEach(c => {
            if (c.imageUrl) {
                c.imageUrl = c.imageUrl.replace('\\', '/');
            }
            slides[`slide_${c.id}`] = c;
        })
        homePage.slides = slides;

        res.render('templates/front/home', {
            homePage,
            blogs: blog
        })
    } catch (err) {
        next(err)
    }
}


////////////////////////////////////////////////////////////////
////--- Used to Search Guestpert with keywords or id or nothing
///////////////////////////////////////////////////////////////
exports.searchGuestpert = async (req, res, next) => {
    // Get the page number
    const pageNum = req.query.pageNum;
    const filter = req.query.filter.toLowerCase();
    const keywords = req.query.keywords;
    const keywordsArr = keywords.split(':');
    let totalItems = 0;
    const objPerPage = 6;
    const skip = (+pageNum - 1) * objPerPage;
    const end = skip + objPerPage;

    // Fetch from database
    let data = [];
    let tempData;

    if (keywordsArr.length >= 2 && keywordsArr[1] === 'tiai') {
        // fetch with id
        data = await guestpert.fetchGuestpertwithIdLimitedInfo(keywordsArr[0]);
    } else if (keywordsArr.length === 1) {
        if (keywords.length === 0) {
            if (filter === 'all') {
                tempData = await guestpert.fetchGuestpertFilterAll();
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            } else if (filter === 'platinum') {
                tempData = await guestpert.fetchGuestpertFilterFunc('platinum');
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            } else if (filter === 'elite') {
                tempData = await guestpert.fetchGuestpertFilterFunc('elite');
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            } else if (filter === 'standard') {
                tempData = await guestpert.fetchGuestpertFilterFunc('standard');
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            } else if (filter === 'produced reels') {
                tempData = await guestpert.fetchGuestpertFilterReels();
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            } else if (filter === 'expertise') {
                tempData = await guestpert.fetchGuestpertFilterExpertise();
                totalItems = tempData.length;
                data = tempData.slice(skip, end)
            }
        } else {

            tempData = await guestpert.fetchAllGuestpertbyKeywordLimitedInfo(keywords);
            totalItems = tempData.length;
            data = tempData.slice(skip, end)
        }
    }
    !data ? data = [] : '';

    // Send Output
    res.status(200).json({
        dataArr: data,
        pageNum,
        totalItems
    });
}

//////////////////////////////////////////
////--- Filter Topics From Home-------
//////////////////////////////////////////

exports.searchTopicsFromHome = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            return res.redirect('/');
        }

        let keyword = req.body['hotTopic-keyword']
        const id = req.body['hotTopic-id'];

        if (id && id.length > 0) {
            topicId = id.split(':')[0];
            return res.redirect(`/readTopic/${topicId}`)
        }

        const allHotTopic = await hotTopics.fetchAllHotTopicWithKeywordLimitedInfo(keyword);
        if (allHotTopic.length === 0) {
            return res.redirect('/hotTopics')
        }
        let tempAuthors = [];
        allHotTopic.forEach(c => {
            const fullName = `${c.authFirstName} ${c.authLastName}`;
            tempAuthors.push(fullName);
        })

        const allAuthors = [...new Set(tempAuthors)]

        res.render('templates/front/catagory_hotTopic', {
            pageTitle: 'hot topics',
            allHotTopic,
            allAuthors
        });
    } catch (err) {
        next(err)
    }
}

//////////////////////////////////////////
////--- Filter Guestpert From Home-------
//////////////////////////////////////////

exports.SearchGuestpertFromHome = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            return res.redirect('/');
        }
        let keyword = req.body['guestpert-keyword']
        const id = req.body['guestpert-id'];
        if (id) {
            return res.redirect('/guestperts/' + id.split(':')[0])
        }

        res.render('templates/front/guestpert', {
            id, keyword
        })
    } catch (err) {
        next(err)
    }
}


//////////////////////////////////////////
////--- Export searchAllGuestpert ---////
//////////////////////////////////////////
exports.getAllGuestpert = async (req, res, next) => {


    // Fetch from database
    let data = await guestpert.fetchAllGuestpertUltraLimited();

    // Send Output
    res.status(200).json({
        dataArr: data
    });
}

//////////////////////////////////////////////
////--- Search bar to find guestperts ---////
////////////////////////////////////////////
exports.searchguestpertAutocomplete = async (req, res, next) => {

    // Get the Keywords
    const keywords = req.params;

    // Fetch from Server
    const data = await guestpert.fetchAllGuestpertbyKeyword(keywords);
    let obj = {
        data
    };

    // Send data to Client
    res.status(200).json(obj);
}

//////////////////////////////////////////////
////--- Get Guestperts personal page ---////
////////////////////////////////////////////

exports.getGuestpertsPersonalPage = async (req, res, next) => {
    try {

        const { id } = req.params;
        if (!id) {
            res.redirect('/guestperts')
        }

        const guestpertDetail = await guestpert.fetchAGuestpertWithIdLongList(id);
        if (!guestpertDetail[0]) {
            return res.redirect('/guestperts')
        }

        const upcomingAppearance = [];
        const priorAppearance = [];
        guestpertDetail[0].allMedia.forEach(c => {
            const appDate = new Date(c.appearanceDate);
            if (appDate > new Date()) {
                upcomingAppearance.push(c)
            } else {
                priorAppearance.push(c)
            }
        })

        res.render('templates/front/guestpert_personal', {
            guestpert: guestpertDetail[0],
            upcomingAppearance, priorAppearance
        })
    } catch (err) {
        next(err)
    }
}



//////////////////////////////////////////////
////--- Search bar to find hotTopics ---////
////////////////////////////////////////////
exports.searchHotTopicAutocomplete = async (req, res, next) => {

    // Get the Keywords
    const keywords = req.params;
    const data = await hotTopics.fetchAllTopicbyKeyword(keywords)

    // Fetch from Server
    let obj = {
        data
    };

    // Send data to Client
    res.status(200).json(obj);
}


//////////////////////////////////////////////
////--- Get Hot Topics Categories page ---////
////////////////////////////////////////////

exports.getHotTopicCatPage = async (req, res, next) => {
    try {
        const categories = await hotTopicCategory.fetchAllActiveCategories();

        res.render('templates/front/hot_topics', {
            categories
        })

    } catch (err) {
        next(err)
    }
}

//////////////////////////////////////////
////--- Search Hot Topics by tags ---////
////////////////////////////////////////

exports.searchHotTopicsByTags = async (req, res, next) => {
    try {

        const { tags } = req.query;
        if (!tags || tags.length === 0) {
            return res.redirect('/hotTopics')
        }

        const tempAuthors = [];
        const allHotTopic = await hotTopics.fetchTopicByTag(tags.toLowerCase());

        if (allHotTopic.length === 0) {
            return res.redirect('/hotTopics')
        }

        allHotTopic.forEach(c => {
            const fullName = `${c.authFirstName} ${c.authLastName}`;
            tempAuthors.push(fullName);
        })

        const allAuthors = [...new Set(tempAuthors)]

        res.render('templates/front/catagory_hotTopic', {
            pageTitle: 'hot topics',
            allHotTopic,
            allAuthors
        });
    } catch (err) {
        next(err)
    }
}

////////////////////////////////////
////--- All Hot Topics Page ---////
///////////////////////////////////

exports.getAllHotTopicsPage = async (req, res, next) => {
    try {
        const { title } = req.params;
        if (!title) {
            return res.redirect('/hotTopics')
        }
        const tempAuthors = [];
        const allHotTopic = await hotTopics.fetchAllTopicLimitedInfo(title);

        if (allHotTopic.length === 0) {
            return res.redirect('/hotTopics')
        }

        allHotTopic.forEach(c => {
            const fullName = `${c.authFirstName} ${c.authLastName}`;
            tempAuthors.push(fullName);
        })

        const allAuthors = [...new Set(tempAuthors)]

        res.render('templates/front/catagory_hotTopic', {
            pageTitle: title,
            allHotTopic,
            allAuthors
        });
    } catch (err) {
        next(err)
    }
}


////////////////////////////////
////--- Read Topic Page ---////
///////////////////////////////

exports.getReadTopicPage = async (req, res, next) => {
    try {

        const { id } = req.params;

        const idValidation = validator.isMongoId(id);

        if (!id || !idValidation) {
            return res.redirect('/hotTopics')
        }

        const hotTopic = await hotTopics.fetchReadTopic(id);
        if (!hotTopic) {
            return res.redirect('/hotTopics')
        }

        res.render('templates/front/content_hotTopics', {
            hotTopic
        })
    } catch (err) {
        next(err)
    }
}

////////////////////////////////
////--- Get About Page ---////
///////////////////////////////

exports.getAboutPage = async (req, res, next) => {
    try {

        const team = await teamMembers.getAllMember();
        const primeTeam = []
        const otherTeam = []

        team.filter(c => {
            if (c.memberType === 'prime') { primeTeam.push(c) }
            else if (c.memberType === 'others') { otherTeam.push(c) }
        });
        res.render('templates/front/about', { primeTeam, otherTeam })
    } catch (err) {
        next(err)
    }
}


//////////////////////////
////--- Read Blog ---////
////////////////////////

exports.readBlog = async (req, res, next) => {
    try {
        const { id } = req.params;

        const idValidation = validator.isMongoId(id);
        if (!idValidation) {
            return res.redirect('/')
        }

        const blog = await blogs.fetchABlog(id);
        if (!blog) {
            return res.redirect('/')
        }

        blog.created = dateFormat(blog.createdAt, 'dd mmm yyyy')
        res.render('templates/front/blog', {
            blog
        })

    } catch (err) {
        next(err)
    }
}

////////////////////////////////
////--- Get Founders Page ---////
///////////////////////////////

exports.getFoundersPage = async (req, res, next) => {
    try {
        const blog = req.query.blog;
        const founder = await founders.getFounder();

        res.render('templates/front/jaquiesDetails', { founder, blog })
    } catch (err) {
        next(err)
    }
}

////////////////////////////////
////--- Get Service Page ---////
///////////////////////////////

exports.getServicePage = async (req, res, next) => {
    try {

        const service = await services.fetchAllActiveService();
        res.render('templates/front/services', { services: service })

    } catch (err) {
        next(err)
    }
}

////////////////////////////////
////--- Get Shop Page ---////
///////////////////////////////

exports.getShopPage = async (req, res, next) => {
    try {
        let pageNum = req.query.pageNum ? +req.query.pageNum : 1
        !pageNum ? pageNum = 1 : ''
        const totalItems = 12
        const allProducts = await products.fetchAllActiveProds(pageNum, totalItems);
        const allProductCount = await products.activeProdCount()

        res.render('templates/front/shop', { products: allProducts, activePage: pageNum, totalItems: allProductCount })
    } catch (err) {
        next(err)
    }
}

/////////////////////////////////////////
////--- Get Product Details Page ---////
///////////////////////////////////////
exports.getProductDetailsPage = async (req, res, next) => {
    try {

        const { id } = req.params;

        const idValidation = validator.isMongoId(id)
        const errMessage = req.flash('errMessage')[0];
        const succMessage = req.flash('succMessage')[0];

        if (!id || !idValidation) {
            return res.redirect('/shop');
        }

        const prod = await products.fetchAnActiveProduct(id);
        if (!prod) {
            return res.redirect('/shop');
        }

        res.render('templates/front/product_details', { prod, errMessage, succMessage })

    } catch (err) {
        next(err)
    }
}

////////////////////////////////
////--- Get Faq Page ---////
///////////////////////////////

exports.getFaqPage = async (req, res, next) => {
    try {

        const faq = await faqs.fetchAllActiveFaq();

        res.render('templates/front/faq', { faqs: faq })

    } catch (err) {
        next(err)
    }
}


////////////////////////////////
////--- Get Contact Page ---////
///////////////////////////////

exports.getContactPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        const succMessage = req.flash('succMessage')[0];
        const contactPage = await contact.getContactPage();

        res.render('templates/front/contact', { contactPage, errMessage, succMessage })
    } catch (err) {
        next(err)
    }
}


exports.getTermsOfUsePage = (req, res, next) => {
    try {
        res.render('templates/front/termsAndCondition')
    } catch (error) {
        next(error)
    }
}

exports.getPrivacyPolicyPage = (req, res, next) => {
    try {
        res.render('templates/front/privacyPolicy')
    } catch (error) {
        next(error)
    }
}



exports.sendContactMail = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Something went wrong. Please review your input fields');
            return res.redirect('/contact');
        }

        const { name, email, phone, comment } = req.body

        const emailTemplate = await emails.fetchAEmail(email_ids.contactFormMessage);

        const msg = {
            to: process.env.ADMIN_RECEIVING_EMAIL,
            from: {
                name: process.env.ADMIN_MAILING_NAME,
                email: process.env.ADMIN_SENDING_EMAIL,
            },
            subject: emailTemplate.subject,
            html: emailTemplate.emailContent
                .replace(/\[name\]/g, `${name}`)
                .replace(/\[email\]/g, `${email}`)
                .replace(/\[phone\]/g, `${phone}`)
                .replace(/\[comment\]/g, `${comment}`)

        }

        sgMail.send(msg)

        req.flash('succMessage', 'Your message have been successfully sent');
        return res.redirect('/contact');
    } catch (error) {
        next(error)
    }
}


exports.registerOrder = async (req, res, next) => {

    try {
        const validationErr = validationResult(req);
        const { productId } = req.query

        if (validationErr.errors.length > 0 || !productId) {
            req.flash('errMessage', 'Something went wrong. Please review your input fields');
            return res.redirect('/product/'+productId);
        }

        const productDetail = await products.fetchAProduct(productId)
        if (productDetail.paymentMode !== 'order') {
            req.flash('errMessage', 'Something went wrong. Please try again');
            return res.redirect('/product/'+productId);
        }

        const { name, email, phone, address, promo } = req.body

        const emailTemplate = await emails.fetchAEmail(email_ids.orderReceived);

        const msg = {
            to: process.env.ADMIN_RECEIVING_EMAIL,
            from: {
                name: process.env.ADMIN_MAILING_NAME,
                email: process.env.ADMIN_SENDING_EMAIL,
            },
            subject: emailTemplate.subject,
            html: emailTemplate.emailContent
                .replace(/\[name\]/g, `${name}`)
                .replace(/\[email\]/g, `${email}`)
                .replace(/\[phone\]/g, `${phone}`)
                .replace(/\[address\]/g, `${address}`)
                .replace(/\[promo\]/g, `${promo}`)
                .replace(/\[productName\]/g, `${productDetail.title}`)

        }

        sgMail.send(msg)
        req.flash('succMessage', 'Your order have been successfully placed');
        return res.redirect('/product/'+productId);

    } catch (error) {
        next(error)
    }
}

exports.fetchAllActiveTestimonials = async (req, res, next)=>{

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        
        // Get data from database
        const data = await testimonials.fetchActiveTestimonials(pageNum);
        const totalItems = await testimonials.testimonialsCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}