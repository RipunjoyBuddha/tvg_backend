const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const dateFormat = require('dateformat');
const uniqid = require('uniqid');
const mongo = require('mongodb');
const sgMail = require('@sendgrid/mail');
const paypal = require('paypal-rest-sdk');
const addDays = require('add-days')

paypal.configure({
    'mode': process.env.PAYPAL_MODE, //sandbox or live
    'client_id': process.env.PAYPAL_CLIENT,
    'client_secret': process.env.PAYPAL_SECRET
});

sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);

const guestperts = require('../models/guestperts.models');
const blogs = require('../models/blogs.models');
const hotTopicCategory = require('../models/hotTopicCategory.models');
const tags = require('../models/tags.models');
const temp = require('../models/temp.models');
const hotTopics = require('../models/hotTopics.models');
const books = require('../models/books.models');
const reels = require('../models/reels.models');
const media = require('../models/media.models');
const tempLinks = require('../models/tempLinks.models');
const emails = require('../models/email.models');
const tvgGpannelDashboard = require('../models/tvgGpannelDashboard.models');
const products = require('../models/products.models')
const tempOrders = require('../models/tempOrders.models')
const orders = require('../models/orders.models');
const membership = require('../models/membership.models');
const users = require('../models/users.models');
const enquiries = require('../models/enquiries.models');

const fileHelper = require('../util/file');
const roles = require('../util/roles');
const email_Ids = require('../util/email');
const createHash = require('../util/randomHash');
const membershipOrderCode = require('../util/membership');

/////////////////////////////
///--- Rest Functions ---///
///////////////////////////

// Edit Guestperts Blogs -----------
exports.editGuestpertsBlogs = async (req, res, next) => {
    try {
        // Get id and store it
        const id = req.query.id;

        if (!id) {
            // Send error report
            return new Error('Invalid blog ID');
        }

        // Get data from server
        const blog = await blogs.fetchABlogForEditing(id);

        if (!blog) {
            throw new Error('No such blog present')
        }

        // send object
        res.status(200).json(blog);
    }
    catch (err) {
        res.status(404).json({ msg: 'server error' });
    }
}



// Edit Guestperts Media -----------
exports.fetchMedia = async (req, res, next) => {

    // Get id and store it
    const id = req.query.id;

    if (!id) {
        // Send error report
        return new Error('Invalid blog ID');
    }

    const obj = await media.fetchMediaById(id);

    // send object
    res.status(200).json(obj);
}


// Search Tag List -----------
exports.searchTagList = async (req, res, next) => {
    // Get the Keywords
    const keywords = req.query.tag;

    // Fetch from Server
    let tagList = await tags.fetchParticularTags(1, keywords);

    const data = tagList.map(c => c.keyword);

    // send object
    res.status(200).json(data);
}

///////////////////////////////
//--- Guestpert Books ---//
/////////////////////////////

exports.fetchGuestpertBooksPage = async (req, res, next) => {
    try {
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }
        const guestpertId = authorId;
        const errMessage = req.flash('errMessage')[0];
        const allBooks = await books.fetchAuthorBooks(authorId);
        allBooks.forEach(c => {
            c.createdAt = dateFormat(c.createdAt, 'd mmm yyyy')
            c.updatedAt = dateFormat(c.updatedAt, 'd mmm yyyy')
        })

        res.render('./templates/guestpertPannel/ga_books', {
            guestpertId, errMessage, allBooks
        })
    } catch (err) {
        next(err)
    }
}

exports.getOrdersPage = async (req, res, next) => {
    try {

        const userId = req.session.user._id;
        const userOrders = await orders.fetchOrderByUserId(userId)

        userOrders.forEach(c => {
            c.purchaseDate = dateFormat(c.purchaseDate, 'dS mmm yyyy, h:MM TT')
        })

        res.render('./templates/guestpertPannel/ga_orders', {
            userOrders
        })
    } catch (err) {
        next(err)
    }
}


exports.getMembershipPage = async (req, res, next) => {
    try {

        const features = await membership.fetchFeatures();
        const memberships = await membership.fetchMemberships();
        const currentGuestpertMembership = await guestperts.fetchGuestpertMembershipInfo(req.session.user._id);
        const currentMembership = new Date() < new Date(currentGuestpertMembership.membershipExpiryDate)? currentGuestpertMembership.membership:null;

        res.render('./templates/guestpertPannel/ga_membership', {
            features, memberships, currentMembership
        })

    } catch (err) {
        next(err)
    }
}


exports.deleteBook = async (req, res, next) => {
    try {
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }
        const { bookId } = req.body;
        const prevBook = await books.fetchABook(bookId);

        if (!prevBook || authorId.toString() !== prevBook.authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to delete this book')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }

        const delStats = await books.deleteBook(bookId)
        if (delStats.deletedCount < 1) {
            req.flash('errMessage', 'Deleting failed. Please try again later')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
        }

        fileHelper.delFile(prevBook.imageUrl);

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/books');
    } catch (err) {
        next(err)
    }
}

exports.editBook = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let authorId;
        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            if (imageUrl) { fileHelper.del(imageUrl) };
            // Send Invalid Inputs page page
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }

        if (!authorId) {
            req.flash('errMessage', 'Could not find authorId. Please try again')
            if (imageUrl) { fileHelper.del(imageUrl) };
            // Send Invalid Inputs page
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }

        const { id } = req.query;
        const prevBook = await books.fetchABook(id);

        if (prevBook && prevBook.authorId.toString() !== authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to edit this book')
            if (imageUrl) { fileHelper.del(imageUrl) };
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }

        const finalObj = { ...req.body, active: req.body.active ? true : false };
        if (imageUrl) {
            finalObj.imageUrl = imageUrl
        }

        const updateBook = await books.editBook(id, finalObj);
        if (updateBook.modifiedCount < 1) {
            req.flash('errMessage', 'You are not authorized to delete this book')
        }

        if (imageUrl) { fileHelper.delFile(prevBook.imageUrl) };

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/books');
    } catch (err) {
        next(err)
    }
}

exports.fetchGuestpertBook = async (req, res, next) => {
    try {

        const id = req.query.id;

        const bookDetails = await books.fetchABook(id);

        res.json(bookDetails)

    } catch (err) {
        next(err)
    }
}

exports.createBook = async (req, res, next) => {
    try {
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');
        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0 || !imageUrl) {
            if (!imageUrl) {
                req.flash('errMessage', 'Please attach an image')
            } else {
                req.flash('errMessage', 'Invalid inputs')
            }
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            // Send Invalid Inputs page

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }


        if (!authorId) {
            req.flash('errMessage', 'author Id is not provided')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/guestpert/books');
        }

        const finalObj = { ...req.body, authorId: mongo.ObjectID(authorId), imageUrl };

        const createBook = new books(finalObj);
        const createStats = await createBook.save();
        if (createStats.insertedCount < 1) {
            req.flash('errMessage', 'could\'t create book cover . Please try again')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/books');
        }
        const bookId = createStats.insertedId;
        const booksRes = await guestperts.createBook(authorId, bookId);

        if (booksRes.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot create book . Please Try again')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/books?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/books');
    } catch (err) {
        next(err)
    }
}


///////////////////////////////
//--- Guestpert HotTopic ---//
/////////////////////////////

// Edit Guestperts Hot Topics -----------
exports.fetchAHotTopic = async (req, res, next) => {
    try {
        // Get id and store it
        const id = req.query.id;

        if (!id) {
            // Send error report
            return new Error('Invalid blog ID');
        }

        // Get data from server
        const obj = await hotTopics.fetchById(id);

        // send object
        res.status(200).json(obj);
    }
    catch (err) {
        res.status(404).json({ msg: 'could\'t fetch data. please try again' });
    }
}

exports.deleteHotTopic = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const { delId } = req.body;
        let guestpertId;

        if (req.session.user.role === roles.guestpert) {
            guestpertId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            guestpertId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0 || !delId) {
            req.flash('errMessage', 'Couldn\'t delete hotTopic. Please try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${guestpertId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        const hotTopic = await hotTopics.fetchById(delId);
        if (!hotTopic || hotTopic.authorId.toString() !== guestpertId.toString()) {
            req.flash('errMessage', 'Couldn\'t delete hotTopic. Please try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${guestpertId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        const { imgArr } = hotTopic;

        const delStats = await hotTopics.delHotTopic(delId);
        if (delStats.deletedCount < 1) {
            req.flash('errMessage', 'Couldn\'t delete hotTopic. Please try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${guestpertId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        imgArr.forEach(c => {
            fileHelper.delFile(c)
        })

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${guestpertId}`)
        }
        res.redirect('/guestpert/hotTopics');
    } catch (err) {
        next(err)
    }
}

exports.fetchManageGuestpertHotTopicsPage = async (req, res, next) => {
    try {
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        const allCategories = await hotTopicCategory.fetchAllActiveCategories();
        const allHotTopics = await hotTopics.fetchAuthorTopics(authorId);
        const errMessage = req.flash('errMessage')[0];

        allHotTopics.forEach(c => {
            c.createdAt = dateFormat(c.createdAt, 'd mmm yyyy')
            c.updatedAt = dateFormat(c.updatedAt, 'd mmm yyyy')
        })

        res.render('./templates/guestpertPannel/ga_hotTopicsSettings', {
            allCategories,
            hotTopics: allHotTopics,
            errMessage,
            guestpertId: authorId
        })
    } catch (err) {
        next(err)
    }
}

exports.delTempArray = async (req, res, next) => {
    try {
        const id = req.query.tempId;
        const tempArr = await temp.getTemp(id);
        await temp.delTemp(id);
        if (tempArr) {
            tempArr.imgArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }
        res.json({ success: true })
    } catch (err) {
        res.status(404).json({ success: false })
    }
}

exports.editHotTopic = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const hotTopicId = req.query.hotTopicId;

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0 || !hotTopicId) {
            req.flash('errMessage', 'Invalid inputs, please reffer the instructions')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }


        const prevHotTopic = await hotTopics.fetchById(hotTopicId);
        if (!prevHotTopic || prevHotTopic.authorId.toString() !== authorId.toString()) {
            req.flash('errMessage', 'Couldn\'t delete hotTopic. Please try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        const tempArrId = req.query.tempId;
        const tempImgArr = await temp.getTemp(tempArrId);

        finalTempImgArr = [...prevHotTopic.imgArr];
        if (tempImgArr) {
            finalTempImgArr = [...prevHotTopic.imgArr, ...tempImgArr.imgArr]
        }

        let { content, tags } = req.body;

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        imgArr = finalTempImgArr.filter(c => {
            if (content.includes(c)) {
                return c;
            } else {
                trashArr.push(c)
            }
        })

        // refurnish tags
        tags = tags.split(' ').filter(c => { if (c !== ' ' || c !== '') { return c } });

        const finalObj = {
            ...req.body,
            imgArr,
            tags,
            active: req.body.active ? true : false
        };

        const result = await hotTopics.updateHotTopic(hotTopicId, finalObj);
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t update hotTopic. Please try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        if (trashArr.length > 0) {
            trashArr.forEach(c => fileHelper.delFile(c));
        }
        temp.delTemp(tempArrId);

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/hotTopics');
    } catch (err) {
        next(err)
    }
}


exports.createHotTopic = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please reffer the instructions')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        const tempArrId = req.query.tempId;
        const tempImgArr = await temp.getTemp(tempArrId);

        let { content, tags } = req.body;

        // refurnish tags
        const spaceChck = RegExp(/^[\s]*$/g)
        tags = tags.split(' ').filter(c => { if (!spaceChck.test(c) && typeof(c)== 'string') { return c} });

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        if (tempImgArr) {
            imgArr = tempImgArr.imgArr.filter(c => {
                if (content.includes(c)) {
                    return c;
                } else {
                    trashArr.push(c)
                }
            })
        };

        const finalObj = { ...req.body, authorId: mongo.ObjectID(authorId), imgArr, tags };

        const create = new hotTopics(finalObj);
        const createState = await create.save();

        if (!createState || !createState.insertedId) {
            req.flash('errMessage', 'Couldnot create hotTopic . Please Try again')

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/hotTopics');
        }

        const hotTopicId = createState.insertedId;
        const hotTopicRes = await guestperts.createHotTopic(authorId, hotTopicId);

        if (hotTopicRes.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot create hotTopic . Please Try again')
            return res.redirect('/guestpert/hotTopics');
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => fileHelper.delFile(c));
        }

        temp.delTemp(tempArrId)

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/hotTopics?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/hotTopics');
    } catch (err) {
        next(err)
    }
}


exports.uploadHotTopicsImage = async (req, res, next) => {
    try {
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (mimeTypeErr.length > 0 || !imageUrl) {

            fileHelper.delFile(imageUrl);
            throw new Error();
        }

        const tempId = req.query.tempId;

        // Get previous image Array
        const prev = await temp.getTemp(tempId);

        if (!prev) {
            // If prev dont exist run create new temp Array
            const newTemp = new temp(tempId, imageUrl);
            await newTemp.save();
        } else {
            // If prev exist update the temp Array
            await temp.updateTemp(tempId, imageUrl)
        }

        res.status(200).json({ location: imageUrl })
    } catch (err) {
        res.status(404).json({ msg: 'error in uploading' })
    }
}

///////////////////////////
//--- Guestpert Blogs ---//
///////////////////////////



exports.editGuestpertBlogs = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs. Please check the instructions.')

            if (imageUrl) { fileHelper.delFile(imageUrl) };
            // Send Invalid Inputs page
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/blog');
        }

        const blogId = req.query.id;
        const tempId = req.query.tempId;

        const prevBlog = await blogs.fetchABlogForEditing(blogId);
        
        if (!prevBlog || authorId.toString() != prevBlog.authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to make changes to this blog')

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/blog');
        }

        const tempArr = await temp.getTemp(tempId);
        
        let tempImgArr = [];
        if (tempArr) { tempImgArr = tempArr.imgArr }

        // Make an image array
        if(!prevBlog.imgArr){
            prevBlog.imgArr = []
        }
        const { title, content, active } = req.body;
        const mixImgArr = [...prevBlog.imgArr, ...tempImgArr];
        
        if (imageUrl) {
            finalObj.imageUrl = imageUrl;
        }
        
        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        imgArr = mixImgArr.filter(c => {
            if (content.includes(c)) {
                return c;
            } else {
                trashArr.push(c)
            }
        })

        const finalObj = { 
            title, content,
            activeStats: active ? true : false ,
            imgArr
        }

        if (imageUrl) {
            finalObj.imageUrl = imageUrl
        }

        const update = await blogs.updateBlog(blogId, finalObj);
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Could not update blog. Please try again')
        }

        if (imageUrl) { fileHelper.delFile(prevBlog.imageUrl) };
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        if (tempImgArr) {
            temp.delTemp(tempId);
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/blog');
    } catch (err) {
        next(err)
    }
}



exports.delTempBlogsArr = async (req, res, next) => {
    try {

        const tempId = req.query.tempId;

        const prev = await temp.getTemp(tempId);
        
        if(prev && prev.imgArr){
            prev.imgArr.forEach(c=>{
                fileHelper.delFile(c)
            })
        }

        // Delete in temp database
        await temp.delTemp(tempId)

        // Send success message
        res.status(200).json({ success: true })

    } catch (err) {
        // Send error message
        res.status(404).json({ success: false })
    }
}


exports.uploadBlogPic = async (req, res, next) => {
    try {
        const mimeTypeErr = req.flash('passOnMsg');
        const image = req.file;
        let imagePath = null;

        if (image) {
            imagePath = `/${image.path}`;
        }

        if (mimeTypeErr.length > 0 || !imagePath) {
            // Send Invalid JSON response
            throw new Error();
        }

        const tempId = req.query.tempId;

        // Get previous image Array
        const prev = await temp.getTemp(tempId);

        if (!prev) {
            // If prev dont exist run create new temp Array
            const newTemp = new temp(tempId, imagePath);
            await newTemp.save();
        } else {
            // If prev exist update the temp Array
            await temp.updateTemp(tempId, imagePath)
        }

        res.status(200).json({ location: imagePath })
    } catch (err) {
        
        res.status(404).json({ msg: 'error in uploading' })
    }
}



exports.createGuestpertBlogs = async (req, res, next) => {
    try {

        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let guestpertId;

        if (req.session.user.role === roles.guestpert) {
            guestpertId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            guestpertId = req.query.guestpertId;
        }

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0 ) {
            
            req.flash('errMessage', 'Invalid inputs. Please try again.')
            if (imageUrl) { fileHelper.delFile(imageUrl) };

            // Send Invalid Inputs page
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${guestpertId}`)
            }
            return res.redirect('/guestpert/blog');
        }


        const { title, content, active } = req.body;
        const tempId = req.query.tempId;

        // Get array from tempId
        const tempImgArr = await temp.getTemp(tempId);

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        if (tempImgArr) {
            imgArr = tempImgArr.imgArr.filter(c => {
                if (content.includes(c)) {
                    return c;
                } else {
                    trashArr.push(c)
                }
            })
        };

        const create = new blogs(guestpertId, title, content, active, imageUrl, imgArr);
        const result = await create.save();

        // Check if inserting to blogs is successful
        if (!result || !result.insertedId) {
            req.flash('errMessage', 'Error have been occoured in creating blog. Try again later');
            if (imageUrl) { fileHelper.delFile(imageUrl) };

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${guestpertId}`)
            }
            return res.redirect('/guestpert/blog');
        }
        const blogId = result.insertedId;

        const blogRes = await guestperts.createBlog(guestpertId, blogId);

        if (!blogRes.modifiedCount > 0) {
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            req.flash('errMessage', 'Error have been occoured in creating blog. Try again later');
        }


        // Delete Trash
        if (tempImgArr) {
            temp.delTemp(tempId);
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${guestpertId}`)
        }
        res.redirect('/guestpert/blog');
    } catch (err) {
        next(err)
    }
}



exports.getGuestpertBlogPage = async (req, res, next) => {
    try {
        let guestpertId;

        if (req.session.user.role === roles.guestpert) {
            guestpertId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            guestpertId = req.query.guestpertId;
        }

        // Fetch All blogs
        const allBlogs = await blogs.fetchBlogsWithAuthorId(guestpertId);

        allBlogs.forEach(c => {
            c.createdAt = dateFormat(c.createdAt, 'd mmm yyyy')
            c.updatedAt = dateFormat(c.updatedAt, 'd mmm yyyy')
        })

        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/guestpertPannel/ga_blog', {
            errMessage,
            blogs: allBlogs,
            guestpertId
        })
    } catch (err) {
        next(err)
    }
}



exports.deleteGuestpertBlog = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'blog Id not valid')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/blog');
        }

        const { blogId } = req.body;
        const blog = await blogs.fetchABlog(blogId);
        

        if (authorId.toString() !== blog.authorId.toString()) {
            req.flash('errMessage', 'You are unauthorized to delete this blog')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/blog');
        }

        const delResult = await blogs.delBlog(blogId);
        if (delResult.deletedCount < 1) {
            req.flash('errMessage', 'unable to delete the blog. please try again')
        }

        fileHelper.delFile(blog.imageUrl)
        if(blog.imgArr){
            blog.imgArr.forEach(c=>{
                fileHelper.delFile(c)
            })
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/blogs?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/blog');
    } catch (err) {
        next(err)
    }
}


///////////////////////////
//--- Dashboard ---//
///////////////////////////

exports.getGuestpertDashboard = async (req, res, next) => {
    try {
        const { _id } = req.session.user;
        const { videoLink } = await tvgGpannelDashboard.fetchGPannelDashboard();
        const guestpertDetails = await guestperts.fetchAGuestpert(_id);
        if (!guestpertDetails) {
            throw new Error()
        }

        const { imgSrc, firstName, lastName, prefix, suffix, title, primaryExpertise, expandedExpertise } = guestpertDetails;
        const verificationStatus = req.session.verificationStats;

        res.render('./templates/guestpertPannel/ga_dashboard', {
            verificationStatus, imgSrc, firstName, lastName, prefix, suffix, title, primaryExpertise, expandedExpertise,
            videoLink
        })

    } catch (err) {
        next(err)
    }
}


exports.getGuestpertProfileSettings = async (req, res, next) => {
    try {
        const { _id } = req.session.user;

        const errMessage = req.flash('errMessage')[0];

        const guestpertDetails = await guestperts.fetchAGuestpert(_id);
        if (!guestpertDetails) {
            throw new Error()
        }

        res.render('./templates/guestpertPannel/ga_profileSettings',
            {
                ...guestpertDetails,
                errMessage
            }
        )
    } catch (err) {
        next(err)
    }
}


//////---****** Edit Guestpert Profile ********----/////

exports.editGuestpertCredentials = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const errMessage = [...validationErr.errors];

        const { newPassword, confirmPassword, oldPassword } = req.body;

        if (newPassword !== confirmPassword) {
            errMessage.push({ msg: 'Password and Confirm Password don\'t match' })
        }


        const id = req.session.user._id;

        const { password } = await guestperts.fetchAGuestpert(id);

        const oldPasswordChck = await bcrypt.compare(oldPassword, password);
        if (!oldPasswordChck) {
            errMessage.push({ msg: 'incorrect old password' });
        }

        if (errMessage.length > 0) {

            req.flash('errMessage', errMessage[0].msg)
            // Send Invalid Inputs page
            return res.redirect('/guestpert/profileSettings');
        }

        const hashedPassword = await bcrypt.hash(req.body.newPassword, 12);

        const finalObj = {
            password: hashedPassword,
        }

        const result = await guestperts.updateGuestpert(id, finalObj);
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot update guestpert. Please try again');
        }

        res.redirect('/guestpert/profileSettings');

    } catch (err) {
        next(err)
    }
}


exports.editGuestpertCommon = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/guestpert/profileSettings');
        }

        const guestpertId = req.session.user._id;
        const result = await guestperts.updateGuestpert(guestpertId, { ...req.body });

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem occoured with updating guestpert. Please try again')
        }

        res.redirect('/guestpert/profileSettings');
    } catch (err) {
        next(err)
    }
}


exports.editGuestpertProfilePic = async (req, res, next) => {
    try {

        const mimeTypeErr = req.flash('passOnMsg');
        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (!imageUrl) {
            return res.redirect('/guestpert/profileSettings');
        }

        if (mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')

            if (imageUrl) { fileHelper.delFile(imageUrl) };
            // Send Invalid Inputs page
            return res.redirect('/guestpert/profileSettings');
        }

        const guestpertId = req.session.user._id;

        const prevInfo = await guestperts.fetchAGuestpert(guestpertId);
        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a guestpert with the attached id');
            fileHelper.delFile(imageUrl);
            return res.redirect('/guestpert/profileSettings');
        }


        const result = await guestperts.updateGuestpert(guestpertId, { imgSrc: imageUrl });
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')
            fileHelper.delFile(imageUrl)
            return res.redirect('/guestpert/profileSettings');
        }

        fileHelper.delFile(prevInfo.imgSrc);
        res.redirect('/guestpert/profileSettings');
    } catch (err) {
        next(err)
    }
}



///////////////////////////////////
/////----- Demo Reel ------ /////
/////////////////////////////////

exports.getManageDemoReelPage = async (req, res, next) => {
    try {

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        const allReels = await reels.fetchReelsByAuthor(authorId);
        const errMessage = req.flash('errMessage')[0]

        res.render('./templates/guestpertPannel/ga_demoReel', {
            guestpertId: authorId,
            errMessage,
            allReels
        })
    } catch (err) {
        next(err)
    }
}

exports.editDemoReel = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please see the requirements and try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }


        const reelId = req.query.reelId;

        const prevReel = await reels.fetchReelById(reelId);
        if (prevReel && prevReel.authorId.toString() != authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to delete this reel')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }

        const finalObj = {
            ...req.body,
            active: req.body.active ? true : false
        }

        const edit = reels.editReel(reelId, finalObj);
        if (edit.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t delete reel. Please try again')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/demoReel');
    } catch (err) {
        next(err)
    }
}


exports.createDemoReel = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please see the requirements and try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }


        const finalObj = {
            ...req.body,
            authorId: mongo.ObjectID(authorId),
        }

        const create = new reels(finalObj);
        const createStats = await create.save();
        if (createStats.insertedCount < 1) {
            req.flash('errMessage', 'Reel couldn\'t be inserted . Please try again later')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }

        const reelId = createStats.insertedId;
        const reelRes = await guestperts.createReel(authorId, reelId);

        if (reelRes.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot create media. Please Try again')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/demoReel');
    } catch (err) {
        next(err)
    }
}

exports.deleteDemoReel = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'invalid Reel Id')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }

        const { reelId } = req.body;
        const prevReel = await reels.fetchReelById(reelId);
        if (!prevReel) {
            req.flash('errMessage', 'No such Reel exist')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');

        }

        if (prevReel.authorId.toString() != authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to delete this reel')

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/demoReel');
        }

        const del = reels.delReelById(reelId);
        if (del.deletedCount < 1) {
            req.flash('errMessage', 'Couldn\'t delete reel. Please try again')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/demoReel?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/demoReel');
    } catch (err) {
        next(err)
    }
}



///////////////////////////////////
/////----- Media ------ /////
/////////////////////////////////


exports.createMedia = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please see the requirements and try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/media');
        }

        let { tags } = req.body;

        // refurnish tags
        tags = tags.split(' ').filter(c => { if (c !== ' ' || c !== '') { return c } });

        const finalObj = { ...req.body, authorId: mongo.ObjectID(authorId), tags };

        const create = new media(finalObj);
        const createState = await create.save();

        if (!createState || !createState.insertedId) {
            req.flash('errMessage', 'Couldnot create media . Please Try again')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/media');
        }

        const mediaId = createState.insertedId;
        const mediaRes = await guestperts.createMedia(authorId, mediaId);

        if (mediaRes.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot create media. Please Try again')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/media');
    } catch (err) {
        next(err)
    }
}

exports.deleteMedia = async (req, res, next) => {
    try {

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        const { delId } = req.body;
        const prevMedia = await media.fetchMediaById(delId);

        if (prevMedia && prevMedia.authorId.toString() != authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to delete this media')
            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/media');
        }

        const delMedia = await media.delMedia(delId);
        if (delMedia.deletedCount < 1) {
            req.flash('errMessage', 'Couldn\'t delete media. try again later')
        }

        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/media');
    } catch (err) {
        next(err)
    }
}


exports.getManageMediaPage = async (req, res, next) => {
    try {
        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }
        const errMessage = req.flash('errMessage')[0];
        const allMedia = await media.fetchMediaByAuthorId(authorId);

        // ---- Refurnish Media
        allMedia.forEach(c => {
            c.createdAt = dateFormat(c.createdAt, 'dd mmm yyyy')
            c.appearanceDate = dateFormat(c.appearanceDate, 'dd mmm yyyy')
        })

        res.render('./templates/guestpertPannel/ga_media', {
            guestpertId: authorId,
            errMessage,
            allMedia
        })
    } catch (err) {
        next(err)
    }
}


exports.editMedia = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let authorId;

        if (req.session.user.role === roles.guestpert) {
            authorId = req.session.user._id;
        } else if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            authorId = req.query.guestpertId;
        }

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please see the requirements and try again')

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/media');
        }


        const mediaId = req.query.id;
        let { tags, active } = req.body;

        const prevMedia = await media.fetchMediaById(mediaId);
        if (prevMedia && prevMedia.authorId.toString() != authorId.toString()) {
            req.flash('errMessage', 'You are not authorized to edit this media')

            if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
                return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
            }
            return res.redirect('/guestpert/media');
        }

        // refurnish tags
        tags = tags.split(' ').filter(c => { if (c !== ' ' || c !== '') { return c } });
        active = active ? true : false;

        const finalObj = { ...req.body, active, tags };

        const editMedia = await media.editMedia(mediaId, finalObj);
        if (editMedia.modifiedCount < 1) {
            req.flash('errMessage', 'Could not update media. Please try again .')
        }
        if (req.session.user.role === roles.subAdmin || req.session.user.role === roles.godMode) {
            return res.redirect(`/admin/editGuestpertOtherFields/media?guestpertId=${authorId}`)
        }
        res.redirect('/guestpert/media');
    } catch (err) {
        next(err)
    }
}


exports.setVerificationToGuestpert = async (req, res, next) => {
    try {

        const { hash } = req.query;

        const fetchHash = await tempLinks.fetchHash(hash);
        if (!fetchHash) {
            return res.redirect('/timeout')
        }

        const {createdAt} = fetchHash;

        console.log(createdAt)

        // const { user, _id } = fetchHash;
        // const update = await guestperts.verifyGuestpert(user);
        // if (update.modifiedCount < 1) {
        //     return res.redirect('/timeout')
        // }

        // req.session.verificationStats = true;
        // await tempLinks.delLink(_id);

        res.redirect('/guestpert/dashboard');

    } catch (err) {
        next(err)
    }
}

// ---------------------------------
// ---- Buy Product

exports.buyProduct = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            return res.redirect('/product');
        }

        const userId = req.session.user._id;
        const { prodId } = req.body;

        const prod = await products.fetchAProduct(prodId)
        if (!prod) {
            return res.redirect(`/product/${prodId}`);
        }

        const { _id, title, price, currency, imgSrc } = prod;

        const product = {
            name: title,
            price: price,
            currency: currency.toUpperCase(),
            quantity: 1
        }

        const tempProd = new tempOrders({ ...product, productId: _id, imgSrc });
        const create = await tempProd.save();
        const tempProductId = create.insertedId;

        const create_payment_json = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": `${process.env.DOMAIN}/guestpert/productPaymentSuccess/${userId}/${tempProductId}`,
                "cancel_url": `${process.env.DOMAIN}/product/${_id}`
            },
            "transactions": [{
                "item_list": {
                    "items": [
                        product
                    ]
                },
                "amount": {
                    "currency": currency.toUpperCase(),
                    "total": price
                }
            }]
        };

        paypal.payment.create(create_payment_json, (error, payment) => {

            if (error) {
                tempOrders.delOrder(tempProductId)
                return res.redirect(`/product/${_id}`)
            } else {
                for (let i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === 'approval_url') {
                        return res.redirect(payment.links[i].href);
                    }
                }
            }
        })

    } catch (err) {
        next(err)
    }
}


exports.captureProdPayment = async (req, res, next) => {
    try {

        const payerId = req.query.PayerID;
        const paymentId = req.query.paymentId;
        const userId = req.params.userId;
        const tempOrderId = req.params.productId;

        const order = await tempOrders.fetchOrder(tempOrderId)
        if (!order) {
            return res.redirect('/product')
        }

        const { name, productId, price, currency, imgSrc } = order

        const execute_payment_json = {
            "payer_id": payerId,
            "transactions": [{
                "amount": {
                    "currency": currency,
                    "total": price
                }
            }]
        };

        paypal.payment.execute(paymentId, execute_payment_json, async (error, payment) => {
            if (error) {
                tempOrders.delOrder(tempOrderId)
                return res.redirect(`/product/${productId}`);
            }

            // Create order in database
            const { create_time, transactions } = payment

            const finalOrder = {
                title: name,
                amount: transactions[0].amount.total,
                paymentStatus: 'paid',
                purchaseDate: create_time,
                address: Object.values(transactions[0].item_list.shipping_address).join(', '),
                userId,
                imgSrc
            }

            const newOrder = new orders(finalOrder);
            const createOrder = await newOrder.save();
            if (createOrder.insertedCount < 1) {
                tempOrders.delOrder(tempOrderId)
                return res.redirect(`/product/${productId}`);
            }

            // Delete Temporary Order
            tempOrders.delOrder(tempOrderId)


            // Send Mails to Guestpert and Admin
            const emailTemplate1 = await emails.fetchAEmail(email_Ids.userProductPurchase);
            const emailTemplate2 = await emails.fetchAEmail(email_Ids.userProductPurchaseToAdmin);
            const userAllDetails = await users.fetchUserById(userId)

            const msg1 = {
                to: userAllDetails.email,
                from: {
                    name: process.env.ADMIN_MAILING_NAME,
                    email: process.env.ADMIN_SENDING_EMAIL,
                },
                subject: emailTemplate1.subject,
                html: emailTemplate1.emailContent
                    .replace('[CUSTOMERFULLNAME]', `${userAllDetails.firstName} ${userAllDetails.lastName}`)
                    .replace('[PRODUCTNAME]', `${transactions[0].item_list.items[0].name}`)
                    .replace('[ORDERNUMBER]' , `${createOrder.insertedId}`)
                    .replace(/\[WEBSITELINK\]/g, `${process.env.DOMAIN}`)
            }

            const msg2 = {
                to: process.env.ADMIN_RECEIVING_EMAIL,
                from: {
                    name: process.env.ADMIN_MAILING_NAME,
                    email: process.env.ADMIN_SENDING_EMAIL,
                },
                subject: emailTemplate2.subject,
                html: emailTemplate2.emailContent
                    .replace('[CUSTOMERFULLNAME]', `${userAllDetails.firstName} ${userAllDetails.lastName}`)
                    .replace('[PRODUCTNAME]', `${transactions[0].item_list.items[0].name}`)
                    .replace('[DATETIME]' , `${dateFormat(create_time, 'dS mmm yyyy, h:MM:ss TT')}`)
                    .replace(/\[WEBSITELINK\]/g, `${process.env.DOMAIN}`)
            }

            sgMail.send(msg1)
            sgMail.send(msg2)

            res.redirect('/guestpert/orders')
        });

    } catch (err) {
        next(err)
    }
}



exports.buyMembership = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const availableMemberships = ['standard', 'elite']
        const { membershipType } = req.body;
        const curGuestpert = req.session.user._id;


        let chck = true;
        if (!membershipType || !availableMemberships.includes(membershipType)) { chck = false };

        if (validationErr.errors.length > 0 || !chck) {
            return res.redirect('/guestpert/membership');
        }

        const guestpertMembershipDetails = await guestperts.fetchGuestpertMembershipInfo(curGuestpert);
        if (!guestpertMembershipDetails) {
            return res.redirect('/guestpert/membership');
        }
        if (guestpertMembershipDetails.membershipOrder && new Date() <= new Date(guestpertMembershipDetails.membershipExpiryDate)) {
            if (guestpertMembershipDetails.membership !== membershipType) {
                return res.redirect('/guestpert/membership');
            }
        }

        const membershipDetails = await membership.fetchMembershipByTitle(membershipType)
        if (!membershipDetails) {
            return res.redirect('/guestpert/membership');
        }
        const { price, _id, membershipTitle } = membershipDetails

        const product = {
            name: `TvGuestpert ${membershipType} membership`,
            price: price,
            currency: 'USD',
            quantity: 1
        }
        const userId = req.session.user._id;
        const tempProd = new tempOrders({ ...product, name: membershipTitle , productId: _id, imgSrc: `/images/${membershipTitle}Membership.svg` });
        const create = await tempProd.save();
        const tempProductId = create.insertedId;

        const create_payment_json = {
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": `${process.env.DOMAIN}/guestpert/membershipPaymentSuccess/${userId}/${tempProductId}`,
                "cancel_url": `${process.env.DOMAIN}/guestpert/membership`
            },
            "transactions": [{
                "item_list": {
                    "items": [
                        product
                    ]
                },
                "amount": {
                    "currency": product.currency,
                    "total": product.price
                }
            }]
        };

        paypal.payment.create(create_payment_json, (error, payment) => {

            if (error) {
                
                tempOrders.delOrder(tempProductId)
                return res.redirect(`/guestpert/membership`)
            } else {
                for (let i = 0; i < payment.links.length; i++) {
                    if (payment.links[i].rel === 'approval_url') {
                        return res.redirect(payment.links[i].href);
                    }
                }
            }
        })

    } catch (err) {
        next(err)
    }
}

exports.captureMembership = async (req, res, next) => {
    try {

        const payerId = req.query.PayerID;
        const paymentId = req.query.paymentId;
        const userId = req.params.userId;
        const tempOrderId = req.params.productId;

        const order = await tempOrders.fetchOrder(tempOrderId)
        if (!order) {
            return res.redirect('/guestpert/membership')
        }
        
        const { name, price, currency, imgSrc } = order

        const execute_payment_json = {
            "payer_id": payerId,
            "transactions": [{
                "amount": {
                    "currency": currency,
                    "total": price
                }
            }]
        };

        paypal.payment.execute(paymentId, execute_payment_json, async (error, payment) => {
            if (error) {
                
                tempOrders.delOrder(tempOrderId)
                return res.redirect(`/guestpert/membership`);
            }

            const guestpertMembershipDetails = await guestperts.fetchAGuestpert(userId);
            if (!guestpertMembershipDetails) {
                return res.redirect('/guestpert/membership');
            }
            
            // Create order in database
            const { create_time, transactions } = payment
            
            const finalOrder = {
                title: transactions[0].item_list.items[0].name,
                amount: transactions[0].amount.total,
                paymentStatus: 'paid',
                purchaseDate: create_time,
                address: Object.values(transactions[0].item_list.shipping_address).join(', '),
                userId,
                imgSrc
            }

            // update Guestperts section by 1 month
            let startDay = new Date(guestpertMembershipDetails.membershipExpiryDate?guestpertMembershipDetails.membershipExpiryDate:null)
            if(startDay < new Date()){
                startDay = new Date()
            }
            const endDay = addDays(startDay, 30)
            
            const updateGuestpertObj = {
                membershipExpiryDate: dateFormat(endDay, 'yyyy-mm-dd'),
                membership: name,
                membershipOrder: membershipOrderCode[name]
            }
            
            const updateGuestpert = await guestperts.updateGuestpert(userId, updateGuestpertObj)

            const newOrder = new orders(finalOrder);
            const createOrder = await newOrder.save();
            if (createOrder.insertedCount < 1 || updateGuestpert.modifiedCount<1) {
                tempOrders.delOrder(tempOrderId)
                return res.redirect(`/guestpert/membership`);
            }

            // Delete Temporary Order
            tempOrders.delOrder(tempOrderId)
            
            // Send Mails to Guestpert and Admin
            const emailTemplate1 = await emails.fetchAEmail(email_Ids.userMembershipPurchase);
            const emailTemplate2 = await emails.fetchAEmail(email_Ids.userProductPurchaseToAdmin);

            const msg1 = {
                to: guestpertMembershipDetails.email,
                from: {
                    name: process.env.ADMIN_MAILING_NAME,
                    email: process.env.ADMIN_SENDING_EMAIL,
                },
                subject: emailTemplate1.subject,
                html: emailTemplate1.emailContent
                    .replace('[CUSTOMERFULLNAME]', `${guestpertMembershipDetails.firstName} ${guestpertMembershipDetails.lastName}`)
                    .replace('[PRODUCTNAME]', `${transactions[0].item_list.items[0].name}`)
                    .replace('[DATETIME]' , `${dateFormat(endDay, 'dS mmm yyyy')}`)
                    .replace(/\[WEBSITELINK\]/g, `${process.env.DOMAIN}`)
            }

            const msg2 = {
                to: process.env.ADMIN_RECEIVING_EMAIL,
                from: {
                    name: process.env.ADMIN_MAILING_NAME,
                    email: process.env.ADMIN_SENDING_EMAIL,
                },
                subject: emailTemplate2.subject,
                html: emailTemplate2.emailContent
                    .replace('[CUSTOMERFULLNAME]', `${guestpertMembershipDetails.firstName} ${guestpertMembershipDetails.lastName}`)
                    .replace('[PRODUCTNAME]', `${transactions[0].item_list.items[0].name}`)
                    .replace('[DATETIME]' , `${dateFormat(create_time, 'dS mmm yyyy, h:MM:ss TT')}`)
                    .replace(/\[WEBSITELINK\]/g, `${process.env.DOMAIN}`)
            }

            sgMail.send(msg1)
            sgMail.send(msg2)

            res.redirect('/guestpert/orders')
        });

    } catch (err) {
        next(err)
    }
}


////////////////////////////////
// ---- Enqueries Page ----- //

exports.getEnquiries = async (req, res, next)=>{
  try{

    const curUser = req.session.user._id;
    const {pageNum} = req.query;

    const enquiryList = await enquiries.getEnquiriesByGuestpertId(curUser, pageNum);
    const totalItems = await enquiries.getCountEnquiriesGuestpertId(curUser)
    if(!enquiryList){
        throw new Error('Error fetching from database')
    }

    res.json({dataArr: enquiryList, totalItems, pageNum})
    
  }catch(err){
    res.status(404).json({msg: 'something went wrong. Please refresh the page'})
  }
}

exports.getProducerInquiriesPage = (req, res, next)=>{
  try{
    
    res.render('./templates/guestpertPannel/ga_inquiries')

  }catch(err){
    next(err)
  }
}