const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);


const producerModel = require('../models/producers.models')
const guestpertModel = require('../models/guestperts.models')
const enquiriesModel = require('../models/enquiries.models');
const tempLinks = require('../models/tempLinks.models');
const tvgPPannel = require('../models/tvgGpannelDashboard.models')
const emailModel = require('../models/email.models')

const fileHelper = require('../util/file')
const emailIds = require('../util/email')
const createHash = require('../util/randomHash');

exports.getProducerDashboard = async (req, res, next) => {
  try {

    const userId = req.session.user._id
    const user = await producerModel.fetchProducerById(userId)
    const walkthroughVideo = await tvgPPannel.fetchPPannelDashboard()

    const { firstName, lastName, prefix, primaryExpertise, expandedExpertise, suffix, title, imgSrc, verified, } = user;

    res.render('./templates/producerPannel/pr_dashboard', {
      verificationStatus: verified,
      firstName,
      lastName,
      prefix,
      suffix,
      title,
      imgSrc,
      primaryExpertise,
      expandedExpertise,
      videoLink: walkthroughVideo.videoLink
    })

  } catch (err) {
    next(err)
  }
}


exports.getEnquiriesOfProducer = async (req, res, next) => {
  try {

    const { guestpertId, pageNum } = req.query
    let producerId = '5fe1d8c22b31282344a27ca2';
    if(process.env.MODE !== 'development'){
      producerId = req.session.user._id
    }
 
    const requiredEnquiries = await enquiriesModel.getEnquiriesOfProducers(pageNum, guestpertId, producerId)
    const totalItems = await enquiriesModel.getCountForProducerPannel(guestpertId, producerId)

    if (!requiredEnquiries) {
      throw new Error('Couldnot fetch Enquiries')
    }

    res.json({ dataArr: requiredEnquiries, pageNum, totalItems })

  } catch (err) {
    res.status(404).json({ msg: 'Something went wrong' })
  }
}


exports.getProducerEnquiries = (req, res, next) => {
  try {

    const errMessage = req.flash('errMessage')[0]
    res.render('./templates/producerPannel/pr_enquiries',
      {
        errMessage
      }
    )

  } catch (err) {
    next(err)
  }
}


exports.getAllGuestperts = async (req, res, next) => {
  try {

    const pageNum = req.query.pageNum;
    const keyword = req.query.keywords;

    const allGuestperts = await guestpertModel.fetchAllGuestpertLimitedInfo(pageNum, keyword)
    const totalItems = await guestpertModel.activeGuestpertCount()
    res.json({ dataArr: allGuestperts, pageNum, totalItems })

  } catch (err) {
    next(err)
  }
}


exports.createEnquiry = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, Please Try again')
      return res.redirect('/producer/enquiries');
    }

    const { media, show, airDate, location, topic, rating } = req.body;
    const { guestpertId } = req.query;
    const producerId = req.session.user._id;

    const finalObj = {
      media, show, topic, airDate, location, rating, guestpertId, producerId
    }

    const newEnquiries = new enquiriesModel(finalObj)
    const create = await newEnquiries.save()

    if (create.insertedCount < 1) {
      req.flash('errMessage', 'Something went wrong. Please try again later')
    }

    await guestpertModel.createGuestpertUnreadInquiries(guestpertId)

    res.redirect('/producer/enquiries');
  } catch (err) {
    next(err)
  }
}


exports.editRating = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, Please try again')
      return res.redirect('/producer/enquiries');
    }

    const enquiryId = req.query.inquiryId;

    const { rating } = req.body
    if (!rating) {
      req.flash('errMessage', 'Please don\'t leave empty rating')
      return res.redirect('/producer/enquiries');
    }
    
    const finalObj = { rating }

    const update = await enquiriesModel.updateEnquiry(enquiryId, finalObj)
    if (update.modifiedCount < 1) {
      throw new Error('Could not update in database')
    }

    res.redirect('/producer/enquiries');
  } catch (err) {
    next(err)
  }
}


exports.setVerificationToProducer = async (req, res, next) => {
  try {

    const { hash } = req.query;

    const fetchHash = await tempLinks.fetchHash(hash);
    if (!fetchHash) {
      return res.redirect('/timeout')
    }

    const { user, _id } = fetchHash;
    const update = await guestpertModel.verifyGuestpert(user);
    if (update.modifiedCount < 1) {
      return res.redirect('/timeout')
    }

    req.session.verificationStats = true;
    await tempLinks.delLink(_id);

    res.redirect('/producer/dashboard');

  } catch (err) {
    next(err)
  }
}

///////////////////////////////////////////
//////------ Edit Producers ------ ///////

exports.getProfileSettingsPage = async (req, res, next) => {
  try {
    const errMessage = req.flash('errMessage')[0]
    const userId = req.session.user._id;
    const producerInfo = await producerModel.fetchProducerById(userId)

    res.render('./templates/producerPannel/pr_profileSettings', {
      errMessage,
      producerInfo
    })

  } catch (err) {
    next(err)
  }
}

// Edit producer profile picture
exports.editProducerProfilePic = async (req, res, next) => {
  try {

    const mimeTypeErr = req.flash('passOnMsg');

    let imageUrl;
    if (req.file) {
      imageUrl = `/${req.file.path}`;
    }

    if (!imageUrl) {
      req.flash('errMessage', 'Please attach an image');
      if (imageUrl) { fileHelper.delFile(imageUrl) };
      return res.redirect('/producer/profileSettings');
    }

    if (mimeTypeErr.length > 0 || !imageUrl) {
      req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')
      if (imageUrl) { fileHelper.delFile(imageUrl) };
      return res.redirect('/producer/profileSettings');
    }

    const producerId = req.session.user._id;
    const prevInfo = await producerModel.fetchProducerById(producerId)

    if (!prevInfo) {
      req.flash('errMessage', 'Could not found such producer')
      if (imageUrl) fileHelper.delFile(imageUrl)
      return res.redirect('/producer/profileSettings');
    }

    const result = await producerModel.updateProducer(producerId, { imgSrc: imageUrl });
    if (!result.modifiedCount > 0) {
      req.flash('errMessage', 'something went wrong. Please try again later')
    }

    fileHelper.delFile(prevInfo.imgSrc)
    res.redirect('/producer/profileSettings');
  } catch (err) {
    next(err)
  }
}


exports.editProducerPersonalInfo = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, Please try again.')
      return res.redirect('/producer/profileSettings');
    }

    const producerId = req.session.user._id;

    const { firstName, lastName, prefix, suffix, title, } = req.body;
    const finalObj = { firstName, lastName, prefix, suffix, title };

    const result = await producerModel.updateProducer(producerId, finalObj);

    if (!result.modifiedCount > 0) {
      req.flash('errMessage', 'Something went wrong. Please try again')
    }

    res.redirect('/producer/profileSettings');
  } catch (err) {
    next(err)
  }
}


exports.editProducerContact = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, Please try again')
      return res.redirect('/producer/profileSettings');
    }

    const producerId = req.session.user._id;

    const { address, phone, mobile, company, website, fax } = req.body;
    const finalObj = { address, phone, mobile, company, website, fax };

    const result = await producerModel.updateProducer(producerId, finalObj);

    if (!result.modifiedCount > 0) {
      req.flash('errMessage', 'Something went wrong. Please try again')
    }

    res.redirect('/producer/profileSettings');
  } catch (err) {
    next(err)
  }
}


exports.editProducerCredentials = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, please try again')
      return res.redirect('/producer/profileSettings');
    }

    const { oldPassword, password } = req.body

    const producerId = req.session.user._id;
    const prevInfo = await producerModel.fetchProducerById(producerId)

    if (!prevInfo) {
      req.flash('errMessage', 'Could not found such producer')
      return res.redirect('/producer/profileSettings');
    }

    const passwordMatch = await bcrypt.compare(oldPassword, prevInfo.password);

    if (!passwordMatch) {
      req.flash('errMessage', 'Incorrect old password. Please try again')
      return res.redirect('/producer/profileSettings');
    }

    const hashedPassword = await bcrypt.hash(password, 12)
    const finalObj = { password: hashedPassword };
    const result = await producerModel.updateProducer(producerId, finalObj);

    if (!result.modifiedCount > 0) {
      req.flash('errMessage', 'Something went wrong. Please try again')
    }

    res.redirect('/producer/profileSettings');
  } catch (err) {
    next(err)
  }
}


exports.editProducerEmail = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      throw new Error()
    }

    const { email } = req.body
    const producerId = req.session.user._id

    const prevEmail = await producerModel.fetchProducerByEmail(email)

    if (prevEmail) {
      return res.status(403).json({ message: 'Email already exists. Please try a different one' })
    }

    const { firstName, lastName } = await producerModel.fetchProducerById(producerId);
    const emailTemplate = await emailModel.fetchAEmail(emailIds['updateEmailAddress'])
    const hash = await createHash()

    const finalObj = {
      user: producerId,
      hash,
      email
    }

    const result = new tempLinks(finalObj);
    const create = await result.save();

    if (create.insertedCount < 1) {
      throw new Error({ success: false })
    }

    const msg = {
      to: email,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: emailTemplate.subject,
      html: emailTemplate.emailContent
        .replace('[userName]', `${firstName} ${lastName}`)
        .replace(/\[resetLink\]/g, `${process.env.DOMAIN}/resetEmail?hash=${hash}`)
    }

    sgMail.send(msg)
    res.json({ msg: 'success' })
  } catch (err) {

    res.status(403).json({ message: 'something went wrong. Please try again later' })
  }
}