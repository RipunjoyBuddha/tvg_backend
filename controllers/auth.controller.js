const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const sgMail = require('@sendgrid/mail');

const roles = require('../util/roles');
const email_ids = require('../util/email');
const createHash = require('../util/randomHash');

const users = require('../models/users.models');
const producers = require('../models/producers.models');
const admins = require('../models/admins.models');
const guestperts = require('../models/guestperts.models');
const emails = require('../models/email.models');
const tempLinks = require('../models/tempLinks.models');

sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);



///////////////////////////////////////////////////
//////////////////////////////////////////////////

exports.verifyUsersAccount = async (req, res, next) => {
  try {

    const userId = req.session.user._id;
    const firstName = req.session.user.firstName;
    const lastName = req.session.user.lastName;
    const userEmail = req.session.user.email;
    const role = roles[req.session.user.role.toString()];

    const finalObj = {
      user: userId,
      hash: await createHash(),
      role
    }

    const result = new tempLinks(finalObj);
    const create = await result.save();

    if (create.insertedCount < 1) {
      throw new Error('could not verify')
    }

    const emailTemplate = await emails.fetchAEmail(email_ids.guestpertVerification);

    const msg = {
      to: userEmail,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: role === 'guestpert' ? 'Guestpert verification' : role === 'producer' ? 'Producer Verification' : 'User Verification',
      html: emailTemplate.emailContent
        .replace('[guestpertName]', `${firstName} ${lastName}`)
        .replace(/\[any url\]/g, `${process.env.DOMAIN}/verifyUsers?hash=${finalObj.hash}`)
    }

    sgMail.send(msg)

    res.json({ success: true })

  } catch (error) {

    res.status(404).json({ success: false })
  }
}


exports.verifyUsers = async (req, res, next) => {
  try {

    const { hash } = req.query

    if (!hash) {
      return res.redirect(`/pageNotFound`);
    }

    const userDetails = await tempLinks.fetchHash(hash);
    if (!userDetails) {
      return res.redirect('/pageNotFound')
    }

    const { user, _id, role } = userDetails;

    const update = await producers.updateProducer(user, { verified: true })

    if (update.modifiedCount < 1) {
      return res.redirect('/pageNotFound')
    }

    tempLinks.delLink(_id)

    res.redirect(`/${role.toString() === 'guestpert' ? 'guestpert/dashboard' : role.toString() === 'producer' ? 'producer/dashboard' : 'pageNotFound'}`)

  } catch (err) {
    next(err)
  }
}


exports.getRegisterProducerPage = (req, res, next) => {
  try {

    res.render('templates/auth/register', {
      captcha: process.env.RECAPTCHA_CLIENTSIDE
    })

  } catch (err) {
    next(err)
  }
}

exports.getForgotPasswordPage = (req, res, next) => {
  try {
    res.render('templates/auth/forgotPassword')
  } catch (err) {
    next(err)
  }
}


exports.getResetPasswordPage = async (req, res, next) => {
  try {

    const { hash } = req.query;
    if (!hash) {
      return res.redirect('/pageNotFound')
    }

    const tempLink = await tempLinks.fetchHash(hash);
    if (!tempLink) {
      return res.redirect('/pageNotFound')
    }

    const { createdAt } = tempLink;
    const newTime = new Date(new Date(createdAt).getTime() + 10 * 60000);

    if (new Date() > newTime && process.env.MODE === 'production') {
      return res.redirect('/pageNotFound')
    }

    res.render('templates/auth/resetPassword', { hash })

  } catch (err) {
    next(err)
  }
}

exports.getRegisterGuestpertPage = async (req, res, next) => {
  try {

    const errMessage = req.flash('errMessage')[0];

    res.render('templates/auth/guestpertRegistration', {
      captcha: process.env.RECAPTCHA_CLIENTSIDE,
      errMessage
    })

  } catch (err) {
    next(err)
  }
}


exports.getGuestpertsLoginPage = async (req, res, next) => {
  try {
    const errMessage = req.flash('errMessage')[0];
    res.render('templates/auth/login', {
      errMessage
    })

  } catch (err) {
    next(err)
  }
}

exports.getAdminLoginPage = (req, res, next) => {
  try {
    if (req.session.user && (req.session.user.role.toString() == roles['godMode'].toString() || req.session.user.role.toString() == roles['subAdmin'].toString())) {
      return res.redirect('/admin/dashboard')
    }

    const errMessage = req.flash('errMessage')[0];
    res.render('templates/auth/admin_login', {
      errMessage
    })
  } catch (err) {
    next(err)
  }
}


exports.loginUser = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs')
      return res.redirect('/login');
    }


    const { username, password } = req.body;
    const user = await users.fetchUserLogin(username, 'username');

    if (user.length < 1) {
      req.flash('errMessage', 'Your username or password is incorrect')
      return res.redirect('/login');
    }


    const matchPassword = user[0].password;
    const passChck = await bcrypt.compare(password, matchPassword);


    if (!passChck) {
      req.flash('errMessage', 'Your username or password is incorrect')
      return res.redirect('/login');
    }

    // Set userrole to a string
    user[0].role = user[0].role.toString()

    req.session.isLoggedIn = true;
    req.session.user = user[0];
    req.session.save(err => {
      if (err) {
        throw err
      }
    })


    if (user[0].role.toString() == roles['guestpert'].toString()) {
      return res.redirect('/guestpert/dashboard');
    } else if (user[0].role.toString() == roles['producer'].toString()) {
      return res.redirect('/producer/dashboard');
    }

    res.redirect('/login')

  } catch (err) {
    next(err)
  }
}


exports.loginAdmin = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs')
      return res.redirect('/admin_login');
    }

    const { username, password } = req.body;
    const user = await admins.fetchAdmin(username);
    if (!user) {
      req.flash('errMessage', 'Incorrect username or password')
      return res.redirect('/admin_login');
    }

    const { activeStats } = user;
    if (!activeStats) {
      req.flash('errMessage', 'This user have been blocked. Please contact higher officials to unblock account.')
      return res.redirect('/admin_login');
    }

    const matchPassword = user.password;
    const passChck = await bcrypt.compare(password, matchPassword);

    if (!passChck) {
      req.flash('errMessage', 'Incorrect username or password')
      return res.redirect('/admin_login');
    }

    // Set userrole to a string
    user.role = user.role.toString()

    const sessionUser = {
      _id: user._id,
      role: user.role,
      firstName: user.firstName,
      lastName: user.lastName,
      userName: user.userName,
      activeStats: user.activeStats
    }

    req.session.isLoggedIn = true;
    req.session.user = sessionUser;
    req.session.save(err => {
      if (err) {
        throw err
      }
    })

    res.redirect('/admin/dashboard');
  } catch (err) {
    next(err)
  }
}

exports.logoutUser = (req, res, next) => {
  try {
    let user;
    if (req.session.user) {
      user = req.session.user.role
    }
    req.session.destroy(err => {
      if (err) {
        throw new Error()
      }
    })
    if (!user) {
      res.redirect('/')
    } else if (user.toString() == '10101101001110' || user.toString() == '10101011010101') {
      res.redirect('/admin_login')
    } else if (user.toString() === '10101011001010' || user.toString() === '10111010000101') {
      res.redirect('/login')
    } else {
      res.redirect('/')
    }

  } catch (err) {
    next(err)
  }
}

exports.logoutUserRest = (req, res)=>{

  try {
    
    req.session.destroy(err=>{
      if(err)throw new Error('Error logging out')
    })
    
    res.json({msg: 'logout successful'})

  } catch (error) {
    res.status(403).json({msg: 'Failed logging out'})
  }
}

/////////////////////////////////////////////
///---- Register New Producer ------ ///

exports.createProducer = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {

      req.flash('errMessage', 'Invalid inputs, please try again')
      return res.redirect('/register/producer');
    }

    const recaptcha = req.body['g-recaptcha-response'];
    const result = await axios(`https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SERVERSIDE}&response=${recaptcha}`)

    if ((result && !result.data.success) || !result) {
      req.flash('errMessage', 'Captcha not valid, please try again.')
      return res.redirect('/register/guestpert');
    }

    const { username, password, email, firstName, lastName, phone, website, mobile, fax, knowingUs, bookingMandate } = req.body;

    // Search for username and email
    const prevProducer = await guestperts.fetchAGuestpertWithUsernameOrEmail(username, email);

    // Send Error message if username or email is present
    if (prevProducer.length > 0) {
      if (prevProducer[0].email === email) {
        req.flash('errMessage', 'email already exist, please change your email');
        return res.redirect('/register/producer');
      } else if (prevProducer[0].username === username) {
        req.flash('errMessage', 'username already exist, please change your username');
        return res.redirect('/register/producer');
      } else {
        req.flash('errMessage', 'user already exist, please change your username and email');
        return res.redirect('/register/producer');
      }
    }


    // Set Role and Password
    const hashedPassword = await bcrypt.hash(password, 12);
    const role = roles.producer;

    const finalObj = {
      username, email, firstName, lastName, phone, mobile, website, mobile, fax, knowingUs, bookingMandate,
      password: hashedPassword,
      role
    }

    const newProducer = new producers(finalObj);
    const create = newProducer.save();

    if (create.insertedCount < 1) {
      req.flash('errMessage', 'Your request couldnot be processed. Please try again later.');
      return res.redirect('/register/producer');
    }

    const allEmails = await emails.fetchEmailsWithId([email_ids.newProducerNotificationToAdmin, email_ids.welcomeNewGuestpert]);

    const email1 = {
      to: process.env.ADMIN_RECEIVING_EMAIL,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: allEmails[1].subject,
      html: allEmails[1].emailContent
        .replace('[producerDetails]', `
          <p>name : ${firstName} ${lastName}</p>
          <p>email : ${email}</p>
          <p>phone: ${phone}</p>`
        )
    }

    const email2 = {
      to: email,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: allEmails[0].subject,
      html: allEmails[0].emailContent
        .replace('[guestpertName]', `${firstName} ${lastName}`)
        .replace(/\[websiteLink\]/g, `${process.env.DOMAIN}/login`)
        .replace('[username]', username)
        .replace('[password]', password)
    }

    sgMail.send(email1);
    sgMail.send(email2);


    // Add Email to mailerlite
    axios({
      method: 'post',
      url: 'https://api.mailerlite.com/api/v2/subscribers/',
      headers: {
        "Content-Type": "application/json",
        "X-MailerLite-ApiKey": "dbb4de9b00233aaed147cb737142a280"
      },
      data: {
        email
      }
    })

    res.redirect('/login');
  } catch (err) {
    next(err)
  }
}


/////////////////////////////////////////////
///---- Register New Guestpert ------ ///

exports.createGuestpert = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      req.flash('errMessage', 'Invalid inputs, please try again .')
      return res.redirect('/register/guestpert');
    }

    const recaptcha = req.body['g-recaptcha-response'];
    const result = await axios(`https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SERVERSIDE}&response=${recaptcha}`)

    if ((result && !result.data.success) || !result) {
      req.flash('errMessage', 'Captcha not valid, please try again.')
      return res.redirect('/register/guestpert');
    }

    const { username, password, email, firstName, lastName, address, city, state, zipCode, country, phone, website, fax, knowingUs, moreInfoCall } = req.body;

    // Search for username and email
    const prevGuestpert = await guestperts.fetchAGuestpertWithUsernameOrEmail(username, email);

    // Send Error message if username or email is present
    if (prevGuestpert.length > 0) {
      if (prevGuestpert[0].email === email) {
        req.flash('errMessage', 'email already exist, please change your email');
        return res.redirect('/register/guestpert');
      } else if (prevGuestpert[0].username === username) {
        req.flash('errMessage', 'username already exist, please change your username');
        return res.redirect('/register/guestpert');
      } else {
        req.flash('errMessage', 'user already exist, please change your username and email');
        return res.redirect('/register/guestpert');
      }
    }


    // Set Role and Password
    const hashedPassword = await bcrypt.hash(password, 12);
    const role = roles.guestpert;

    const finalObj = {
      username, email, firstName, lastName, address, city, state, zipCode, country, phone, website, fax, knowingUs,
      password: hashedPassword,
      role
    }

    const newGuestpert = new guestperts(finalObj);
    const create = newGuestpert.save();

    if (create.insertedCount < 1) {
      req.flash('errMessage', 'Your request couldnot be processed. Please try again later.');
      return res.redirect('/register/guestpert');
    }

    const allEmails = await emails.fetchEmailsWithId([email_ids.newGuestpertNotificationToAdmin, email_ids.welcomeNewGuestpert]);

    const email1 = {
      to: process.env.ADMIN_RECEIVING_EMAIL,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: allEmails[0].subject,
      html: allEmails[0].emailContent
        .replace('[guestpertDetails]', `
            <p>name : ${firstName} ${lastName}</p>
            <p>email : ${email}</p>
            <p>address : ${address}</p>
            <p>phone: ${phone}</p>`)
        .replace('[guestpertCallDetails]', moreInfoCall ? `${firstName} would like to get a call from TVG for more information` : '')
    }

    const email2 = {
      to: email,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: allEmails[1].subject,
      html: allEmails[1].emailContent
        .replace('[guestpertName]', `${firstName} ${lastName}`)
        .replace(/\[websiteLink\]/g, `${process.env.DOMAIN}/login`)
        .replace('[username]', username)
        .replace('[password]', password)
    }

    sgMail.send(email1);
    sgMail.send(email2);


    // Add Email to mailerlite
    axios({
      method: 'post',
      url: 'https://api.mailerlite.com/api/v2/subscribers/',
      headers: {
        "Content-Type": "application/json",
        "X-MailerLite-ApiKey": "dbb4de9b00233aaed147cb737142a280"
      },
      data: {
        email
      }
    })

    res.redirect('/login')
  } catch (err) {
    req.flash('errMessage', 'Internal server error. Please try again');
    res.redirect('/register/guestpert')
  }
}



exports.requestResetPassForm = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);

    if (validationErr.errors.length > 0) {
      throw new Error({ success: false })
    }

    const { username } = req.body;

    // Fetch user associated with username
    const user = await users.fetchAUser(username);
    if (!user) {
      throw new Error({ success: false })
    }

    const { _id, firstName, lastName, email } = user;

    const finalObj = {
      user: _id,
      firstName, lastName,
      username: user.username,
      hash: await createHash()
    }

    const result = new tempLinks(finalObj);
    const create = await result.save();

    if (create.insertedCount < 1) {
      throw new Error({ success: false })
    }
    // Send Email
    const emailTemplate = await emails.fetchAEmail(email_ids.guestpertPasswordRecovery);

    const msg = {
      to: email,
      from: {
        name: process.env.ADMIN_MAILING_NAME,
        email: process.env.ADMIN_SENDING_EMAIL,
      },
      subject: emailTemplate.subject,
      html: emailTemplate.emailContent
        .replace('[user]', `${firstName} ${lastName}`)
        .replace('[resetLink]', `${process.env.DOMAIN}/resetPassword?hash=${finalObj.hash}`)
        .replace('[username]', finalObj.username)
    }

    await sgMail.send(msg)
    res.json({ success: true })
  } catch (err) {
    console.log(err)
    res.status(403).json({ success: false })
  }
}


exports.resetUserPassword = async (req, res, next) => {
  try {
    const validationErr = validationResult(req);
    const { hash } = req.query;
    if (!hash) {
      return res.redirect(`/pageNotFound`);
    }

    const userDetails = await tempLinks.fetchHash(hash);

    if (validationErr.errors.length > 0 || !userDetails) {
      req.flash('errMessage', 'Something went wrong, please try again')
      return res.redirect(`/resetPassword?hash=${hash}`);
    }

    const { password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 12);

    const update = await users.updatePassword(userDetails.user, hashedPassword);
    if (update.modifiedCount < 1) {
      req.flash('errMessage', 'Something went wrong, please try again')
      return res.redirect(`/resetPassword?hash=${hash}`);
    }

    await tempLinks.delLink(userDetails._id)

    res.redirect(`/login`);
  } catch (err) {
    next(err)
  }
}


exports.resetUserEmail = async (req, res, next) => {
  try {

    const { hash } = req.query;
    if (!hash) {
      return res.redirect(`/pageNotFound`);
    }
    const userDetails = await tempLinks.fetchHash(hash);
    if (!userDetails) {
      return res.redirect('/pageNotFound')
    }

    const { user, email, _id } = userDetails;

    const finalObj = {
      email
    }

    const update = await producers.updateProducer(user, finalObj)
    if (update.modifiedCount < 1) {
      return res.redirect('/pageNotFound')
    }

    tempLinks.delLink(_id)

    res.redirect('/producer/profileSettings')

  } catch (err) {
    next(err)
  }
}