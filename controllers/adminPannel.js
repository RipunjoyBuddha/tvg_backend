const { validationResult } = require('express-validator');
const fileHelper = require('../util/file');
const roles = require('../util/roles');
const membership_id = require('../util/membership');
const sgMail = require('@sendgrid/mail');
const axios = require('axios')

sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);

const bcrypt = require('bcryptjs');
const dateFormat = require('dateformat');
const uniqid = require('uniqid');

// Fetch Models
const teamMembers = require('../models/teamMembers.models');
const hotTopicCategory = require('../models/hotTopicCategory.models');
const services = require('../models/services.models');
const faq = require('../models/faq.models');
const admin = require('../models/admins.models');
const settings = require('../models/settings.models');
const email = require('../models/email.models');
const newsletter = require('../models/newsletter.models');
const mdlHotTopics = require('../models/hotTopics.models');
const tags = require('../models/tags.models');
const founder = require('../models/founder.models');
const membership = require('../models/membership.models');
const blogs = require('../models/blogs.models');
const products = require('../models/products.models');
const temp = require('../models/temp.models');
const testimonials = require('../models/testimonials.models');
const guestperts = require('../models/guestperts.models');
const books = require('../models/books.models.js');
const tvgHome = require('../models/tvgHome.models.js');
const tvgContact = require('../models/tvgContact.models');
const tvgGpannelDashboard = require('../models/tvgGpannelDashboard.models');
const newsletterSubs = require('../models/newsletterSubscriber.models');
const ordersModels = require('../models/orders.models');
const producers = require('../models/producers.models');
const enquiries = require('../models/enquiries.models')

////////////////////////////////////////
////////////////////////////---
//--- Rest Functions ---//-----
///////////////////////////---
////////////////////////////////////////



////////////////////////////---
//--- Admin Email Settings ---//-----
///////////////////////////---

// Edit Email Settings  -----------
exports.getEmailSettings = async (req, res, next) => {

    // Get id and store it
    const id = req.query.id;

    if (!id) {
        // Send error report
        return new Error('Invalid blog ID');
    }
    // Get data from database
    const obj = await email.fetchAEmail(id);

    // send object
    res.status(200).json(obj);
}


exports.getEmailSettingsPage = async (req, res, next) => {

    try {

        const data = await email.fetchAllEmail();
        data.forEach(d => {
            const newCreated = dateFormat(d.createdAt, 'd mmm yyyy');
            const newUpdated = dateFormat(d.updatedAt, 'd mmm yyyy');
            d.createdAt = newCreated;
            d.updatedAt = newUpdated;
        });


        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_emailSettings', {
            pathArr: [
                { url: 'siteSettings', page: 'manage settings' },
                { url: 'emailSettings', page: 'email settings' }
            ],
            data,
            errMessage
        })

    } catch (err) {
        next(err);
    }
}


exports.createEmailTemplate = async (req, res, next) => {

    try {
        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')

            // Send Invalid Inputs page
            return res.redirect('/admin/emailSettings');
        }

        // Get the fields
        const { segment, emailContent, subject } = req.body;

        const createEmail = new email(segment, subject, emailContent);
        const result = await createEmail.save();

        if (!result.insertedCount > 0) {

            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
            return res.redirect('/admin/emailSettings');
        }

        res.redirect('/admin/emailSettings')

    } catch (err) {
        next(err)
    }
}


exports.editEmailTemplate = async (req, res, next) => {

    try {

        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')

            // Send Invalid Inputs page
            return res.redirect('/admin/emailSettings');
        }

        // Get the fields
        const { segment, emailContent, subject } = req.body;
        
        const result = await email.update(id, { segment, subject, emailContent });

        if (!result.modifiedCount > 0) {

            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
            return res.redirect('/admin/emailSettings');
        }

        res.redirect('/admin/emailSettings')

    } catch (err) {
        next(err)
    }
}



////////////////////////////---
//--- Homepage Books Carousel Settings ---//-----
///////////////////////////---

// Search Books -----------
exports.searchBooks = async (req, res, next) => {

    try {// Get keyword and pageNum
        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;
        const data = await products.fetchAllProdsBooks(pageNum, keyword);
        const totalItems = await products.fetchProdBookCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


// Add Books To Home -----------
exports.addBooksToHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.bookId;

        // Req db to add to homePage
        const book = await products.fetchAProduct(id);
        if (!book) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        const { _id, imgSrc, title, bestSeller } = book;
        const finalObj = {
            imgSrc, title, bestSeller, _id,
            id: uniqid()
        }

        const update = await tvgHome.addProductsToHome(finalObj)
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }
        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


// Remove Books To Home -----------
exports.removeBooksFromHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.bookId;

        // Req db to remove from homePage
        const { homepageBooks } = await tvgHome.fetchBooksOnHome();
        if (homepageBooks.length <= 4) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        const finalArr = homepageBooks.filter(c => c.id != id);

        const update = await tvgHome.updateHome({ homepageBooks: finalArr });
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }
        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



////////////////////////////---
//--- Homepage Guestperts Settings ---//-----
///////////////////////////---

// Search guestperts not on Home -----------
exports.searchGuestperts = async (req, res, next) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;

        let data = await guestperts.fetchAllGuestpertLimitedInfo(pageNum, keyword);
        const totalItems = await guestperts.activeGuestpertCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


// Add Guestperts To Home -----------
exports.addGuestpertsToHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.guestpertId;

        // Req db to add to homePage
        const guestpert = await guestperts.fetchAGuestpertLimitedInfo(id);
        
        if (!guestpert) {
            return res.status(404).json({ msg: 'failed to do update homepage' });
        }
        const finalObj = {
            ...guestpert,
            id: uniqid()
        }
        const pushGuestpert = await tvgHome.addGuestpertToHome(finalObj);
        if (pushGuestpert.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do update homepage' });
        }
        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


// Remove Guestperts from Home ------------
exports.removeGuestpertsFromHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.guestpertId;

        // Req db to add to homePage
        const { homepageGuestperts } = await tvgHome.fetchGuestpertsOnHome();
        if (homepageGuestperts.length <= 6) {
            return res.status(404).json({ msg: 'Homepage updating failed' });
        }

        const removeList = homepageGuestperts.filter(c => c.id !== id);

        // Update guestpert list
        const update = await tvgHome.updateHome({ homepageGuestperts: removeList });
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'Homepage updating failed' });
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



////////////////////////////---
//--- Homepage Hot Topics Settings ---//-----
///////////////////////////---

// Search hotTopics not on Home -----------
exports.searchHotTopics = async (req, res, next) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;

        const data = await mdlHotTopics.fetchAllTopicsLimitedInfo(pageNum, keyword);
        data.forEach(c => {
            c.imgSrc = c.author[0]['imgSrc'];
            c.author = `${c.author[0]['firstName']} ${c.author[0]['lastName']}`;
        })

        const totalItems = await mdlHotTopics.homePageHotTopicsCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


// Add Topics To Home -----------
exports.addTopicsToHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.topicId;

        const topics = await mdlHotTopics.fetchATopicLimitedInfo(id);
        if (!topics) {
            return res.status(404).json({ msg: 'failed to do update homepage' });
        }
        topics.imgSrc = topics.author[0].imgSrc;
        topics.author = `${topics.author[0].firstName} ${topics.author[0].lastName}`;
        const finalObj = {
            ...topics,
            id: uniqid()
        }
        const pushTopics = await tvgHome.addTopicsToHome(finalObj);
        if (pushTopics.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do update homepage' });
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


// Remove Topics To Home -----------
exports.removeTopicsFromHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.topicId;

        // Req db to add to homePage
        const { homepageHotTopics } = await tvgHome.fetchHotTopicsOnHome();
        if (homepageHotTopics.length < 6) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        const finalArr = homepageHotTopics.filter(c => c.id != id);

        // Update guestpert list
        const update = await tvgHome.updateHome({ homepageHotTopics: finalArr });
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'Homepage updating failed' });
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



////////////////////////////---
//--- Homepage Testimonials Settings ---//-----
///////////////////////////---

exports.searchGuestpertTestimonials = async (req, res, next) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;
        const data = await testimonials.fetchTestimonialBykeywords(pageNum, keyword)
        const totalItems = await testimonials.homepageTestimonialCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.addTestimonialsToHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.testimonialId;

        // Req db to add to homePage
        const testimonial = await testimonials.fetchATestimonial(id);

        if (!testimonial || testimonial.role.toString() !== '10101011001010' || testimonial.vidLink.length > 0 || testimonial.imgArr.length > 0) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        const finalObj = {
            fullName: testimonial.name,
            imgSrc: testimonial.imgSrc,
            content: testimonial.content,
            title: testimonial.title,
            id: uniqid(),
            _id: testimonial._id
        }

        const update = await tvgHome.addTestimonialsToHome(finalObj);
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}

exports.removeTestimonialsFromHome = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.testimonialId;

        // Req db to add to homePage
        const { homepageTestimonials } = await tvgHome.fetchTestimonialsOnHome();
        const finalArr = homepageTestimonials.filter(c => c.id !== id);

        const update = await tvgHome.updateHome({ homepageTestimonials: finalArr });
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        // Send Output
        res.status(200).json({ success: true });
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


//////////////////////////////////////////---
//--- Manage Hot Topics Categories ---//-----
/////////////////////////////////////////---

exports.createHotTopicCategory = async (req, res, next) => {

    try {
        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');
        let imageUrl = null;

        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0 || !imageUrl) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }
        // Get the fields
        const { catTitle, active } = req.body;
        if (!catTitle) { throw new Error };

        let activeStats = false;
        if (active) { activeStats = true };


        // Creating the Team member
        const hotTopicCat = new hotTopicCategory(catTitle.replace(/[<>\/]/g, ''), imageUrl, activeStats);
        const result = await hotTopicCat.save();
        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }
    } catch (err) {
        // Error handling
        return next(err)
    }

    res.redirect('/admin/userInterfaces/frontend/hotTopicsCat');
}

// --------------------------------------------

exports.editHotTopicCategory = async (req, res, next) => {

    try {
        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0) {

            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        const { catTitle, active } = req.body;

        if (!catTitle) { throw new Error };

        let activeStats = false;
        if (active) { activeStats = true };
        let imageUrl = 'dontExist';
        if (req.file) {

            // Fetch the member from database and extract its imageurl
            const data = await hotTopicCategory.fetchCategory(id);
            const prevImg = data.imageUrl;

            if (prevImg && prevImg.includes('hotTopicsCategories')) {
                // Delete the prev image
                fileHelper.delFile(prevImg);
            }

            imageUrl = `/${req.file.path}`;
        }


        // Creating updated object
        const obj = { title: catTitle.toLowerCase().replace(/[<>\/]/g, ''), imageUrl, active: activeStats };

        const result = await hotTopicCategory.update(id, obj);
        // If update is not successful then flash error message
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }
    } catch (err) {
        next(err)
    }

    // Redirect to about page
    res.redirect('/admin/userInterfaces/frontend/hotTopicsCat')
}

// --------------------------------------------

exports.getHotTopicsCatPage = async (req, res, next) => {

    try {

        const data = await hotTopicCategory.getAllMember();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageHotTopicsCat', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/hotTopicsCat', page: 'hot topics category' }
            ],
            data, errMessage
        })

    } catch (err) {
        next(err);
    }

}


////////////////////////////---
//--- Manage About Page ---//-----
///////////////////////////---

exports.getManageAboutPage = async (req, res, next) => {

    try {

        // Get details from database
        const data = await teamMembers.getAllMember();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageAboutPage', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/about', page: 'about' }
            ],
            data,
            errMessage
        })

    } catch (err) {
        next(err);
    }

}

exports.getMemberDetails = async (req, res, next) => {

    try {
        // Get and store the ID
        const id = req.query.id;

        // Fetch the member from database and extract its imageurl
        const data = await teamMembers.fetchMember(id);

        // Fetch from database
        const obj = {
            id: data._id,
            imgSrc: data.imageUrl,
            firstName: data.firstName,
            lastName: data.lastName,
            title: data.title,
            description: data.description,
            memberType: data.memberType
        }

        // Send JSON
        res.status(200).json(obj);

    } catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}

exports.deleteTeamMember = async (req, res, next) => {

    try {
        // Get and store the ID
        const id = req.query.id;

        // Fetch the member from database and extract its imageurl
        const data = await teamMembers.fetchMember(id);

        const prevImg = data.imageUrl;

        // Delete in database
        const delRes = await teamMembers.deleteMember(id);
        if (!delRes.deletedCount > 0) {
            throw new Error();
        }

        if (prevImg && prevImg.includes('teamMembers')) {
            // Delete the prev image
            fileHelper.delFile(prevImg);

        }

        // Send success report
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}

// ------------------------------------------

exports.editPrimaryTeamMember = async (req, res, next) => {

    try {
        const id = req.params.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');


        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0) {

            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        const { firstName, lastName, title, description } = req.body;
        let imageUrl = null;
        if (req.files.length > 0) {

            // Fetch the member from database and extract its imageurl
            const data = await teamMembers.fetchMember(id);
            const prevImg = data.imageUrl;

            if (prevImg && prevImg.includes('teamMembers')) {

                // Delete the prev image
                fileHelper.delFile(prevImg);

            }

            imageUrl = `/${req.files[0].path}`;
        }

        // Creating updated object
        const obj = { firstName, lastName, title, description, imageUrl };

        const result = await teamMembers.update(id, obj);
        // If update is not successful then flash error message
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }
    } catch (err) {
        next(err)
    }

    // Redirect to about page
    res.redirect('/admin/userInterfaces/frontend/about')
}

// ------------------------------------------

exports.createPrimaryTeamMember = async (req, res, next) => {

    try {
        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');


        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { firstName, lastName, title, description } = req.body;
        let imgUrl = '/images/user.png';

        if (req.files.length > 0) {
            imgUrl = `/${req.files[0].path}`;
        }

        // Creating the Team member
        const teamMember = new teamMembers(firstName, lastName, title, 'prime', description, imgUrl);
        const result = await teamMember.save();
        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating member. Try again later');
        }
    } catch (err) {
        // Error handling
        return next(err)
    }

    res.redirect('/admin/userInterfaces/frontend/about')
}
// ------------------------------------------

exports.editOtherTeamMember = async (req, res, next) => {

    try {
        const id = req.params.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        const { firstName, lastName, title } = req.body;

        // Creating updated object
        const obj = { firstName, lastName, title };

        const result = await teamMembers.update(id, obj);
        // If update is not successful then flash error message
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }
    } catch (err) {
        next(err)
    }

    // Redirect to about page
    res.redirect('/admin/userInterfaces/frontend/about')
}

// ------------------------------------------
exports.createOtherTeamMember = async (req, res, next) => {

    try {
        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { firstName, lastName, title } = req.body;

        // Creating the Team member
        const teamMember = new teamMembers(firstName, lastName, title, 'others');
        const result = await teamMember.save();
        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating member. Try again later');
        }
    } catch (err) {
        // Error handling
        return next(err)
    }

    res.redirect('/admin/userInterfaces/frontend/about')
}



////////////////////////////////////////////////////---
//--- Contact page producer testimonial settings ---//-----
///////////////////////////////////////////////////---

exports.searchProducerTestimonials = async (req, res, next) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;
        const data = await testimonials.fetchProducerTestimonialBykeywords(pageNum, keyword);
        const totalItems = await testimonials.getProducerTestimonialCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.addTestimonialsToContact = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.testimonialId;
        // Req db to add to homePage
        const testimonial = await testimonials.fetchATestimonial(id);

        if (!testimonial || testimonial.role.toString() !== '10111010000101' || testimonial.vidLink.length > 0 || testimonial.imgArr.length > 0) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        const finalObj = {
            _id: testimonial._id,
            fullName: testimonial.name,
            imgSrc: testimonial.imgSrc,
            content: testimonial.content,
            title: testimonial.title,
            id: uniqid(),
        }

        const update = await tvgContact.addTestimonialToContact(finalObj)

        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


exports.removeTestimonialsFromContact = async (req, res, next) => {
    try {
        // Get Book Id
        const id = req.query.testimonialId;

        // Req db to add to homePage
        const { testimonials } = await tvgContact.getContactPage()
        const finalArr = testimonials.filter(c => c.id !== id);

        const update = await tvgContact.updateContactPage({ testimonials: finalArr });
        if (update.modifiedCount < 1) {
            return res.status(404).json({ msg: 'failed to do task' });
        }

        // Send Output
        res.status(200).json({ success: true });
    }
    catch (err) {

        res.status(404).json({ msg: 'failed to do task' });
    }
}

exports.getManageContactPage = async (req, res, next) => {
    try {

        const errMessage = req.flash('errMessage')[0];
        const { testimonials, content } = await tvgContact.getContactPage();

        res.render('./templates/adminPannel/ad_manageContactPage', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/contact', page: 'contact' }
            ],
            errMessage,
            testimonials, content
        })
    } catch (err) {
        next(err)
    }
}

///////////////////////--
//--- Faq page ---//-----
//////////////////////--

exports.getManageFaqPage = async (req, res, next) => {

    try {

        const data = await faq.fetchAllFaq();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageFaqPage', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/faq', page: 'faq' }
            ],
            data, errMessage
        })

    } catch (err) {
        next(err)
    }

}

exports.searchFaq = async (req, res, next) => {

    try {// Store the ID
        const id = req.query.id;

        const dbRes = await faq.fetchFaq(id);

        // Send JSON
        res.status(200).json(dbRes);
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}

exports.removeFaq = async (req, res, next) => {

    try {
        // Get and store the ID
        const id = req.query.id;

        // Delete in database
        await faq.delete(id);

        // Send success report
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}

exports.createFaq = async (req, res, next) => {
    try {

        // See if there is some error in validation
        const validationRes = validationResult(req);


        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { question, answerTitle, videoLink, answer, active } = req.body;


        if (!question) { throw new Error };

        let activeStats = false;
        if (active) {
            activeStats = true;
        }

        // Creating the Team member
        const faqEntry = new faq(question, answerTitle, videoLink, answer, activeStats);
        const result = await faqEntry.save();

        if (!result.insertedCount > 0) {
            throw new Error;
        }

        res.redirect('/admin/userInterfaces/frontend/faq');

    } catch (err) {
        next(err);
    }
}

exports.uploadFaqPic = (req, res) => {

    try {
        const mimeTypeErr = req.flash('passOnMsg');

        if (mimeTypeErr.length > 0) {
            // Send Invalid JSON response
            throw new Error();
        }

        const image = req.file;
        let imagePath = null;

        if (image) {
            imagePath = `/${image.path}`;
        }

        if (!imagePath) { throw new Error() };

        res.status(200).json({ location: imagePath })

    } catch (err) {

        res.status(404).json({ message: 'uploading failed' })
    }
}


exports.editFaq = async (req, res, next) => {
    try {

        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);


        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { question, answerTitle, videoLink, answer, active } = req.body;


        if (!question) { throw new Error() };

        let activeStats = false;
        if (active) {
            activeStats = true;
        }

        // Creating the faq
        const result = await faq.update(id, { question, answerTitle, videoLink, answer, activeStats });

        if (!result.modifiedCount > 0) {
            throw new Error();
        }

        res.redirect('/admin/userInterfaces/frontend/faq');

    } catch (err) {
        next(err);
    }
}


////////////////////////////---
//--- Manage Membership ---//-----
///////////////////////////---

exports.getManageMembershipPage = async (req, res, next) => {

    try {

        const features = await membership.fetchFeatures();
        const memberships = await membership.fetchMemberships();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageMembership', {
            pathArr: [
                { url: 'manageMemberships', page: 'manage memberships' }
            ],
            features, memberships, errMessage
        })
    } catch (err) {
        next(err);
    }
}


exports.editMembership = async (req, res, next) => {

    try {

        const validationErr = validationResult(req);
        if (validationErr.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageMemberships');
        }

        const { membershipTitle, price } = req.body;
        const id = req.query.id;

        const chk = await membership.fetchMembership(id);

        if (!chk) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/manageMemberships');
        }

        const result = await membership.editMembership(id, {
            membershipTitle: membershipTitle.toLowerCase(),
            price: +price
        })

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating member. Try again later');
        }

        res.redirect('/admin/manageMemberships');

    } catch (err) {
        next(err);
    }
}


exports.createFeature = async (req, res, next) => {

    try {

        const validationErr = validationResult(req);
        if (validationErr.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageMemberships');
        }

        const { featureTitle, stdActive, eliteActive, platinumActive } = req.body;

        const create = new membership(featureTitle, stdActive, eliteActive, platinumActive);
        const result = await create.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating member. Try again later');
        }

        res.redirect('/admin/manageMemberships');

    } catch (err) {
        next(err);
    }
}

exports.editFeature = async (req, res, next) => {

    try {

        const validationErr = validationResult(req);
        if (validationErr.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageMemberships');
        }

        const { featureId, featureTitle, stdActive, eliteActive, platinumActive } = req.body;

        const finalObj = {
            standard: stdActive ? true : false,
            elite: eliteActive ? true : false,
            platinum: platinumActive ? true : false
        };

        if (featureTitle.length > 0) {
            finalObj.featureTitle = featureTitle;
        }

        const edit = await membership.update(featureId, finalObj)
        if (edit.modifiedCount < 1) {
            req.flash('errMessage', 'Failed to update membership feature. Please Try again')
            return res.redirect('/admin/manageMemberships');
        }

        res.redirect('/admin/manageMemberships');
    } catch (err) {
        next(err);
    }
}

////////////////////////////---
//--- Manage Guestperts ---//-----
///////////////////////////---

exports.createGuestpert = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = '/images/user.png';
        if (req.file) {
            req.file.path ? imageUrl = `/${req.file.path}` : '';
        }

        // Save fro Validation Error
        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0 || !imageUrl) {
            req.flash('errMessage', 'Invalid inputs');

            if (imageUrl) {
                fileHelper.delFile(imageUrl);
            }
            // Send Invalid Inputs page
            return res.redirect('/admin/manageUsers/guestperts');
        }

        let { password, confirmPassword, email, username, membership, membershipExpiryDate } = req.body;

        // Password === confirm Password
        if (password !== confirmPassword) {
            req.flash('errMessage', 'Password & Confirm Password don\'t match');
            fileHelper.delFile(imageUrl);
            return res.redirect('/admin/manageUsers/guestperts');
        }

        // Membership Error
        if (membership.length > 0) {
            if (!membershipExpiryDate.length > 0) {
                req.flash('errMessage', 'Please provide an membership expiry date');
                fileHelper.delFile(imageUrl);
                return res.redirect('/admin/manageUsers/guestperts');
            }
        } else {
            membershipExpiryDate = '';
        }

        // Search for username and email
        const prevUsername = await guestperts.fetchAGuestpert(username, 'username');
        const prevEmail = await guestperts.fetchAGuestpert(email, 'email');

        if (prevUsername || prevEmail) {
            req.flash('errMessage', 'user already exist');
            fileHelper.delFile(imageUrl);
            return res.redirect('/admin/manageUsers/guestperts');
        }

        // Set Role and Password
        const hashedPassword = await bcrypt.hash(password, 12);
        const role = roles.guestpert;

        const finalObj = {
            ...req.body,
            imgSrc: imageUrl,
            membershipExpiryDate,
            role,
            password: hashedPassword,
            membershipOrder: membership_id[membership],
            verified: true
        };

        const newGuestpert = new guestperts(finalObj);
        const result = newGuestpert.save();

        if (result.insertedCount < 1) {
            req.flash('errMessage', 'Your request couldnot be proceed. Please try again later.');
            fileHelper.delFile(imageUrl);
        }


        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}



exports.getManageGuestpertPage = (req, res, next) => {
    try {

        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageGuestperts', {
            pathArr: [
                { url: 'manageUsers', page: 'manage users' },
                { url: 'manageUsers/guestperts', page: 'manage guestperts' }
            ],
            errMessage
        })

    } catch (err) {
        next(err)
    }
}

// Search All Guestperts -----------
exports.searchGuestpertsAllInfo = async (req, res, next) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;

        const data = await guestperts.fetchAllGuestpert(pageNum, keyword);
        const totalItems = await guestperts.totalItems();

        // Refurnish the data
        data.forEach(c => c.role = 'guestpert')

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


// Search hotTopics not on Home -----------
exports.searchGuestpertsEdit = async (req, res, next) => {

    try {// Get keyword and pageNum

        const id = req.query.id;

        if (!id) {
            // Send error report
            return new Error('Invalid blog ID');
        }

        const obj = await guestperts.fetchAGuestpert(id);

        // send object
        res.status(200).json(obj);

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


// Delete the guestpert from database -----------
exports.deleteGuestpert = async (req, res, next) => {

    try {
        // Get and store the ID
        const id = req.query.id;

        // Get the guestpert
        const guestpert = await guestperts.fetchAGuestpert(id);
        const { imgSrc, _id } = guestpert;

        // Delete guestperts blogs hot topics etc...
        await blogs.delBlogAuthorId(id);

        // Delete in database
        const result = await guestperts.delAGuestpert(_id);

        if (result.deletedCount < 1) {
            throw new Error('Could not delete Guestpert. Please try again')
        }

        // Delete the image file
        fileHelper.delFile(imgSrc);

        // Send success report
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


exports.editGuestpertProfilePic = async (req, res, next) => {
    try {

        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (!imageUrl) {
            req.flash('errMessage', 'Please attach an image')
            return res.redirect('/admin/manageUsers/guestperts');
        }

        if (mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')

            if (imageUrl) { fileHelper.delFile(imageUrl) };
            // Send Invalid Inputs page
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const guestpertId = req.query.id;

        const prevInfo = await guestperts.fetchAGuestpert(guestpertId);
        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a guestpert with the attached id')
            fileHelper.delFile(imageUrl)
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const result = await guestperts.updateGuestpert(guestpertId, { imgSrc: imageUrl });
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')
            fileHelper.delFile(imageUrl)
            return res.redirect('/admin/manageUsers/guestperts');
        }

        fileHelper.delFile(prevInfo.imgSrc);
        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}


exports.editGuestpertCommon = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const id = req.query.id;

        if (validationErr.errors.length > 0 || !id) {
            req.flash('errMessage', 'Invalid inputs. Please reffer the guidelines.')

            return res.redirect('/admin/manageUsers/guestperts');
        }

        const finalObj = {
            ...req.body
        }

        if (req.body.firstName && req.body.lastName && req.body.title) {
            finalObj.premium = req.body.premium ? true : false;
        }

        const result = await guestperts.updateGuestpert(id, finalObj);

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem occoured with updating guestpert. Please try again')
        }

        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}


exports.editGuestpertMembership = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const id = req.query.id;

        const errMessage = [...validationErr.errors];

        if (validationErr.errors.length > 0 || !id) {

            if (!id) { errMessage.push({ msg: 'No Guestpert Id provided' }) }

            req.flash('errMessage', errMessage[0].msg)
            // Send Invalid Inputs page
            return res.redirect('/admin/manageUsers/guestperts');
        }


        const prevInfo = await guestperts.fetchAGuestpert(id);
        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a guestpert with the attached id')
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const { membership } = prevInfo;
        let finalObj = {};

        if (membership) {
            if (req.body.membership.length > 0) {
                finalObj.membership = req.body.membership;
                finalObj.membershipOrder = membership_id[req.body.membership];
            }
            if (req.body.membershipExpiryDate.length > 0) {
                finalObj.membershipExpiryDate = req.body.membershipExpiryDate;
            }
        } else {
            if (req.body.membership.length > 0) {
                if (!req.body.membershipExpiryDate.length > 0) {
                    req.flash('errMessage', 'Please provide an membership expiry date');
                    return res.redirect('/admin/manageUsers/guestperts');
                } else {
                    finalObj = {
                        membership: req.body.membership,
                        membershipOrder: membership_id[req.body.membership],
                        membershipExpiryDate: req.body.membershipExpiryDate
                    }
                }
            }
        }
        // Controll Guestpert Activation
        if(req.body.active!=='active'){
            finalObj.membershipOrder = null
        }
        
        const result = await guestperts.updateGuestpert(id, finalObj);

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem occoured with updating guestpert. Please try again')
        }


        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}


exports.updateGuestpertCredentials = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        const id = req.query.id;
        const errMessage = [...validationErr.errors];

        if (validationErr.errors.length > 0 || !id) {

            if (!id) { errMessage.push({ msg: 'No Guestpert Id provided' }) }

            req.flash('errMessage', errMessage[0].msg)
            // Send Invalid Inputs page
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const emailChckObj = await guestperts.fetchAGuestpert(req.body.email, 'email');
        let checkingId = null;
        emailChckObj ? checkingId = emailChckObj._id : '';

        if (emailChckObj && id != checkingId) {
            req.flash('errMessage', 'Email already exist')
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const hashedPassword = await bcrypt.hash(req.body.password, 12);

        const finalObj = {
            password: hashedPassword,
            email: req.body.email
        }

        const result = await guestperts.updateGuestpert(id, finalObj);
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot update guestpert. Please try again');
        }

        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}


exports.updateGuestpertBio = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const id = req.query.id;

        const errMessage = [...validationErr.errors];

        if (validationErr.errors.length > 0 || !id) {
            if (!id) { errMessage.push({ msg: 'No Guestpert Id provided' }) }

            req.flash('errMessage', errMessage[0].msg)
            // Send Invalid Inputs page
            return res.redirect('/admin/manageUsers/guestperts');
        }

        const result = await guestperts.updateGuestpert(id, { biography: req.body.biography })
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Couldnot update guestpert\'s biography. Please try again');
        }

        res.redirect('/admin/manageUsers/guestperts');
    } catch (err) {
        next(err)
    }
}

////////////////////////////---
//--- Manage Founder ---//-----
///////////////////////////---

exports.founderDetailsImageUpload = async (req, res, next) => {
    try {
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (mimeTypeErr.length > 0 || !imageUrl) {
            throw new Error();
        }

        const tempId = req.query.tempId;

        // Get previous image Array
        const prev = await temp.getTemp(tempId);

        if (!prev) {
            // If prev dont exist run create new temp Array
            const newTemp = new temp(tempId, imageUrl);
            await newTemp.save();
        } else {
            // If prev exist update the temp Array
            await temp.updateTemp(tempId, imageUrl)
        }

        res.status(200).json({ location: imageUrl })
    } catch (err) {
        res.status(404).json({ msg: 'error in uploading' })
    }
}

exports.updateFounderDetails = async (req, res, next) => {
    try {

        const { content } = req.body;
        const { tempId } = req.query;

        const tempArr = await temp.getTemp(tempId);
        let tempImgArr = [];
        const prevTempImgArr = await founder.fetchFounderImgArr()

        if (tempArr) { tempImgArr = tempArr.imgArr }
        if (!prevTempImgArr.imgArr) {
            prevTempImgArr.imgArr = []
        }

        // Make an image array
        const mixImgArr = [...prevTempImgArr.imgArr, ...tempImgArr];

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        imgArr = mixImgArr.filter(c => {
            if (content.includes(c)) {
                return c;
            } else {
                trashArr.push(c)
            }
        })

        const finalObj = { details: content, imgArr }

        const update = await founder.editFounderDetails(finalObj)
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Request could not be completed. Try again later')
        }

        // Delete Trash
        if (tempImgArr) {
            temp.delTemp(tempId);
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        res.redirect('/admin/manageFounder/details');

    } catch (err) {
        next(err)
    }
}

exports.getFounderDetailsPage = async (req, res, next) => {
    try {

        const detailsArr = await founder.fetchDetails();
        const details = (detailsArr[0].details);

        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageJaquiesDetail', {
            pathArr: [
                { url: 'manageFounder', page: 'manage founder' },
                { url: 'manageFounder/details', page: 'details' }
            ],
            details,
            errMessage
        })

    } catch (err) {
        next(err)
    }
}


exports.getManageFounderAppearancePage = async (req, res, next) => {
    try {

        const result = await founder.fetchAppearances();
        const appearances = result[0].appearances;
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_jaquiesAppearance', {
            pathArr: [
                { url: 'manageFounder', page: 'manage founder' },
                { url: 'manageFounder/appearances', page: 'appearances' }
            ],
            appearances,
            errMessage
        });

    } catch (err) {
        next(err)
    }
}

exports.fetchAppearance = async (req, res, next) => {
    try {
        const id = req.query.id;

        const result = await founder.fetchAppearances();
        const dataArr = result[0].appearances;

        const data = dataArr.filter(e => e.id === id);
        if (data.length < 1) {
            res.status(404).json({ msg: 'internal server error' })
        }

        res.status(200).json({
            data: data[0]
        });
    } catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}

exports.createFounderAppearance = async (req, res, next) => {
    try {

        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            // Send Invalid Inputs page
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/manageFounder/appearances');
        }

        const { videoTitle, videoLink } = req.body;
        if (!videoTitle || !videoLink) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/manageFounder/appearances');
        }

        const result = await founder.createAppearance({ videoTitle, videoLink });
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }

        res.redirect('/admin/manageFounder/appearances');

    } catch (err) {
        next(err)
    }
}

exports.editFounderAppearance = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageFounder/appearances');
        }

        const { videoTitle, videoLink } = req.body;
        if (!videoTitle || !videoLink) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/manageFounder/appearances');
        }
        const id = req.query.id;

        const result = await founder.editAppearance(id, { videoTitle, videoLink });

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }

        res.redirect('/admin/manageFounder/appearances');
    } catch (err) {
        next(err)
    }
}

exports.deleteFounderAppearance = async (req, res, next) => {
    try {


        const id = req.body.id;

        const appearances = await founder.fetchAppearances();
        const newAppearances = appearances[0].appearances.filter(e => e.id !== id);

        const result = await founder.deleteAppearance(newAppearances);
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }

        res.redirect('/admin/manageFounder/appearances');
    } catch (err) {
        next(err)
    }
}

// ---------- Get Founder Blogs ------------- //

exports.getFounderBlogsPage = (req, res, next) => {
    try {

        const errMessage = req.flash('errMessage')[0]

        res.render('./templates/adminPannel/ad_manageJaquiesBlog', {
            pathArr: [
                { url: 'manageFounder', page: 'manage founder' },
                { url: 'manageFounder/blogs', page: 'blogs' }
            ],
            errMessage
        })
    } catch (err) {
        next(err)
    }
}


exports.uploadBlogPic = async (req, res, next) => {
    try {
        const mimeTypeErr = req.flash('passOnMsg');
        const image = req.file;
        let imagePath = null;

        if (image) {
            imagePath = `/${image.path}`;
        }

        if (mimeTypeErr.length > 0 || !imagePath) {
            // Send Invalid JSON response
            throw new Error();
        }

        const tempId = req.query.tempId;

        // Get previous image Array
        const prev = await temp.getTemp(tempId);

        if (!prev) {
            // If prev dont exist run create new temp Array
            const newTemp = new temp(tempId, imagePath);
            await newTemp.save();
        } else {
            // If prev exist update the temp Array
            await temp.updateTemp(tempId, imagePath)
        }

        res.status(200).json({ location: imagePath })
    } catch (err) {
        res.status(404).json({ msg: 'error in uploading' })
    }
}

exports.delTempBlogsArr = async (req, res, next) => {
    try {

        const tempId = req.query.tempId;

        const prev = await temp.getTemp(tempId);

        if (prev) {
            prev.imgArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        // Delete in temp database
        await temp.delTemp(tempId)

        // Send success message
        res.status(200).json({ success: true })

    } catch (err) {
        // Send error message
        res.status(404).json({ success: false })
    }
}

exports.createFounderBlogs = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = null;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please try again')
            if (imageUrl) { fileHelper.delFile(imageUrl) }
            return res.redirect('/admin/manageFounder/blogs');
        }

        const { title, content, active } = req.body;
        const tempId = req.query.tempId;

        // Get array from tempId
        const tempImgArr = await temp.getTemp(tempId);

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        if (tempImgArr) {
            imgArr = tempImgArr.imgArr.filter(c => {
                if (content.includes(c)) {
                    return c;
                } else {
                    trashArr.push(c)
                }
            })
        };


        const create = new blogs('jacquie jordan', title, content, active, imageUrl, imgArr);
        const result = await create.save();

        // Check if inserting to blogs is successful
        if (!result || !result.insertedId) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
            return res.redirect('/admin/manageFounder/blogs');
        }
        const blogId = result.insertedId;

        const blogRes = await founder.createBlog(blogId);
        if (!blogRes.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }


        // Delete Trash
        if (tempImgArr) {
            temp.delTemp(tempId);
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        res.redirect('/admin/manageFounder/blogs');

    } catch (err) {
        next(err)
    }
}


exports.searchFounderBlogs = async (req, res) => {
    try {
        // Get pageNum
        const pageNum = req.query.pageNum;

        const lookupBlog = await founder.lookupBlogs(pageNum);
        const data = lookupBlog.map(c => ({ ...c.allBlogs }));
        const totalItems = await founder.getBlogsCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.getABlog = async (req, res) => {
    try {// Get keyword and pageNum

        const id = req.query.id;
        const blog = await founder.getBlogsArr();
        const blogsArr = blog[0].blogs;

        // Check if id exist in blogsArr
        const filter = blogsArr.filter(r => r == id);

        if (!id || filter.length < 1) {
            throw new Error('Invalid blog ID');
        }

        // Get the object
        const data = await blogs.fetchABlog(id);
        data.active = data.activeStats;

        // send object
        res.status(200).json(data);

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.editFounderBlogs = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = null;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please try again')
            return res.redirect('/admin/manageFounder/blogs');
        }

        const id = req.query.id;
        const tempId = req.query.tempId;

        const prevBlog = await blogs.fetchABlog(id);
        if (!prevBlog || prevBlog.authorId !== 'jacquie jordan') {
            req.flash('errMessage', 'something went wrong!, please try again')
            return res.redirect('/admin/manageFounder/blogs');
        }

        const { title, content, active } = req.body;

        const tempArr = await temp.getTemp(tempId);
        let tempImgArr = [];
        if (tempArr) { tempImgArr = tempArr.imgArr }

        // Make an image array
        if (!prevBlog.imgArr) {
            prevBlog.imgArr = []
        }
        const mixImgArr = [...prevBlog.imgArr, ...tempImgArr];

        if (imageUrl) {
            finalObj.imageUrl = imageUrl;
        }

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        imgArr = mixImgArr.filter(c => {
            if (content.includes(c)) {
                return c;
            } else {
                trashArr.push(c)
            }
        })

        const prevImg = prevBlog.imageUrl;

        const finalObj = {
            title, content,
            activeStats: active ? true : false,
            imgArr
        }

        // Update the blog
        const result = await blogs.updateBlog(id, finalObj);
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }

        if (prevImg && prevImg.includes('blogs')) {
            // Delete the prev image
            fileHelper.delFile(prevImg);
        }

        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        if (tempImgArr) {
            temp.delTemp(tempId);
        }

        res.redirect('/admin/manageFounder/blogs');
    } catch (err) {
        next(err)
    }
}


exports.deleteAdminBlogs = async (req, res) => {
    try {
        // Get and store the ID
        const id = req.query.id;
        if (!id) {
            throw new Error('No id found');
        }

        // Delete in founder array
        const fetchFounderBlog = await founder.getBlogsArr(id);
        const founderBlog = fetchFounderBlog[0].blogs;
        const updatedBlogs = founderBlog.filter(c => c != id);
        if (founderBlog.length <= updatedBlogs.length) { throw new Error('failed') };

        // update the array
        const res1 = await founder.editBlogs(updatedBlogs);

        if (res1.modifiedCount < 1) {
            throw new Error('couldnot update');
        }

        // Delete in database
        const prev = await blogs.fetchABlog(id);
        const prevImg = prev.imageUrl;
        if (!prev) {
            throw new Error('No id found');
        }
        const delDb = await blogs.delBlog(prev._id);
        if (delDb.deletedCount < 1) {
            throw new Error('No id found');
        }

        // Delete the image in disk
        if (prevImg && prevImg.includes('blogs')) {
            // Delete the prev image
            fileHelper.delFile(prevImg);
        }

        if (prev.imgArr) {
            prev.imgArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        // Send success report
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


////////////////////////////---
//--- Manage Products ---//-----
///////////////////////////---

exports.getManageProdsPage = (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        res.render('./templates/adminPannel/ad_manageProducts', {
            pathArr: [
                { url: 'products', page: 'manage products' },
            ],
            errMessage
        })

    } catch (err) {
        next(err)
    }
}

exports.getBookDetails = async (req, res, next) => {
    try {

        const id = req.query.id;
        const book = await books.fetchABook(id);
        if (!book) {
            throw new Error;
        }
        res.status(200).json(book);

    } catch (err) {
        res.status(404).json({ msg: 'Could not fetch book details' })
    }
}

exports.createProduct = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = null;

        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }


        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/products');
        }
        if (!imageUrl) {
            req.flash('errMessage', 'Please attach an image')
            return res.redirect('/admin/products');
        }

        const prods = new products(imageUrl, { ...req.body });
        const result = await prods.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
        }


        res.redirect('/admin/products');
    } catch (err) {
        next(err)
    }
}


exports.editProduct = async (req, res, next) => {
    try {

        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');
        const id = req.query.id;

        let prevImg = null;
        let imgSrc = null;

        if (req.file) {
            imgSrc = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/products');
        }

        const finalObj = { ...req.body };

        // Setting up the checkboxes
        finalObj.active = req.body.active ? true : false;
        finalObj.bestSeller = req.body.bestSeller ? true : false;
        if (!req.body.thirdpartyUrl) {
            finalObj.thirdpartyUrl = null;
        }

        if (imgSrc) {
            // Get previous image path
            const prevProd = await products.fetchAProduct(id);
            prevImg = prevProd.imgSrc;

            // Update the finalObject
            finalObj.imgSrc = imgSrc;
        }

        // Update the database
        const result = await products.updateProd(id, finalObj);
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Could not edit the product. Try again')
        }

        // Delete the previous image from disk
        if (prevImg && prevImg.includes('products')) {
            // Delete the prev image
            fileHelper.delFile(prevImg);
        }

        res.redirect('/admin/products');
    } catch (err) {
        next(err)
    }
}


exports.searchActiveProds = async (req, res) => {

    try {
        // Get pageNum
        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords;

        const data = await products.fetchAllProds(pageNum, keyword);
        const totalItems = await products.allProdsCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}



exports.searchInactiveProds = async (req, res) => {

    try {
        // Get pageNum
        const pageNum = req.query.pageNum;
        const keyword = req.query.keywords

        const data = await books.fetchAllBooks(pageNum, keyword);
        data.forEach(c => {
            c.imgSrc = c.imageUrl
        })
        const totalItems = await books.allBooksCount()

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.searchEditProds = async (req, res) => {

    try {// Get keyword and pageNum

        const id = req.query.id;

        if (!id) {
            // Send error report
            return new Error('Invalid blog ID');
        }

        // Get data from database
        const obj = await products.fetchAProduct(id);
        // send object
        res.status(200).json(obj);

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}

exports.removeProds = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Get previous image
        const prevProd = await products.fetchAProduct(id);

        // Req db to remove from shop
        const result = await products.removeProd(id);
        if (!result.deletedCount > 0) {
            throw new Error('could not delete. please try again');
        }

        // Send Output
        res.status(200).json({ success: true })

        const prevImg = prevProd.imgSrc;

        // Delete the previous image from disk
        if (prevImg && prevImg.includes('products')) {
            // Delete the prev image
            fileHelper.delFile(prevImg);
        }
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


////////////////////////////---
//--- Manage General Settings ---//-----
///////////////////////////---

exports.getGereralSettings = async (req, res, next) => {

    try {

        const data = await settings.fetchAll();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_generalSettings', {
            pathArr: [
                { url: 'siteSettings', page: 'manage settings' },
                { url: 'generalSettings', page: 'general settings' }
            ],
            data, errMessage
        })
    } catch (err) {
        next(err)
    }
}

exports.editPaypalSettings = async (req, res, next) => {

    try {
        const validationRes = validationResult(req);
        // Send Bad requests to errors

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/generalSettings');
        }

        const finalObj = {
            paypalEmail: req.body.paypalEmail,
            paypalUsername: req.body.paypalUsername,
            paypalPassword: req.body.paypalPassword,
            paypalSignature: req.body.paypalSignature
        }

        const result = await settings.updatePaypalSettings(finalObj);
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }

        res.redirect('/admin/generalSettings');
    } catch (err) {
        next(err)
    }
}

exports.editSeoSettings = async (req, res, next) => {
    try {

        const validationRes = validationResult(req);
        // Send Bad requests to errors

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/generalSettings');
        }

        const finalObj = {
            siteKeywords: req.body.siteKeywords,
            siteDescription: req.body.siteDescription
        }

        const result = await settings.updateSeoSettings(finalObj);
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }


        res.redirect('/admin/generalSettings');
    } catch (err) {
        next(err);
    }
}


exports.editSiteSettings = async (req, res, next) => {

    try {

        const validationRes = validationResult(req);
        // Send Bad requests to errors

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/generalSettings');
        }

        const title = 'site_settings';

        let otherEmailsArr = [];
        let mailAddressArr = [];

        const otherEmail = req.body.otherEmail;
        const emailTitle = req.body.emailTitle;
        const mailAddress = req.body.mailAddress;

        if (Array.isArray(otherEmail)) {
            if (!Array.isArray(emailTitle) || emailTitle.length !== otherEmail.length) {
                req.flash('errMessage', 'Invalid Email or Email Title');
                return res.redirect('/admin/generalSettings');
            }
        } else {
            if (Array.isArray(emailTitle)) {
                req.flash('errMessage', 'Invalid Email or Email Title');
                return res.redirect('/admin/generalSettings');
            }
        }

        if (otherEmail) {
            if (Array.isArray(otherEmail) && Array.isArray(emailTitle) && otherEmail.length > 0) {
                otherEmail.forEach((c, i) => {
                    otherEmailsArr.push({
                        email: c,
                        title: emailTitle[i]
                    })
                })
            } else {
                otherEmailsArr.push({
                    email: otherEmail,
                    title: emailTitle
                })
            }
        }

        if (mailAddress) {
            if (Array.isArray(mailAddress) && mailAddress.length > 0) {
                mailAddressArr = [...mailAddress]
            } else {
                mailAddressArr.push(mailAddress)
            }
        }

        const finalObj = {
            siteName: req.body.siteName,
            adminEmail: req.body.adminEmail,
            otherEmail: otherEmailsArr,
            mailAddress: mailAddressArr,
            phone: req.body.phone,
            copyright: req.body.copyright,
            twitter: req.body.twitter,
            facebook: req.body.facebook,
            youtube: req.body.youtube,
            instagram: req.body.instagram,
            radioshow: req.body.radioshow,
            companyTagLine: req.body.companyTagLine
        }

        const result = await settings.updateGensettings(title, finalObj);
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }


        res.redirect('/admin/generalSettings')

    } catch (err) {
        next(err)
    }
}

////////////////////////////---
//--- Manage Services ---//-----
///////////////////////////---

exports.getServicesPage = async (req, res, next) => {

    try {

        const data = await services.fetchAllService();
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageServicePage', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/services', page: 'services' }
            ],
            data, errMessage
        })

    } catch (err) {
        next(err)
    }

}

// ------------------------------------------

exports.uploadServicePic = (req, res) => {
    try {
        const mimeTypeErr = req.flash('passOnMsg');

        if (mimeTypeErr.length > 0) {

            // Send Invalid JSON response
            throw new Error();
        }

        const image = req.file;
        let imagePath = null;

        if (image) {
            imagePath = `/${image.path}`;
        }

        if (!imagePath) { throw new Error() };

        res.status(200).json({ location: imagePath })

    } catch (err) {
        res.status(404).json({ message: 'Image Uploading Failed' })
    }

}

// ------------------------------------------

exports.searchAService = async (req, res) => {

    try {// Get keyword and pageNum

        const id = req.query.id;

        if (!id) {

            // Send error report
            throw new Error();
        }

        // Get data from database
        const service = await services.fetchService(id);

        // send object
        res.status(200).json(service);

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}

// ------------------------------------------

exports.deleteService = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Req db to remove from shop

        const delStatus = await services.delete(id);
        if (!delStatus.deletedCount > 0) {
            throw new Error();
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


// ------------------------------------------

exports.createService = async (req, res, next) => {
    try {

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { title, short_description, description, active } = req.body;
        let activeStats = false;
        if (active) { activeStats = true };

        // Creating the service
        const service = new services(title, short_description, description, activeStats);
        const result = await service.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating hotTopicCategory. Try again later');
        }

        res.redirect('/admin/userInterfaces/frontend/services');

    } catch (err) {
        return next(err)
    }
}

// ------------------------------------------

exports.editService = async (req, res, next) => {

    try {
        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);


        if (validationRes.errors.length > 0) {
            // Send Invalid Inputs page
            return res.redirect('/error/invalidInput');
        }

        // Get the fields
        const { title, short_description, description, active } = req.body;
        let activeStats = false;
        if (active) { activeStats = true };

        // Creating updated object
        const obj = { title, shortDescription: short_description, description, active: activeStats };

        // Editing the service
        const result = await services.update(id, obj);

        // If update is not successful then flash error message
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }

        res.redirect('/admin/userInterfaces/frontend/services');

    } catch (err) {
        next(err)
    }
}

////////////////////////////---
//--- Manage Sub Admins ---//-----
///////////////////////////---

exports.createAdmin = async (req, res, next) => {

    try {
        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        // Send Bad requests to errors

        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0 || !req.file) {
            // Send Invalid Inputs page
            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/manageSubAdmin');
        }

        if (req.body.password !== req.body.confirmPassword) {
            req.flash('errMessage', 'Password & Confirm Password don\'t match');
            return res.redirect('/admin/manageSubAdmin');
        }


        // Get the fields
        const { firstName, lastName, address, phone, email, userName, password, active } = req.body;
        let activeStats = false;
        if (active) { activeStats = true };

        // Checking for username Existance
        try {
            const prevUser = await admin.fetchAdmin(userName);
            if (prevUser) {
                req.flash('errMessage', 'User already exist. Try a different username');
                return res.redirect('/admin/manageSubAdmin');
            }
        } catch (err) {
            if (err) {
                req.flash('errMessage', 'User already exist. Try a different username');
                return res.redirect('/admin/manageSubAdmin');
            }
        }

        // Hashing the password
        const hashedPassword = await bcrypt.hash(password, 12);

        const role = roles.subAdmin;
        const imageUrl = `/${req.file.path}`;


        // Creating the service
        const newAdmin = new admin(role, firstName, lastName, imageUrl, address, phone, email, userName, hashedPassword, activeStats);
        const result = await newAdmin.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in creating Admin. Try again later');
        }

        res.redirect('/admin/manageSubAdmin');

    } catch (err) {
        next(err)
    }
}


exports.editAdminBasics = async (req, res, next) => {

    try {

        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        if (validationRes.errors.length > 0 || mimeTypeErr.length > 0) {

            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/manageSubAdmin');
        }

        // Get the fields
        const { firstName, lastName, address, phone, active } = req.body;

        let activeStats = false;
        if (active) { activeStats = true };

        const obj = { firstName, lastName, address, phone, activeStats };

        let imageUrl = null;
        let previmage = null;

        if (req.file) {
            imageUrl = req.file.path;
            obj['imageUrl'] = `/${imageUrl}`;
        }

        if (imageUrl) {
            const data = await admin.fetchAdminById(id);
            previmage = data.imageUrl;

        }

        const updateAdmin = await admin.update(id, obj);
        if (!updateAdmin.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating admin. Try again later');
        }
        if (previmage && previmage.includes('teamMembers')) {
            // Delete the prev image
            fileHelper.delFile(previmage);
        }

        res.redirect('/admin/manageSubAdmin');

    } catch (err) {
        next(err);
    }
}


exports.editAdminCredentials = async (req, res, next) => {

    try {
        const id = req.query.id;

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {
            req.flash('errMessage', 'Invalid Input');
            return res.redirect('/admin/manageSubAdmin');
        }

        if (req.body.password !== req.body.confirmPassword) {
            req.flash('errMessage', 'Password & Confirm Password don\'t match');
            return res.redirect('/admin/manageSubAdmin');
        }


        // Get the fields
        const { email, userName, password } = req.body;

        if (!email || !userName || !password) { throw new Error('Some fields are empty') };

        // Checking for username Existance
        try {
            const prevUser = await admin.fetchAdmin(userName);

            if (prevUser) {
                if (prevUser._id != id) {
                    req.flash('errMessage', 'User already exist. Try a different username');
                    return res.redirect('/admin/manageSubAdmin');
                }
            }
        } catch (err) {
            req.flash('errMessage', 'User already exist. Try a different username');
            return res.redirect('/admin/manageSubAdmin');
        }

        const hashedPassword = await bcrypt.hash(password, 12);
        const obj = { email, userName, password: hashedPassword };

        const updateAdmin = await admin.update(id, obj);
        if (!updateAdmin.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating admin. Try again later');
        }

        res.redirect('/admin/manageSubAdmin');

    } catch (err) {
        next(err)
    }
}

exports.getManageSubAdminPage = async (req, res, next) => {

    try {

        const data = await admin.fetchAllSubAdmins();

        data.forEach(d => {
            const newDate = dateFormat(d.createdAt, 'd mmm yyyy');
            d.createdAt = newDate;
        })


        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_subAdmin', {
            pathArr: [
                { url: 'manageAdmins', page: 'admin settings' },
                { url: 'manageSubAdmin', page: 'sub admin settings' }
            ],
            errMessage,
            data
        })

    } catch (err) {
        next(err)
    }
}

exports.deleteSubAdmin = async (req, res, next) => {

    try {

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {

            req.flash('errMessage', 'Your Request couldn\'t be proceed. Please try again.');
            return res.redirect('/admin/manageSubAdmin');
        }

        const id = req.body.id;

        const data = await admin.fetchAdminById(id);
        const imageUrl = data.imageUrl;

        // Delete in database
        const delRes = await admin.deleteAdmin(id);
        if (!delRes.deletedCount > 0) {
            throw new Error();
        }

        if (imageUrl && imageUrl.includes('teamMembers')) {
            // Delete the prev image
            fileHelper.delFile(imageUrl);
        }

        res.redirect('/admin/manageSubAdmin');

    } catch (err) {
        next(err)
    }
}

////////////////////////////---
//--- Manage Tags ---//-----
///////////////////////////---

exports.getManageTagsPage = (req, res, next) => {

    try {
        const errMessage = req.flash('errMessage')[0]

        res.render('./templates/adminPannel/ad_manageTags', {
            pathArr: [
                { url: 'manageTags', page: 'manage tags' }
            ],
            errMessage
        })

    } catch (err) {
        next(err);
    }
}

exports.createTag = async (req, res, next) => {

    try {

        // See if there is some error in validation
        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageTags');
        }

        // Get the fields
        const { tag, active } = req.body;

        // Check if exists
        const prevTag = await tags.fetchTag(tag.toLowerCase());
        if (prevTag) {
            req.flash('errMessage', 'Tag already exists');
            return res.redirect('/admin/manageTags');
        }

        let activeStats = false;
        if (active) { activeStats = true };

        const createTag = new tags(tag, activeStats);
        const result = await createTag.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
            return res.redirect('/admin/manageTags');
        }

        res.redirect('/admin/manageTags');

    } catch (err) {
        next(err)
    }
}

exports.updateTags = async (req, res, next) => {

    try {// Get keyword and pageNum

        const validationRes = validationResult(req);

        if (validationRes.errors.length > 0) {

            req.flash('errMessage', 'Invalid inputs')
            // Send Invalid Inputs page
            return res.redirect('/admin/manageTags');
        }

        // Get the fields
        const { tag, active } = req.body;


        // Check if exists
        const prevTag = await tags.fetchTag(tag.toLowerCase());
        if (prevTag) {
            req.flash('errMessage', 'Tag already exists');
            return res.redirect('/admin/manageTags');
        }

        const id = req.query.id;


        let activeStats = false;
        if (active) { activeStats = true };

        const result = await tags.update(id, { keyword: tag.toLowerCase(), active: activeStats });

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
        }


        res.redirect('/admin/manageTags');
    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.searchTags = async (req, res) => {

    try {// Get keyword and pageNum

        const keyword = req.query.keywords;
        const pageNum = req.query.pageNum;

        const data = await tags.fetchParticularTags(pageNum, keyword);
        const totalItems = await tags.getCount();

        // Get data from database

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
};

exports.deleteTag = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Req db to remove from shop
        const delTag = await tags.delete(id);

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



///////////////////////////////---
//--- Newsletter History ---//-----
/////////////////////////////---

exports.searchNewsletterHistory = async (req, res) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const data = await newsletter.getByHistory(pageNum);
        const totalItems = await newsletter.getCount();
        // Get data from database

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


///////////////////////////////---
//--- Newsletter Subscribers ---//-----
/////////////////////////////---

exports.searchNewsletterSubscribers = async (req, res) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const keywords = req.query.keywords;

        // Get data from database
        const data = await newsletter.getNewsletterSubscribers(pageNum, keywords);
        const totalItems = await newsletter.getCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.removeNewsletterSubscribers = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Req db to remove from shop
        const delReq = await newsletter.deleteSubscribers(id);
        if (delReq.deletedCount < 1) { throw new Error('Could not delete') }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


exports.updateNewsletterSubscribers = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;
        const command = req.query.command;

        let activeStats;
        if (command === 'unblock') { activeStats = true }
        else if (command === 'block') { activeStats = false }
        else {
            new Error('command fail')
        }

        // Req db to remove from shop
        const result = await newsletter.updateSubscribers(id, { activeStats });
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Error have been occoured in updating. Try again later');
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}


///////////////////////////////---
//--- Newsletter Templates ---//-----
/////////////////////////////---



exports.createNewsletterTemplate = async (req, res, next) => {

    try {

        const mimeTypeErr = req.flash('passOnMsg');

        let template;
        if (req.file) {
            template = `/${req.file.path}`;
        }

        if (!template || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please try again');
            if (template) { fileHelper.delFile(template) }
            return res.redirect('/admin/manageNewsletters/templates');
        }

        const { title } = req.body;

        const createTemplate = new newsletter({ title, template });
        const result = await createTemplate.save();

        if (!result.insertedCount > 0) {
            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
            fileHelper.delFile(template)
        }

        res.redirect('/admin/manageNewsletters/templates');

    } catch (err) {
        next(err)
    }
}


exports.getNewsletterTemplatesPage = (req, res, next) => {

    try {
        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_newsletterTemplate', {
            pathArr: [
                { url: 'manageNewsletters', page: 'manage Newsletters' },
                { url: 'manageNewsletters/templates', page: 'Newsletters templates' }
            ],
            errMessage
        })
    } catch (err) {
        next(err);
    }
}

exports.searchNewsletterTemplates = async (req, res) => {

    try {
        // Get keyword and pageNum

        const pageNum = req.query.pageNum;
        const data = await newsletter.fetchTemplates(pageNum);
        const totalItems = await newsletter.getCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.removeNewsletterTemplate = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        const prevNewsletter = await newsletter.fetchNewsletter(id);
        if (!prevNewsletter) {
            throw new Error('something went wrong!')
        }

        // Req db to remove from shop
        const delReq = await newsletter.delete(id);
        if (delReq.deletedCount < 1) { throw new Error('Could not delete') }

        fileHelper.delFile(prevNewsletter.template)

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



exports.sendNewsletterTemplate = async (req, res) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Get  the template

        const prevTemplate = await newsletter.fetchNewsletter(id)
        if (!prevTemplate) {
            throw new Error('Could not find prev Template')
        }
        const active = prevTemplate.active ? false : true

        const update = newsletter.update(id, { active });
        if (update.modifiedCount < 1) {
            throw new Error('Failed to update in database')
        }

        // Send Output
        res.status(200).json({ textContent: active ? 'unpost' : 'post' })
    }
    catch (err) {
        res.status(404).json({ msg: 'failed to do task' });
    }
}



///////////////////////////////---
//--- Manage Testimonials---//-----
/////////////////////////////---

exports.getManageTestimonialsPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0]

        res.render('./templates/adminPannel/ad_manageTestimonials', {
            pathArr: [
                { url: 'manageTestimonials', page: 'manageTestimonials' },
            ],
            errMessage
        })
    } catch (err) {
        next(err)
    }
}


exports.delTempTestimonialArr = async (req, res, next) => {
    try {

        const tempId = req.query.tempId;
        const prevTestimonial = await temp.getTemp(tempId);
        
        if(!prevTestimonial){
            return res.status(200).json({success: true})
        }

        // Delete in temp database
        await temp.delTemp(tempId)

        prevTestimonial.imgArr.forEach(c => {
            fileHelper.delFile(c)
        })

        // Send success message
        res.status(200).json({ success: true })

    } catch (err) {
        // Send error message
        res.status(404).json({ success: false })
    }
}


exports.uploadTestimonialpic = async (req, res, next) => {
    try {

        const mimeTypeErr = req.flash('passOnMsg');
        const image = req.file;
        let imagePath = null;

        if (image) {
            imagePath = `/${image.path}`;
        }

        if (mimeTypeErr.length > 0 || !imagePath) {
            // Send Invalid JSON response
            throw new Error();
        }

        const tempId = req.query.tempId;

        // Get previous image Array
        const prev = await temp.getTemp(tempId);

        if (!prev) {
            // If prev dont exist run create new temp Array
            const newTemp = new temp(tempId, imagePath);
            await newTemp.save();
        } else {
            // If prev exist update the temp Array
            await temp.updateTemp(tempId, imagePath)
        }

        res.status(200).json({ location: imagePath })

    } catch (err) {
        res.status(404).json({ msg: 'error in uploading' })
    }
}


exports.createTestimonial = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = null;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0 || !imageUrl) {
            req.flash('errMessage', 'Invalid inputs')
            return res.redirect('/admin/manageTestimonials');
        }

        // Get the content from Array
        const { content, role, videoLink } = req.body;
        const tempId = req.query.tempId;

        // Get array from tempId
        const tempImgArr = await temp.getTemp(tempId);

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];

        if (tempImgArr) {
            imgArr = tempImgArr.imgArr.filter(c => {
                if (content.includes(c)) {
                    return c;
                } else {
                    trashArr.push(c)
                }
            })
        };

        // Refurnish things
        let videoLinkArr = [];
        let newRole = roles[role.toLowerCase()];
        if (videoLink) {
            if (!Array.isArray(videoLink)) {
                videoLinkArr = [videoLink]
            } else {
                videoLinkArr = videoLink
            }
        }

        // Make the final Array
        const finalObj = {
            ...req.body,
            vidLink: videoLinkArr,
            role: newRole,
            imageUrl,
            imgArr
        };

        const createTestimonial = new testimonials(finalObj);
        const result = await createTestimonial.save();

        if (result.insertedCount < 1) {
            req.flash('errMessage', 'Couldn\'t proceed your request. Try again later');
            return res.redirect('/admin/manageTestimonials');
        }

        // Delete Trash
        if (tempImgArr) {
            temp.delTemp(tempId);
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }

        // Redirect to home page
        res.redirect('/admin/manageTestimonials');

    } catch (err) {
        next(err)
    }
}


exports.editTestimonial = async (req, res, next) => {
    try {

        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl = null;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs, please try again')
            return res.redirect('/admin/manageTestimonials');
        }

        // Get the content from Array
        const { content, role, videoLink, active } = req.body;
        const tempId = req.query.tempId;
        const id = req.query.id;

        // Get previous testimonial
        const prevTestimonial = await testimonials.fetchATestimonial(id);
        const tempArr = await temp.getTemp(tempId);
        let tempImgArr = [];
        if (tempArr) { tempImgArr = tempArr.imgArr }

        if (!prevTestimonial) {
            // Security check if previous testimonial exists
            req.flash('errMessage', 'Such testimonial don\'t exist')
            return res.redirect('/admin/manageTestimonials');
        }

        // Make an image array
        const mixImgArr = [...prevTestimonial.imgArr, ...tempImgArr];

        // Make an image array to store all image path
        let imgArr = [];
        const trashArr = [];


        imgArr = mixImgArr.filter(c => {
            if (content.includes(c)) {
                return c;
            } else {
                trashArr.push(c)
            }
        })

        // Refurnish things
        let videoLinkArr = [];
        let newRole = roles[role.toLowerCase()];
        if (videoLink) {
            if (!Array.isArray(videoLink)) {
                videoLinkArr = [videoLink]
            } else {
                videoLinkArr = videoLink
            }
        }

        // Make the final Array
        const finalObj = {
            ...req.body,
            vidLink: videoLinkArr,
            role: newRole,
            active: active ? true : false,
            imgArr
        };
        if (imageUrl) {
            finalObj.imgSrc = imageUrl
        }


        // Push it in database
        const result = await testimonials.updateTestimonial(id, finalObj);
        if (result.modifiedCount < 1) {
            req.flash('errMessage', 'Request could not be completed. Try again later')
            return res.redirect('/admin/manageTestimonials');
        }

        // Delete Trash
        if (tempImgArr) {
            temp.delTemp(tempId);
        }
        if (trashArr.length > 0) {
            trashArr.forEach(c => {
                fileHelper.delFile(c)
            })
        }
        if (imageUrl) {
            fileHelper.delFile(prevTestimonial.imgSrc);
        }


        res.redirect('/admin/manageTestimonials');
    } catch (err) {
        next(err)
    }
}


exports.searchAllTestimonials = async (req, res) => {

    try {// Get keyword and pageNum

        const pageNum = req.query.pageNum;

        // Get data from database
        const data = await testimonials.fetchAllTestimonials(pageNum);
        const totalItems = await testimonials.testimonialsCount();

        // Send Output
        res.status(200).json({
            dataArr: data,
            pageNum,
            totalItems
        });

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.fetchTestimonial = async (req, res) => {

    try {// Get keyword and pageNum

        const id = req.query.id;

        if (!id) {
            // Send error report
            throw new Error('Invalid blog ID');
        }

        const obj = await testimonials.fetchATestimonial(id);
        obj.role = roles[obj.role];

        // send object
        res.status(200).json(obj);

    }
    catch (err) {
        res.status(404).json({ msg: 'internal server error' })
    }
}


exports.deleteTestimonial = async (req, res, next) => {

    try {
        // Get the Id
        const id = req.query.id;

        // Get previous testimonial
        const prev = await testimonials.fetchATestimonial(id);

        const { imgArr, imgSrc } = prev;

        // Req db to remove from shop
        const result = await testimonials.removeTestimonial(id);
        if (result.deletedCount < 1) {
            throw new Error('could not delete')
        }

        if (imgArr.length > 0) {
            imgArr.forEach(c => {
                fileHelper.delFile(c);
            })
        }
        if (imgSrc) {
            fileHelper.delFile(imgSrc);
        }

        // Send Output
        res.status(200).json({ success: true })
    }
    catch (err) {

        res.status(404).json({ msg: 'failed to do task' });
    }

}



////////////////////////////---
//--- Manage Home Page ---//-----
///////////////////////////---

exports.getManageHomePageSlides = async (req, res, next) => {
    try {
        const data = await tvgHome.fetchAllSlides();
        const slides = data.slides;
        const errMessage = req.flash('errMessage')[0];
        res.render('./templates/adminPannel/ad_manageSlides', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' },
                { url: 'userInterfaces/frontend/home/slides', page: 'manage slides' }
            ],
            errMessage,
            slides
        })

    } catch (err) {
        next(err)
    }
}

exports.editSlides = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs. Please check the requirements before saving')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/userInterfaces/frontend/home/slides');
        }

        const id = req.query.id;
        let slideIndex;
        let prevImg;

        const slidesArr = await tvgHome.fetchAllSlides();
        if (!slidesArr) {
            req.flash('errMessage', 'Could\'t update slide. Try again later')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/userInterfaces/frontend/home/slides');
        }

        const requiredSlide = slidesArr.slides.filter((c, i) => {
            if (c.id == id) {
                slideIndex = i;
                prevImg = c.imageUrl;
                return c;
            }
        });

        const finalObj = {
            ...requiredSlide[0],
            ...req.body,
            active: req.body.active ? true : false
        }
        if (imageUrl) {
            finalObj.imageUrl = imageUrl;
        }

        const finalSlides = [...slidesArr.slides];
        finalSlides[slideIndex] = finalObj;

        const update = tvgHome.updateHome({ slides: finalSlides });
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Could\'t update slide. Try again later')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/userInterfaces/frontend/home/slides');
        }

        if (imageUrl) {
            fileHelper.delFile(prevImg);
        }

        res.redirect('/admin/userInterfaces/frontend/home/slides');
    } catch (err) {
        next(err)
    }
}

/////////////////////////////////////
//---- Manage Company Tags ----//
/////////////////////////////////////
exports.editComapnyTags = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please see the requirements before saving')
            return res.redirect('/admin/userInterfaces/frontend/home/tagLines');
        }

        let companyTags = [];

        if (!Array.isArray(req.body.companyTag)) {
            companyTags.push(req.body.companyTag)
        } else {
            companyTags = req.body.companyTag
        }

        const updateHomeTags = await tvgHome.updateHome({ companyTags })
        if (updateHomeTags.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t update tags . Please try again.')
            return res.redirect('/admin/userInterfaces/frontend/home/tagLines');
        }

        res.redirect('/admin/userInterfaces/frontend/home/tagLines');
    } catch (err) {
        next(err)
    }
}

/////////////////////////////////////
//---- Manage Homepage guestperts page----//
/////////////////////////////////////
exports.editHomePageGuestpertsTags = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please see the requirements before saving');
            return res.redirect('/admin/userInterfaces/frontend/home/tagLines');
        }

        let upcomingAppearanceGuestpert = [];
        let upcomingAppearanceTitle = [];
        let finalArr = [];

        if (!Array.isArray(req.body.upcomingAppearanceGuestpert)) {
            upcomingAppearanceGuestpert.push(req.body.upcomingAppearanceGuestpert)
        } else {
            upcomingAppearanceGuestpert = req.body.upcomingAppearanceGuestpert
        }

        if (!Array.isArray(req.body.upcomingAppearanceTitle)) {
            upcomingAppearanceTitle.push(req.body.upcomingAppearanceTitle)
        } else {
            upcomingAppearanceTitle = req.body.upcomingAppearanceTitle
        }

        if (upcomingAppearanceGuestpert.length !== upcomingAppearanceTitle.length) {
            req.flash('errMessage', 'Couldn\'t update upcoming Appearances . Please try again.')
            res.redirect('/admin/userInterfaces/frontend/home/tagLines');
        }

        upcomingAppearanceGuestpert.forEach((c, i) => {
            finalArr.push({
                upcomingGuestpert: c,
                guestpertTitle: upcomingAppearanceTitle[i]
            })
        })

        const updateHomeTags = await tvgHome.updateHome({ upcomingAppearanceTag: finalArr })
        if (updateHomeTags.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t update upcoming Appearances . Please try again.')
        }

        res.redirect('/admin/userInterfaces/frontend/home/tagLines');
    } catch (err) {
        next(err)
    }
}

/////////////////////////////////////
//---- Manage Homepage hotTopics page----//
/////////////////////////////////////
exports.editHomePageHotTopicsTags = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please see the requirements before saving');
            return res.redirect('/admin/userInterfaces/frontend/home/tagLines');
        }

        let hotTopicTag = [];

        if (!Array.isArray(req.body.hotTopicTag)) {
            hotTopicTag.push(req.body.hotTopicTag)
        } else {
            hotTopicTag = req.body.hotTopicTag
        }

        const updateHomeTags = await tvgHome.updateHome({ hotTopicTag })
        if (updateHomeTags.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t update hotTopics . Please try again.')
        }

        res.redirect('/admin/userInterfaces/frontend/home/tagLines');
    } catch (err) {
        next(err)
    }
}

exports.getManageTagLinespage = async (req, res, next) => {
    const errMessage = req.flash('errMessage')[0];
    const allData = await tvgHome.fetchTagLines();

    res.render('./templates/adminPannel/ad_companyTags', {
        pathArr: [
            { url: 'userInterfaces', page: 'manage UI' },
            { url: 'userInterfaces/frontend', page: 'Frontend UI' },
            { url: 'userInterfaces/frontend/home', page: 'home' },
            { url: 'userInterfaces/frontend/home/tagLines', page: 'company tag lines' }
        ],
        errMessage,
        allData
    })
}

// -------------------------------------------------------
exports.getManageFeaturedGuestpertsPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        const { homepageGuestperts } = await tvgHome.fetchGuestpertsOnHome();

        res.render('./templates/adminPannel/ad_homePageGuestperts', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' },
                { url: 'userInterfaces/frontend/home/featuredGuestperts', page: 'featured guestperts' }
            ],
            errMessage,
            allGuestperts: homepageGuestperts
        })

    } catch (err) {
        next(err)
    }
}
// -------------------------------------------------------

exports.getManageHomePageHotTopicsPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        const { homepageHotTopics } = await tvgHome.fetchHotTopicsOnHome();

        res.render('./templates/adminPannel/ad_homePageHotTopics', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' },
                { url: 'userInterfaces/frontend/home/hotTopics', page: 'hot topics' }
            ],
            errMessage,
            allHotTopics: homepageHotTopics
        })

    } catch (err) {
        next(err)
    }
}

// -------------------------------------------------------

exports.getHomePageTestimonialsPage = async (req, res, next) => {
    try {

        const errMessage = req.flash('errMessage')[0]
        const { homepageTestimonials } = await tvgHome.fetchTestimonialsOnHome();

        res.render('./templates/adminPannel/ad_homePageTestimonials', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' },
                { url: 'userInterfaces/frontend/home/testimonials', page: 'testimonials' }
            ],
            errMessage,
            allTestimonials: homepageTestimonials
        })

    } catch (err) {
        next(err)
    }
}


// -------------------------------------------------------

exports.getManageHomePageBooksPage = async (req, res, next) => {
    try {

        const { homepageBooks } = await tvgHome.fetchBooksOnHome();

        res.render('./templates/adminPannel/ad_homePageBooksCarousel', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' },
                { url: 'userInterfaces/frontend/home/books', page: 'books' }
            ],
            dataArr: homepageBooks
        })
    } catch (err) {
        next(err)
    }
}


exports.updateContactText = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0) {
            req.flash('errMessage', 'Invalid inputs')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/userInterfaces/frontend/contact');
        }

        const prevImg = await tvgContact.getContactPageImage();

        const finalObj = {
            content: req.body.content
        }
        if (imageUrl) {
            finalObj.imageUrl = imageUrl
        }

        const update = await tvgContact.updateContactPage(finalObj);
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Couldn\'t update contact page . please try again')
        }

        if (imageUrl && prevImg) {
            fileHelper.delFile(prevImg.imageUrl)
        }
        res.redirect('/admin/userInterfaces/frontend/contact');
    } catch (err) {
        next(err)
    }
}



exports.addNewsletterSubscriber = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        let { email, curPath } = req.body;
        !curPath ? curPath = '/' : '';

        if (validationErr.errors.length > 0) {
            return res.redirect(curPath);
        }

        await axios({
            method: 'post',
            url: 'https://api.mailerlite.com/api/v2/subscribers/',
            headers: {
                "Content-Type": "application/json",
                "X-MailerLite-ApiKey": "dbb4de9b00233aaed147cb737142a280"
            },
            data: {
                email
            }
        })

        res.redirect(curPath);
    } catch (err) {
        next(err)
    }
}


exports.getGPannelDashboardPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        const data = await tvgGpannelDashboard.fetchGPannelDashboard()

        res.render('./templates/adminPannel/ad_guestpertAdmin', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/guestpertPannel', page: 'Guestpert Pannel UI' },
                { url: 'userInterfaces/guestpertPannel/dashboard', page: 'dashboard' }
            ],
            errMessage,
            updateUrl: '/admin/editGuestpertPannel/walkthrough',
            data
        })
    } catch (err) {
        next(err)
    }
}


exports.getPPannelDashboardPage = async (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0]
        const data = await tvgGpannelDashboard.fetchPPannelDashboard()

        res.render('./templates/adminPannel/ad_guestpertAdmin', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/producerPannel', page: 'producer pannel UI' },
                { url: 'userInterfaces/producerPannel/dashboard', page: 'dashboard' }
            ],
            errMessage,
            updateUrl: '/admin/editProducerPannel/walkthrough',
            data
        })
    } catch (error) {
        next(error)
    }
}


exports.editGuestpertPannel = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, see the requirements and try again')
            return res.redirect('/admin/userInterfaces/guestpertPannel/dashboard');
        }

        const { title, videoLink } = req.body;
        const finalObj = { title, videoLink }

        const update = tvgGpannelDashboard.editGuestpertPannelDashboard(finalObj)
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Could not update guestpert pannel. Please try again')
        }

        res.redirect('/admin/userInterfaces/guestpertPannel/dashboard');
    } catch (err) {
        next(err)
    }
}


exports.editProducerPannel = async (req, res, next) => {
    
    try {
        
        const validationErr = validationResult(req);
        
        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, see the requirements and try again')
            return res.redirect('/admin/userInterfaces/producerPannel/dashboard');
        }
        
        const { title, videoLink } = req.body;
        const finalObj = { title, videoLink }
        
        const update = tvgGpannelDashboard.editProducerPannelDashboard(finalObj)
        if (update.modifiedCount < 1) {
            req.flash('errMessage', 'Could not update producer pannel. Please try again')
        }
        
        res.redirect('/admin/userInterfaces/producerPannel/dashboard');
        
    } catch (error) {
        next(err)
    }
}


exports.getAdminOrdersPage = async (req, res, next) => {
    try {

        const allOrders = await ordersModels.fetchAllLatestOrders()
        allOrders.forEach(c => c.purchaseDate = dateFormat(c.purchaseDate, 'dS mmm yyyy, h:MM TT'))

        res.render('./templates/adminPannel/ad_orders', {
            pathArr: [
                { url: 'allOrders', page: 'all orders' },
            ],
            allOrders
        })
    } catch (err) {
        next(err)
    }
}

// -----------------------------------------------------

exports.getManageProducerPage = (req, res, next) => {
    try {

        const errMessage = req.flash('errMessage')[0];

        res.render('./templates/adminPannel/ad_manageProducers', {
            pathArr: [
                { url: 'manageUsers', page: 'manage users' },
                { url: 'manageUsers/producers', page: 'manage producers' }
            ],
            errMessage
        })
    } catch (err) {
        next(err)
    }
}


exports.createProducer = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);
        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (validationErr.errors.length > 0 || mimeTypeErr.length > 0 || !imageUrl) {

            if (!imageUrl) {
                req.flash('errMessage', 'please attach an image')
            } else {
                req.flash('errMessage', 'Invalid inputs. please try again later')
            }

            if (imageUrl) { fileHelper.delFile(imageUrl) };

            return res.redirect('/admin/manageUsers/producers');
        }

        const { firstName, lastName, prefix, suffix, title, address, phone, mobile, company, website, fax, username, email, password } = req.body;

        const prevProducerWithEmail = await producers.fetchProducerByEmail(email)
        const prevProducerWithUsername = await producers.fetchProducerByEmail(username)
        if (prevProducerWithEmail || prevProducerWithUsername) {

            req.flash('errMessage', 'User already exist. Please try changing email or username')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/manageUsers/producers');
        }

        const hashedPassword = await bcrypt.hash(password, 12);

        const finalObj = {
            firstName,
            lastName,
            prefix,
            suffix,
            title,
            address,
            phone,
            mobile,
            company,
            website,
            fax,
            username,
            email,
            password: hashedPassword,
            imgSrc: imageUrl,
            verified: true
        }

        const producer = new producers(finalObj);
        const createProducer = await producer.save();
        if (createProducer.insertedCount < 1) {
            req.flash('errMessage', 'Something went wrong. Please try again')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/manageUsers/producers');
        }

        res.redirect('/admin/manageUsers/producers');
    } catch (err) {
        next(err)
    }
}


exports.searchAllProducers = async (req, res, next) => {
    try {
        const { pageNum, keywords } = req.query;
        const totalItems = await producers.countTotal(keywords)
        const dataArr = await producers.fetchProducerAllInfo(pageNum, keywords);

        dataArr.forEach(c => {
            c.role = roles[c.role]
        })

        res.json({ dataArr, pageNum, totalItems })

    } catch (err) {
        next(err)
    }
}


exports.editProducerProfilePic = async (req, res, next) => {
    try {

        const mimeTypeErr = req.flash('passOnMsg');

        let imageUrl;
        if (req.file) {
            imageUrl = `/${req.file.path}`;
        }

        if (!imageUrl) {
            req.flash('errMessage', 'Please attach an image');
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/manageUsers/producers');
        }

        if (mimeTypeErr.length > 0 || !imageUrl) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')
            if (imageUrl) { fileHelper.delFile(imageUrl) };
            return res.redirect('/admin/manageUsers/producers');
        }

        const producerId = req.query.id;
        const prevInfo = await producers.fetchProducerById(producerId)

        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a producer with the attached id')
            if (imageUrl) fileHelper.delFile(imageUrl)
            return res.redirect('/admin/manageUsers/producers');
        }

        const result = await producers.updateProducer(producerId, { imgSrc: imageUrl });
        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Problem with uploading image. Please check the requirements again')
        }

        fileHelper.delFile(prevInfo.imgSrc)
        res.redirect('/admin/manageUsers/producers');
    } catch (err) {
        next(err)
    }
}


exports.editProducerPersonalInfo = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please try again.')
            return res.redirect('/admin/manageUsers/producers');
        }

        const producerId = req.query.id;
        const prevInfo = await producers.fetchProducerById(producerId)

        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a producer with the attached id')
            return res.redirect('/admin/manageUsers/producers');
        }
        const { firstName, lastName, prefix, suffix, title, } = req.body;
        const finalObj = { firstName, lastName, prefix, suffix, title, };

        const result = await producers.updateProducer(producerId, finalObj);

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Something went wrong. Please try again')
        }

        res.redirect('/admin/manageUsers/producers');
    } catch (err) {
        next(err)
    }
}


exports.editProducerContact = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please try again')
            return res.redirect('/admin/manageUsers/producers');
        }

        const producerId = req.query.id;
        const prevInfo = await producers.fetchProducerById(producerId)

        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a producer with the attached id')
            return res.redirect('/admin/manageUsers/producers');
        }
        const { address, phone, mobile, company, website, fax } = req.body;
        const finalObj = { address, phone, mobile, company, website, fax };

        const result = await producers.updateProducer(producerId, finalObj);

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Something went wrong. Please try again')
        }


        res.redirect('/admin/manageUsers/producers');
    } catch (err) {
        next(err)
    }
}


exports.editProducerCredentials = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please try again')
            return res.redirect('/admin/manageUsers/producers');
        }

        const producerId = req.query.id;
        const prevInfo = await producers.fetchProducerById(producerId)

        if (!prevInfo) {
            req.flash('errMessage', 'Could not found a producer with the attached id')
            return res.redirect('/admin/manageUsers/producers');
        }

        const { email, password } = req.body

        const prevEmail = await producers.fetchProducerByEmail(email)

        if (prevEmail && prevEmail._id.toString() !== prevInfo._id.toString()) {
            req.flash('errMessage', 'Email already exists. Please try a different one')
            return res.redirect('/admin/manageUsers/producers');
        }

        const hashedPassword = await bcrypt.hash(password, 12)
        const finalObj = { email, password: hashedPassword };
        const result = await producers.updateProducer(producerId, finalObj);

        if (!result.modifiedCount > 0) {
            req.flash('errMessage', 'Something went wrong. Please try again')
        }

        res.redirect('/admin/manageUsers/producers');
    } catch (err) {
        next(err)
    }
}


exports.deleteProducer = async (req, res, next) => {
    try {

        const { id } = req.query;
        const result = await producers.deleteProducer(id)

        if (!result.deletedCount > 0) {
            throw new Error('deleting failed')
        }

        res.json({ success: true })
    } catch (err) {
        res.status(404).json({ msg: 'Something went wrong. Please try again' })
    }
}



exports.fetchGuestpertWithEnquiries = async (req, res, next) => {
    try {

        const { pageNum, keywords } = req.query
        const dataArr = await enquiries.getEnquiriesGuestpertsForAdminPannel(pageNum, keywords)
        const totalItems = await guestperts.getAllGuestpertsCount()
        
        res.json({ dataArr, pageNum , totalItems})

    } catch (err) {
        res.status(404).json({ msg: 'something went wrong. Please try again later' })
    }
}



exports.fetchEnquiriesWithGuestpertId = async (req, res, next) => {
    try {

        const { guestpertId, pageNum } = req.query

        await enquiries.setToRead(guestpertId)
        const requiredEnquiries = await enquiries.getEnquiriesForAdminPannel(pageNum, guestpertId)
        const totalItems = await enquiries.getCountForGuestpertPannel(guestpertId)

        if (!requiredEnquiries) {
            throw new Error('Couldnot fetch Enquiries')
        }

        res.json({ dataArr: requiredEnquiries, pageNum, totalItems })

    } catch (err) {
        res.status(404).json({ msg: 'something went wrong. Please try again later' })
    }
}



exports.changeGuestpertView = async (req, res, next) => {

    try {

        const enquiryId = req.query.guestpertId;

        const prevEnquiry = await enquiries.getEnquiryById(enquiryId);
        if (!prevEnquiry) {
            throw new Error('Could not fetch enquiry')
        }

        let { guestpertView } = prevEnquiry;
        guestpertView ? guestpertView = false : guestpertView = true;
        const update = await enquiries.updateEnquiry(enquiryId, { guestpertView })
        if (update.modifiedCount < 1) {
            throw new Error('Could not update in database')
        }

        res.json({ textContent: !guestpertView ? 'Send to guestpert' : 'Remove from guestpert' })

    } catch (err) {

        res.status(404).json({ msg: 'something went wrong. Please try again later' })
    }
}


exports.getProducerEnqueriesPage = (req, res, next) => {
    try {
        const errMessage = req.flash('errMessage')[0];
        res.render('./templates/adminPannel/ad_producerEnquiries', {
            pathArr: [
                { url: 'producerEnquiries', page: 'producer Inquiries' },
            ],
            errMessage
        })

    } catch (err) {
        next(err)
    }
}



exports.editEnquiry = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please try again')
            return res.redirect('/admin/producerEnquiries');
        }

        const enquiryId = req.query.guestpertId;

        const { media, show, airDate, location, topic, rating } = req.body
        const finalObj = { media, show, topic, airDate, location, rating }

        const update = await enquiries.updateEnquiry(enquiryId, finalObj)
        if (update.modifiedCount < 1) {
            throw new Error('Could not update in database')
        }

        res.redirect('/admin/producerEnquiries');
    } catch (err) {

        next(err)
    }
}

exports.createEnquiry = async (req, res, next) => {
    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please Try again')
            return res.redirect('/admin/producerEnquiries');
        }

        const { media, show, airDate, location, topic, rating } = req.body;
        const { guestpertId } = req.query;
        const producerId = req.session.user._id;

        const finalObj = {
            media, show, topic, airDate, location, rating, guestpertId, producerId
        }

        const newEnquiries = new enquiries(finalObj)
        const create = await newEnquiries.save()

        if (create.insertedCount < 1) {
            req.flash('errMessage', 'Something went wrong. Please try again later')
        }

        res.redirect('/admin/producerEnquiries');
    } catch (err) {
        next(err)
    }
}


