const bcrypt = require('bcryptjs')

const getDb = require('../util/database').getDb;
const { validationResult } = require('express-validator');

exports.createGod = async(req, res, next)=>{

    try {
        
        const password = bcrypt.hashSync('pacificocean', 10)

        const admin = {
            role: '10101101001110',
            userName: 'aka11y2ksa49',
            activeStats: true,
            password,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString()
        }

        const db = getDb()

        const result = await db.collection('admin').insertOne(admin);

        if (result.insertedCount < 1) {
            res.send('Failed Task')
            return
        }
        
        res.send('<h1> Successfull</h1>')

    } catch (error) {
        next(error)
    }
}


exports.changeAdminPassword = async(req, res, next)=>{

    try {
        const validationErr = validationResult(req);

        if (validationErr.errors.length > 0) {
            req.flash('errMessage', 'Invalid inputs, Please Try again')
            return res.redirect('/admin/changePassword');
        }
        
        const {newPassword} = req.body
        const hashedPassword = bcrypt.hashSync(newPassword, 10)
        const db = getDb()
        
        const result = await db.collection('admin').updateOne({userName: 'aka11y2ksa49'}, {$set: {password: hashedPassword}});
        
        if (result.modifiedCount < 1) {
            
            req.flash('errMessage', 'Error in updating password')
            res.redirect('/admin/changePassword')
            return
        }
        
        res.redirect('/admin/dashboard')

    } catch (error) {
        next(error)
    }
}
