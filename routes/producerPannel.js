const express = require('express')
const { body } = require('express-validator');
const multer = require('multer');

const multerSettings = require('../util/multer');
const { alphaNumericSpace, urlCheck, nonEmpty, validMobile, numeric } = require('../util/validator');

const router = express.Router();

const producerPannelController = require('../controllers/producerPannel');
const producerAuthorize = require('../middleware/producer.middleware');

router.get('/dashboard',
    producerAuthorize.authorize,
    producerPannelController.getProducerDashboard
)

router.get('/enquiries',
    producerAuthorize.authorize,
    producerPannelController.getProducerEnquiries
)

router.get('/profileSettings',
    producerAuthorize.authorize,
    producerPannelController.getProfileSettingsPage
)

////////////////////////////////
//***** REST API ROUTES *****//
////////////////////////////////

router.get('/searchGuestperts/allInfo',
    producerAuthorize.authorize,
    producerPannelController.getAllGuestperts
)

router.get('/searchEnquiries',
    producerAuthorize.authorize,
    producerPannelController.getEnquiriesOfProducer
)


router.get('/verify', producerPannelController.setVerificationToProducer)


////////////////////////////////
//***** POST ROUTES *****//
////////////////////////////////

router.post('/createEnquiry',

    producerAuthorize.authorize,
    [
        body('media').custom(alphaNumericSpace),
        body('show').custom(alphaNumericSpace),
        body('airDate').custom(alphaNumericSpace),
        body('location').custom(alphaNumericSpace),
        body('topic').custom(alphaNumericSpace),
        body('rating').custom(numeric)
    ],
    producerPannelController.createEnquiry
    )
    
router.post('/editRating',
    producerAuthorize.authorize,
    body('rating').custom(numeric),
    producerPannelController.editRating

)

router.post('/changeProfilePicture',

    producerAuthorize.authorize,
    multer(
        {
            storage: multerSettings('Producer').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 1024 * 500 }
        }).single('profile'),
    producerPannelController.editProducerProfilePic
)

router.post('/editPersonalInfo',

    producerAuthorize.authorize,
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').custom(alphaNumericSpace).custom(nonEmpty).isLength({ max: 150 })
    ],
    producerPannelController.editProducerPersonalInfo
)

router.post('/editProducerContact',

    producerAuthorize.authorize,
    [
        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('mobile').custom(validMobile),
        body('company').isString(),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
    ],
    producerPannelController.editProducerContact
)

router.post('/editCredentials',

    producerAuthorize.authorize,
    [
        body('oldPassword').isString().isLength({ min: 6 }),
        body('password').isString().isLength({ min: 6 })
    ],
    producerPannelController.editProducerCredentials
)

router.post('/editEmail',
    producerAuthorize.authorize,
    [
        body('email').isEmail()
    ],
    producerPannelController.editProducerEmail
)


module.exports = router;