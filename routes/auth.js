const express = require('express');
const authController = require('../controllers/auth.controller');

const {nonEmpty, alphaNumericSpace, validMobile, urlCheck, checkbox} = require('../util/validator');

const {body} = require('express-validator');
const router = express.Router();

////////////////////////////////
//***** REST API routes *****//
////////////////////////////////

router.get('/logout_rest', authController.logoutUserRest)


////////////////////////////////
//***** URL routes *****//
////////////////////////////////

router.get('/login', authController.getGuestpertsLoginPage);

router.get('/admin_login', authController.getAdminLoginPage);

router.get('/forgotPassword', authController.getForgotPasswordPage)

router.get('/resetPassword', authController.getResetPasswordPage)

router.get('/register/guestpert', authController.getRegisterGuestpertPage)

router.get('/register/producer', authController.getRegisterProducerPage)

router.get('/logout', authController.logoutUser)

router.put('/user/verifyAccount', authController.verifyUsersAccount)

router.get('/resetEmail', authController.resetUserEmail)

router.get('/verifyUsers', authController.verifyUsers)


// ----------POST Requests ----------//

router.post('/auth/login', 
    [   
        body('username').isString(),
        body('password').isString()
    ],
    authController.loginUser
)

router.post('/admin/login', 
    [   
        body('username').isString(),
        body('password').isString()
    ],
    authController.loginAdmin
)

router.post('/registerNewGuestpert',
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('city').custom(alphaNumericSpace).custom(nonEmpty),
        body('country').custom(alphaNumericSpace).custom(nonEmpty),
        body('zipCode').isNumeric(),
        body('state').custom(alphaNumericSpace).custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
        body('knowingUs').custom(alphaNumericSpace),
        body('moreInfoCall').custom(checkbox),
        body('username').custom(nonEmpty).isAlphanumeric(),
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 }),
    ],
    authController.createGuestpert
)


router.post('/registerNewProducer',
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        
        body('phone').isMobilePhone(),
        body('mobile').custom(validMobile),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
        body('knowingUs').custom(alphaNumericSpace),

        body('username').custom(nonEmpty).isAlphanumeric(),
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 }),
    ],
    authController.createProducer

)

router.post('/resetPasswordRequest',
    body('username').isString().custom(nonEmpty),
    authController.requestResetPassForm
)

router.post('/resetPassword', 
    body('password').isString().isLength({min:6}),
    authController.resetUserPassword
)



module.exports = router;