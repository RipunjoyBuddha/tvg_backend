const express = require('express');

const router = express.Router();
const { body } = require('express-validator');
const { tiaiMongoIdCheck, alphaNumericSpace, nonEmpty } = require('../util/validator');

const frontendController = require('../controllers/frontEnd');

///////////////////////////
//***** URL routes *****//
//////////////////////////

router.get('/', frontendController.getTvgHomePage)

router.get('/about', frontendController.getAboutPage)

router.get('/guestperts/:id', frontendController.getGuestpertsPersonalPage)

router.get('/guestperts', (req, res, next) => {
    res.render('templates/front/guestpert',
        { keyword: '' }
    )
})

router.get('/newsletter/:id', frontendController.readNewsletter)

router.get('/hotTopics/search', frontendController.searchHotTopicsByTags)

router.get('/hotTopics/:title', frontendController.getAllHotTopicsPage)

router.get('/hotTopics', frontendController.getHotTopicCatPage)

router.get('/readTopic/:id', frontendController.getReadTopicPage)

router.get('/services', frontendController.getServicePage)

router.get('/faq', frontendController.getFaqPage)

router.get('/blog/:id', frontendController.readBlog)

router.get('/contact', frontendController.getContactPage)

router.get('/founder', frontendController.getFoundersPage)

router.get('/shop', frontendController.getShopPage)

router.get('/product/:id', frontendController.getProductDetailsPage)

router.get('/testimonials', frontendController.getTestimonialsPage)

router.get('/terms_of_use', frontendController.getTermsOfUsePage)

router.get('/privacy_policy', frontendController.getPrivacyPolicyPage)

////////////////////////////////
//***** REST API ROUTES *****//
////////////////////////////////

// Guestpert page routes -----
router.get('/allGuestperts', frontendController.getAllGuestpert); // For A-z guestpert list

// Home page routes -----
router.get('/searchguestpert/:keywords', frontendController.searchguestpertAutocomplete);

router.get('/searchHotTopics/:keywords', frontendController.searchHotTopicAutocomplete);

router.get('/searchguestpert', frontendController.searchGuestpert);

router.get('/fetchAllTestimonials', frontendController.fetchAllActiveTestimonials)


////////////////////////////////
//***** POST ROUTES *****//
////////////////////////////////

router.post('/searchHotTopics',
    [
        body('hotTopic-keyword').isString().escape('<', '>', '/', '\'', '"'),
        body('hotTopic-id').custom(tiaiMongoIdCheck)
    ],
    frontendController.searchTopicsFromHome
)
router.post('/searchGuestpert',
    [
        body('guestpert-keyword').isString().escape('<', '>', '/', '\'', '"'),
        body('guestpert-id').custom(tiaiMongoIdCheck)
    ],
    frontendController.SearchGuestpertFromHome
)

router.post('/contactTvg',
    [
        body('name').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('email').isEmail(),
        body('phone').isMobilePhone(),
        body('comment').isString()
    ],
    frontendController.sendContactMail
)


router.post('/orderProduct',
    [
        body('name').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('email').isEmail(),
        body('phone').isMobilePhone(),
        body('address').isString(),
        body('promo').isString()
    ],
    frontendController.registerOrder

)


module.exports = router;