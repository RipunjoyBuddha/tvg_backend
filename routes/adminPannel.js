const express = require('express');
const { body } = require('express-validator');
const multer = require('multer');

const adminAuthorize = require('../middleware/admin.middleware');
const multerSettings = require('../util/multer');
const { videoLinkValidator, validMobile, numeric, checkPassword, dateValidation, membershipValidation, urlCheck, videoLinkArrValidator, alphaNumericSpace, nonEmpty, EmailArray, validUrl, stringArray, checkbox } = require('../util/validator');

const router = express.Router();

const adminPannelController = require('../controllers/adminPannel');
const guestpertPannelController = require('../controllers/guestpertPannel');
const godController = require('../controllers/godController')


////////////////////////////////
//***** REST API ROUTES *****//
////////////////////////////////

router.get('/emailSettings/edit', adminAuthorize.restAuthorize, adminPannelController.getEmailSettings);

//-- HomePage Books Carousel Settings
router.get('/searchBooks', adminAuthorize.restAuthorize, adminPannelController.searchBooks);

router.put('/addBooksToHome', adminAuthorize.restAuthorize, adminPannelController.addBooksToHome);

router.put('/removeBooksFromHome', adminAuthorize.restAuthorize, adminPannelController.removeBooksFromHome);

//-- HomePage Featured Guestperts Settings
router.get('/searchGuestperts', adminAuthorize.restAuthorize, adminPannelController.searchGuestperts);

router.put('/addGuestpertsToHome', adminAuthorize.restAuthorize, adminPannelController.addGuestpertsToHome);

router.put('/removeGuestpertsFromHome', adminAuthorize.restAuthorize, adminPannelController.removeGuestpertsFromHome);

//-- HomePage Hot Topics Settings
router.get('/searchTopics', adminAuthorize.restAuthorize, adminPannelController.searchHotTopics);

router.put('/addTopicsToHome', adminAuthorize.restAuthorize, adminPannelController.addTopicsToHome);

router.put('/removeTopicsFromHome', adminAuthorize.restAuthorize, adminPannelController.removeTopicsFromHome);

//-- HomePage Testimonials Settings
router.get('/searchTestimonials/guestpert', adminAuthorize.restAuthorize, adminPannelController.searchGuestpertTestimonials);

router.put('/addTestimonialsToHome', adminAuthorize.restAuthorize, adminPannelController.addTestimonialsToHome);

router.put('/removeTestimonialFromHome', adminAuthorize.restAuthorize, adminPannelController.removeTestimonialsFromHome);

//-- Manage About Page
router.get('/manageAbout/edit',  adminAuthorize.restAuthorize, adminPannelController.getMemberDetails);

router.delete('/manageAbout/deleteMember', adminAuthorize.restAuthorize, adminPannelController.deleteTeamMember);

//-- ContactPage Testimonials Settings
router.get('/searchTestimonials/producer', adminAuthorize.restAuthorize, adminPannelController.searchProducerTestimonials);

router.put('/addTestimonialsToContact', adminAuthorize.restAuthorize, adminPannelController.addTestimonialsToContact);

router.put('/removeTestimonialFromContact', adminAuthorize.restAuthorize, adminPannelController.removeTestimonialsFromContact);

//-- Manage Faq Page
router.get('/manageFaq/edit', adminAuthorize.restAuthorize, adminPannelController.searchFaq);

router.delete('/manageFaq/remove', adminAuthorize.restAuthorize, adminPannelController.removeFaq)

//-- Manage Producers
router.get('/searchProducers', adminAuthorize.restAuthorize, adminPannelController.searchAllProducers)


//-- Manage Guestperts
router.get('/searchGuestperts/all_info', adminAuthorize.restAuthorize, adminPannelController.searchGuestpertsAllInfo);

router.get('/manageGuestperts/searchEdit', adminAuthorize.restAuthorize, adminPannelController.searchGuestpertsEdit);


router.delete('/manageGuestperts/deleteGuestpert', adminAuthorize.restAuthorize, adminPannelController.deleteGuestpert);

//-- Manage Founder
router.get('/searchFounderBlogs', adminAuthorize.restAuthorize, adminPannelController.searchFounderBlogs);

router.get('/manageFounderBlogs/edit', adminAuthorize.restAuthorize, adminPannelController.getABlog);

router.delete('/manageFounderBlogs/delete', adminAuthorize.restAuthorize, adminPannelController.deleteAdminBlogs);


//-- Manage Products
router.get('/searchProducts/active', adminAuthorize.restAuthorize, adminPannelController.searchActiveProds);

router.get('/searchProducts/inactive', adminAuthorize.restAuthorize, adminPannelController.searchInactiveProds);

router.get('/manageProducts/edit', adminAuthorize.restAuthorize, adminPannelController.searchEditProds);

router.put('/manageProducts/remove', adminAuthorize.restAuthorize, adminPannelController.removeProds);

router.get('/manageProducts/addBook', adminAuthorize.restAuthorize, adminPannelController.getBookDetails)


//-- Manage Services
router.get('/manageServices/getService', adminAuthorize.restAuthorize, adminPannelController.searchAService);

router.delete('/manageServices/delete', adminAuthorize.restAuthorize, adminPannelController.deleteService);


//-- Manage Tags
router.get('/searchTags', adminAuthorize.restAuthorize, adminPannelController.searchTags);

router.delete('/deleteTags', adminAuthorize.restAuthorize, adminPannelController.deleteTag);

//-- Newsletter History
router.get('/searchNewsletterHistory', adminAuthorize.restAuthorize, adminPannelController.searchNewsletterHistory);

//-- Newsletter Subscribers
router.get('/newsletterSubscribers/search', adminAuthorize.restAuthorize, adminPannelController.searchNewsletterSubscribers);

router.delete('/newsletterSubscribers/delete', adminAuthorize.restAuthorize, adminPannelController.removeNewsletterSubscribers);

router.put('/newsletterSubscribers/update', adminAuthorize.restAuthorize, adminPannelController.updateNewsletterSubscribers);


//-- Newsletter Subscribers
router.get('/newsletterTemplates/search', adminAuthorize.restAuthorize, adminPannelController.searchNewsletterTemplates);

router.delete('/newsletterTemplates/delete', adminAuthorize.restAuthorize, adminPannelController.removeNewsletterTemplate);

router.post('/newsletterTemplates/send', adminAuthorize.restAuthorize, adminPannelController.sendNewsletterTemplate);

//-- Manage Testimonials

router.delete('/tempBlogsArr/delete', adminAuthorize.restAuthorize, adminPannelController.delTempBlogsArr)

//-- Manage Testimonials
router.get('/manageTestimonials/search', adminAuthorize.restAuthorize, adminPannelController.searchAllTestimonials);

router.get('/manageTestimonials/edit', adminAuthorize.restAuthorize, adminPannelController.fetchTestimonial);

router.delete('/manageTestimonials/delete', adminAuthorize.restAuthorize, adminPannelController.deleteTestimonial);

router.delete('/tempTestimonialsArr/delete', adminAuthorize.restAuthorize, adminPannelController.delTempTestimonialArr);


router.get('/enquiriesWithGuestperts', adminAuthorize.restAuthorize, adminPannelController.fetchGuestpertWithEnquiries)

router.get('/getEnquiries', adminAuthorize.restAuthorize, adminPannelController.fetchEnquiriesWithGuestpertId)

router.put('/changeGuestpertView', adminAuthorize.restAuthorize, adminPannelController.changeGuestpertView)


////////////////////////////////
//***** URL ROUTES *****//
////////////////////////////////

router.get('/dashboard',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_dashboard', {
            pathArr: []
        })
    })

//-- Manage Admins

router.get('/manageAdmins',
    adminAuthorize.authorizeGodMode,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'manageAdmins', page: 'admin settings' }
            ],
            folderArr: [
                { icon: 'icon_manageAdmin', folderTxt: 'sub admin', url: '/admin/manageSubAdmin' },
                { icon: 'icon_changePassword', folderTxt: 'change password', url: '/admin/changePassword' }
            ],
            pageTitle: 'Manage Admins'
        })
    })

router.get('/manageSubAdmin', adminAuthorize.authorizeGodMode, adminPannelController.getManageSubAdminPage)


// Manage Admin Password
router.get('/changePassword',
    adminAuthorize.authorizeGodMode,
    (req, res, next) => {
        const errMessage = req.flash('errMessage')[0];
        res.render('./templates/adminPannel/ad_changePassword', {
            pathArr: [
                { url: 'manageAdmins', page: 'admin settings' },
                { url: 'changePassword', page: 'change admin password' }
            ],
            pageTitle: 'Admin Credentials',
            errMessage
        })
    }
)

//-- Manage Settings

router.get('/siteSettings',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'siteSettings', page: 'manage settings' }
            ],
            folderArr: [
                { icon: 'icon_generalSetting', folderTxt: 'general settings', url: '/admin/generalSettings' },
                { icon: 'icon_emailSetting', folderTxt: 'email settings', url: '/admin/emailSettings' }
            ],
            pageTitle: 'Manage Settings'
        })
    })

router.get('/generalSettings', adminAuthorize.authorize, adminPannelController.getGereralSettings)

router.get('/emailSettings', adminAuthorize.authorize, adminPannelController.getEmailSettingsPage)



//-- Manage pages

router.get('/userInterfaces/frontend/home/slides',
    adminAuthorize.authorize,
    adminPannelController.getManageHomePageSlides
)

router.get('/userInterfaces/frontend/home/TagLines',
    adminAuthorize.authorize,
    adminPannelController.getManageTagLinespage
)

router.get('/userInterfaces/frontend/home/featuredGuestperts',
    adminAuthorize.authorize,
    adminPannelController.getManageFeaturedGuestpertsPage
)

router.get('/userInterfaces/frontend/home/hotTopics',
    adminAuthorize.authorize,
    adminPannelController.getManageHomePageHotTopicsPage
)

router.get('/userInterfaces/frontend/home/testimonials',
    adminAuthorize.authorize,
    adminPannelController.getHomePageTestimonialsPage
)

router.get('/userInterfaces/frontend/home/books',
    adminAuthorize.authorize,
    adminPannelController.getManageHomePageBooksPage
)

// -------------------------------------
router.get('/userInterfaces/frontend/home',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_frontEndUI', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' },
                { url: 'userInterfaces/frontend/home', page: 'home' }
            ]
        })
    })

router.get('/userInterfaces/frontend/about', adminAuthorize.authorize, adminPannelController.getManageAboutPage)


router.get('/userInterfaces/frontend/hotTopicsCat', adminAuthorize.authorize, adminPannelController.getHotTopicsCatPage)

router.get('/userInterfaces/frontend/contact',
    adminAuthorize.authorize,
    adminPannelController.getManageContactPage
)

router.get('/userInterfaces/frontend/services',
    adminAuthorize.authorize,
    adminPannelController.getServicesPage)

router.get('/userInterfaces/frontend/faq', adminAuthorize.authorize, adminPannelController.getManageFaqPage)

// -------------------------------------

router.get('/userInterfaces/guestpertPannel/dashboard',
    adminAuthorize.authorize,
    adminPannelController.getGPannelDashboardPage
)

// -------------------------------------

router.get('/producerEnquiries',
    adminAuthorize.authorize,
    adminPannelController.getProducerEnqueriesPage
)

// -------------------------------------

router.get('/userInterfaces/frontend',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folderStructure2', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/frontend', page: 'Frontend UI' }
            ],
            pages: [
                { name: 'home', url: '/admin/userInterfaces/frontend/home' },
                { name: 'about us', url: '/admin/userInterfaces/frontend/about' },
                { name: 'hot topic category', url: '/admin/userInterfaces/frontend/hotTopicsCat' },
                { name: 'contact us', url: '/admin/userInterfaces/frontend/contact' },
                { name: 'services', url: '/admin/userInterfaces/frontend/services' },
                { name: 'faq', url: '/admin/userInterfaces/frontend/faq' }
            ],
            pageTitle: 'Change Frontend UI'
        })
    })

router.get('/userInterfaces/guestpertPannel',
    adminAuthorize.authorize,
    (req, res, next) => {

        res.render('./templates/adminPannel/ad_folderStructure2', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/guestpertPannel', page: 'guestpert pannel UI' }
            ],
            pages: [
                { name: 'guestpert dashboard', url: '/admin/userInterfaces/guestpertPannel/dashboard' },
            ],
            pageTitle: 'Change Guestpert UI',
        })
    })


router.get('/userInterfaces/producerPannel/dashboard',
    adminAuthorize.authorize,
    adminPannelController.getPPannelDashboardPage
)


router.get('/userInterfaces/producerPannel',
    adminAuthorize.authorize,
    (req, res, next) => {

        res.render('./templates/adminPannel/ad_folderStructure2', {
            pathArr: [
                { url: 'userInterfaces', page: 'manage UI' },
                { url: 'userInterfaces/producerPannel', page: 'producer pannel UI' }
            ],
            pages: [
                { name: 'producer dashboard', url: '/admin/userInterfaces/producerPannel/dashboard' },
            ],
            pageTitle: 'Change Producer UI',
        })
    }
)

// -------------------------------------

router.get('/userInterfaces',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'userInterfaces', page: 'change UI' },
            ],
            folderArr: [
                { icon: 'icon_Frontend', folderTxt: 'frontend', url: '/admin/userInterfaces/frontend' },
                { icon: 'icon_guestpertPanel', folderTxt: 'guestperts pannel', url: '/admin/userInterfaces/guestpertPannel' },
                { icon: 'icon_producerPannel', folderTxt: 'producers pannel', url: '/admin/userInterfaces/producerPannel' }
            ],
            pageTitle: 'Change UI'
        })
    })


// ---- Manage Newsletters

router.get('/manageNewsletters/templates',
    adminAuthorize.authorize,
    adminPannelController.getNewsletterTemplatesPage
)


router.get('/manageNewsletters/subscribers',
    adminAuthorize.authorize,
    (req, res, next) => {

        res.render('./templates/adminPannel/ad_newsletterSubscriber', {
            pathArr: [
                { url: 'manageNewsletters', page: 'manage Newsletters' },
                { url: 'manageNewsletters/subscribers', page: 'Newsletters subscribers' }
            ],

        })
    })

router.get('/manageNewsletters',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'manageNewsletters', page: 'manage Newsletters' }
            ],
            folderArr: [
                { icon: 'icon_newsletterTemplate', folderTxt: 'newsletter Templates', url: '/admin/manageNewsletters/templates' },
                { icon: 'icon_newsletterSubscriber', folderTxt: 'newsletter subscribers', url: '/admin/manageNewsletters/subscribers' }
            ],
            pageTitle: 'Manage Newsletters'
        })
    })


//-- Manage Tags

router.get('/manageTags',
    adminAuthorize.authorize,
    adminPannelController.getManageTagsPage)

//-- Manage Users

router.get('/manageUsers/guestperts',
    adminAuthorize.authorize,
    adminPannelController.getManageGuestpertPage);

router.get('/manageUsers/producers',
    adminAuthorize.authorize,
    adminPannelController.getManageProducerPage
)

router.get('/manageUsers',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'manageUsers', page: 'manage users' }
            ],
            folderArr: [
                { icon: 'icon_Guestpert', folderTxt: 'guestperts', url: '/admin/manageUsers/guestperts' },
                { icon: 'icon_producer', folderTxt: 'producers', url: '/admin/manageUsers/producers' }
            ],
            pageTitle: 'Manage Users'
        })
    })

//-- Manage Memberships

router.get('/manageMemberships',
    adminAuthorize.authorize,
    adminPannelController.getManageMembershipPage);

//-- Manage Founders
router.get('/manageFounder/appearances',
    adminAuthorize.authorize,
    adminPannelController.getManageFounderAppearancePage);

router.get('/manageFounder/details',
    adminAuthorize.authorize,
    adminPannelController.getFounderDetailsPage);

router.get('/manageFounder/blogs',
    adminAuthorize.authorize,
    adminPannelController.getFounderBlogsPage);


router.get('/manageFounder',
    adminAuthorize.authorize,
    (req, res, next) => {
        res.render('./templates/adminPannel/ad_folders', {
            pathArr: [
                { url: 'manageFounder', page: 'manage founder' }
            ],
            folderArr: [
                { icon: 'icon_FounderAppearances', folderTxt: 'appearances', url: '/admin/manageFounder/appearances' },
                { icon: 'icon_FounderDetails', folderTxt: 'founder details', url: '/admin/manageFounder/details' },
                { icon: 'icon_FounderBlogs', folderTxt: 'founder blogs', url: '/admin/manageFounder/blogs' }
            ],
            pageTitle: 'Manage Founder'
        })
    })


router.get('/products',
    adminAuthorize.authorize,
    adminPannelController.getManageProdsPage);

router.get('/allOrders',
    adminAuthorize.authorize,
    adminPannelController.getAdminOrdersPage
)

router.get('/manageTestimonials',
    adminAuthorize.authorize,
    adminPannelController.getManageTestimonialsPage)


// ----- POST REQUESTS ----- //

//---------------------------------------------//
// Route for Guestpert pannel walkthrough video-
//--------------------------------------------//

router.post(
    '/editGuestpertPannel/walkthrough',
    adminAuthorize.authorize,
    [
        body('title').custom(alphaNumericSpace),
        body('videoLink').custom(videoLinkValidator)
    ],
    adminPannelController.editGuestpertPannel
)

router.post(
    '/editProducerPannel/walkthrough',
    adminAuthorize.authorize,
    [
        body('title').custom(alphaNumericSpace),
        body('videoLink').custom(videoLinkValidator)
    ],
    adminPannelController.editProducerPannel
)

// ---------------Create Primary Team Member----------------//
router.post(
    '/createTeam/primary',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('teamMembers').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 1024*1000 }
        }).array('image'),

    [
        body('firstName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('title', 'invalid input').isString().escape('<', '>', '/').custom(nonEmpty),
        body('description', 'invalid input').isString(),
    ],
    adminPannelController.createPrimaryTeamMember
);

// ---------------Edit Primary Team Member----------------//
router.post(
    '/editTeam/primary/:id',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('teamMembers').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 1024*1000 }
        }).array('image'),

    [
        body('firstName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('title', 'invalid input').isString().escape('<', '>', '/').custom(nonEmpty),
        body('description', 'invalid input').isString(),
    ],
    adminPannelController.editPrimaryTeamMember
);
// ---------------Create Other Team Member----------------//

router.post(
    '/createTeam/others',
    adminAuthorize.authorize,
    [
        body('firstName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('title', 'invalid input').isString().escape('<', '>', '/').custom(nonEmpty),
    ],
    adminPannelController.createOtherTeamMember
);

// ---------------Edit Other Team Member----------------//
router.post(
    '/editTeam/others/:id',
    adminAuthorize.authorize,
    [
        body('firstName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName', 'invalid input').isString().custom(alphaNumericSpace).custom(nonEmpty),
        body('title', 'invalid input').isString().escape('<', '>', '/').custom(nonEmpty),
    ],
    adminPannelController.editOtherTeamMember
);




//-----------------------------------//
// Route for page /admin/userInterfaces/frontend/hotTopicsCat-
//-----------------------------------//

// ---------------Create Category for hot topic----------------//
router.post(
    '/createCategoryHotTopics',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('hotTopicsCategories').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 } // 200 KB
        }).single('image'),
    [
        body('catTitle', 'invalid input').isString().custom(nonEmpty),
    ],
    adminPannelController.createHotTopicCategory
);


// ---------------Edit Category for hot topic----------------//
router.post(
    '/editCategoryHotTopics',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('hotTopicsCategories').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 } // 200 KB
        }).single('image'),
    [
        body('catTitle', 'invalid input').isString().custom(nonEmpty),
    ],
    adminPannelController.editHotTopicCategory
);


//-----------------------------------//
// Route for page /admin/userInterfaces/frontend/services-
//-----------------------------------//

// --------- upload image for description via tinymce ---------//

router.post('/imageUpload/servicePage',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('services').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 } // 500 KB
        }).single('file'),

    adminPannelController.uploadServicePic
)

// --------- create a new service ---------//

router.post('/manageServices/create',
    adminAuthorize.authorize,
    [
        body('title').isString().isLength({ min: 0, max: 40 }).escape('<', '>', '/').custom(nonEmpty),
        body('short_description').isString().isLength({ min: 0, max: 200 }).escape('<', '>', '/'),
        body('description').isString()
    ],

    adminPannelController.createService
)
// --------- edit a new service ---------//

router.post('/editService/edit',
    adminAuthorize.authorize,
    [
        body('title').isString().isLength({ min: 0, max: 40 }).escape('<', '>', '/'),
        body('short_description').isString().isLength({ min: 0, max: 200 }).escape('<', '>', '/'),
        body('description').isString()
    ],

    adminPannelController.editService
)


//-----------------------------------//
// Route for page /admin/userInterfaces/frontend/faq-
//-----------------------------------//

router.post('/createFaq',
    adminAuthorize.authorize,
    [
        body('question').isString().isLength({ min: 0, max: 150 }).escape('<', '>', '/').custom(nonEmpty),
        body('answerTitle').isString().isLength({ min: 0, max: 100 }).escape('<', '>', '/'),
        body('videoLink').isString().custom(videoLinkValidator),
        body('answer').isString(),
    ],

    adminPannelController.createFaq
)


router.post('/editFaq',
    adminAuthorize.authorize,
    [
        body('question').isString().isLength({ min: 0, max: 150 }).escape('<', '>', '/').custom(nonEmpty),
        body('answerTitle').isString().isLength({ min: 0, max: 100 }).escape('<', '>', '/'),
        body('videoLink').isString().custom(videoLinkValidator),
        body('answer').isString(),
    ],

    adminPannelController.editFaq
)


router.post('/imageUpload/faqPage',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('faq').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 } // 500 KB
        }).single('file'),

    adminPannelController.uploadFaqPic
)



//-----------------------------------//
// Route for page /admin/manage subAdmin-
//-----------------------------------//

router.post('/createAdmin',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('teamMembers').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 1024000 } // 1MB
        }).single('image'),
    [
        body('firstName').isString().isLength({ min: 0, max: 50 }).escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('lastName').isString().isLength({ min: 0, max: 100 }).escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('address').isString().escape('<', '>', '/', '\'', '"'),
        body('phone').isMobilePhone(),
        body('email').isEmail(),
        body('userName').custom(nonEmpty).isString(),
        body('password').isLength({ min: 6 }).isString(),
        body('confirmPassword').isLength({ min: 6 }).isString()
    ],
    adminPannelController.createAdmin
)


router.post('/editAdminBasics',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('teamMembers').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 1024000 } // 1MB
        }).single('image'),
    [
        body('firstName').isString().isLength({ min: 0, max: 50 }).escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('lastName').isString().isLength({ min: 0, max: 100 }).escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('address').isString().escape('<', '>', '/', '\'', '"'),
        body('phone').isMobilePhone()
    ],
    adminPannelController.editAdminBasics
)


router.post('/editAdminCredentials',
    adminAuthorize.authorize,
    [
        body('email').isEmail(),
        body('userName').custom(nonEmpty).isString(),
        body('password').isLength({ min: 6 }).isString(),
        body('confirmPassword').isLength({ min: 6 }).isString()
    ],
    adminPannelController.editAdminCredentials
)

router.post('/deleteAdmin',
    adminAuthorize.authorize,
    [
        body('id').custom(nonEmpty).isMongoId()
    ],
    adminPannelController.deleteSubAdmin
)


router.post('/changeAdminCredentials',

    adminAuthorize.authorizeGodMode,

    [
        body('newPassword').isLength({ min: 6 }).isString()
    ],
    godController.changeAdminPassword

)


//-----------------------------------//
// Route for page /admin/generalSettings-
//-----------------------------------//

router.post('/editSiteSettings',
    adminAuthorize.authorize,
    [
        body('siteName').isString().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('adminEmail').isEmail().custom(nonEmpty),
        body('otherEmail').custom(EmailArray),
        body('emailTitle').custom(stringArray),
        body('mailAddress').custom(stringArray),
        body('phone').isMobilePhone(),
        body('copyright').isString(),
        body('twitter').custom(urlCheck),
        body('facebook').custom(urlCheck),
        body('youtube').custom(urlCheck),
        body('instagram').custom(urlCheck),
        body('radioshow').custom(urlCheck),
        body('companyTagLine').isString().custom(nonEmpty).isLength({ max: 250 })
    ],
    adminPannelController.editSiteSettings
)


router.post('/editSeoSettings',
    adminAuthorize.authorize,
    [
        body('siteKeywords').isString().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('siteDescription').isString().custom(nonEmpty).escape('<', '>', '/', '\'', '"')
    ],

    adminPannelController.editSeoSettings
)


router.post('/editPaypalSettings',
    adminAuthorize.authorize,
    [
        body('paypalEmail').isString().custom(nonEmpty),
        body('paypalUsername').isString().custom(nonEmpty),
        body('paypalPassword').isString().custom(nonEmpty),
        body('paypalSignature').isString().custom(nonEmpty)
    ],

    adminPannelController.editPaypalSettings
)


//-----------------------------------//
// Route for page /admin/emailSettings-
//-----------------------------------//

router.post('/createEmailSettings',
    adminAuthorize.authorize,
    [
        body('segment').isAlpha(),
        body('subject').isString().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('emailContent').isString().custom(nonEmpty)
    ],
    adminPannelController.createEmailTemplate
)

router.post('/editEmailSettings',
    adminAuthorize.authorize,
    [
        body('segment').isAlpha(),
        body('subject').isString().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('emailContent').isString().custom(nonEmpty)
    ],
    adminPannelController.editEmailTemplate
)


//-----------------------------------//
// Route for page /admin/emailSettings-
//-----------------------------------//

router.post('/createNewsletter',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings().fileStoragePdf,
            fileFilter: multerSettings().fileFilterPdf,
            limits: { fileSize: 5120000 }
        }).single('newsletter'),
    body('title').custom(alphaNumericSpace).isLength({ max: 30 }),
    adminPannelController.createNewsletterTemplate
)


//-----------------------------------//
// Route for page /admin/tagsSettings-
//-----------------------------------//

router.post('/createTag',
    adminAuthorize.authorize,
    [
        body('tag').isString().isAlphanumeric().isLength({ min: 0, max: 70 }).escape('<', '>', '/', '\'', '"')
    ],
    adminPannelController.createTag
)

router.post('/editTag',
    adminAuthorize.authorize,
    [
        body('tag').isString().isAlphanumeric().isLength({ min: 0, max: 70 }).escape('<', '>', '/', '\'', '"')
    ],
    adminPannelController.updateTags
)

//-----------------------------------//
// Route for page /admin/manageMemberships-
//-----------------------------------//

router.post('/editMembership',
    adminAuthorize.authorize,
    [
        body('membershipTitle').isAlpha().isLength({ max: 70 })
    ],
    adminPannelController.editMembership)

router.post('/createMembershipFeature',
    adminAuthorize.authorize,
    [
        body('featureTitle').isString().isLength({ max: 70 }).custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('stdActive').custom(checkbox),
        body('eliteActive').custom(checkbox),
        body('platinumActive').custom(checkbox),
    ],
    adminPannelController.createFeature
);

router.post('/editMembershipFeature',
    adminAuthorize.authorize,
    [
        body('featureTitle').isString().isLength({ max: 70 }).escape('<', '>', '/', '\'', '"'),
        body('featureId').isMongoId(),
        body('stdActive').custom(checkbox),
        body('eliteActive').custom(checkbox),
        body('platinumActive').custom(checkbox),
    ],
    adminPannelController.editFeature
);


//-----------------------------------//
// Route for page /admin/manageFounder/appearances-
//-----------------------------------//

router.post('/createFounderAppearance',
    adminAuthorize.authorize,
    [
        body('videoTitle').isString().isLength({ max: 50 }).custom(nonEmpty),
        body('videoLink').custom(videoLinkValidator).custom(nonEmpty)
    ],
    adminPannelController.createFounderAppearance
)

router.get('/founderAppearance/edit',
    adminAuthorize.restAuthorize,
    adminPannelController.fetchAppearance
)

router.post('/editFounderAppearance',
    adminAuthorize.authorize,
    [
        body('videoTitle').isString().isLength({ max: 50 }).custom(nonEmpty),
        body('videoLink').custom(videoLinkValidator).custom(nonEmpty)
    ],
    adminPannelController.editFounderAppearance
)

router.post('/deleteAppearance', adminPannelController.deleteFounderAppearance);

//-----------------------------------//
// Route for page /admin/manageFounder/details-
//-----------------------------------//

router.post('/imageUpload/founderDetails',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('teamMembers').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('file'),
    adminPannelController.founderDetailsImageUpload
);

router.post('/founderDetails',
    adminAuthorize.authorize,
    adminPannelController.updateFounderDetails
)

//-----------------------------------//
// Route for page /admin/manageFounder/blogs-
//-----------------------------------//

router.post('/createFounderBlog',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('blogs').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('imageFile'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 50 }).escape('<', '>', '/', '\'', '"'),
        body('content').isString().custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    adminPannelController.createFounderBlogs
)

//-- Manage Founder Blogs
router.post('/founderBlogUploadPic',
    adminAuthorize.restAuthorize,
    multer(
        {
            storage: multerSettings('blogs').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('file'),
    adminPannelController.uploadBlogPic
)


router.post('/editFounderBlog',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('blogs').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('imageFile'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 50 }).escape('<', '>', '/', '\'', '"'),
        body('content').isString().custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    adminPannelController.editFounderBlogs
)


//-----------------------------------//
// Route for page /admin/products-
//-----------------------------------//

router.post('/createProduct',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('products').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 }
        }).single('image'),
    [
        body('title').custom(nonEmpty).isString().isLength({ max: 200 }),
        body('paymentMode').custom(nonEmpty).isString().escape('<', '>', '/', '\'', '"').isLength({ max: 12 }),
        body('productType').isAlphanumeric().isLength({ max: 25 }),
        body('description').isString().custom(nonEmpty),
        body('active').custom(checkbox),
        body('bestSeller').custom(checkbox),
        body('currency').isAlpha(),
        body('price').isFloat().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('thirdpartyUrl').custom(validUrl),
        body('audioLink').custom(validUrl)
    ],
    adminPannelController.createProduct
)

router.post('/editProduct',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('products').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 }
        }).single('image'),
    [
        body('title').custom(nonEmpty).isString().isLength({ max: 200 }),
        body('paymentMode').custom(nonEmpty).escape('<', '>', '/', '\'', '"').isLength({ max: 12 }),
        body('productType').isAlphanumeric().isLength({ max: 25 }),
        body('description').isString().custom(nonEmpty),
        body('active').custom(checkbox),
        body('bestSeller').custom(checkbox),
        body('currency').isAlpha(),
        body('price').isFloat().custom(nonEmpty).escape('<', '>', '/', '\'', '"'),
        body('thirdpartyUrl').custom(validUrl),
        body('audioLink').custom(validUrl)
    ],
    adminPannelController.editProduct
)



//-----------------------------------//
// Route for page /admin/testimonials-
//-----------------------------------//
router.post('/imageUpload/testimonials',
    adminAuthorize.restAuthorize,
    multer(
        {
            storage: multerSettings('testimonials').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('file'),

    adminPannelController.uploadTestimonialpic
)

router.post('/createTestimonial',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('testimonials').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 } // Max 500KB
        }).single('image'),
    [
        body('role').isAlpha(),
        body('name').custom(alphaNumericSpace).isLength({ max: 100 }).custom(nonEmpty),
        body('title').custom(alphaNumericSpace).isLength({ max: 150 }).custom(nonEmpty),
        body('content').isString(),
        body('active').custom(checkbox),
        body('videoLink').custom(videoLinkArrValidator)
    ],
    adminPannelController.createTestimonial
)

router.post('/editTestimonial',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('testimonials').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 } // Max 500KB
        }).single('image'),
    [
        body('role').isAlpha(),
        body('name').custom(alphaNumericSpace).isLength({ max: 100 }).custom(nonEmpty),
        body('title').custom(alphaNumericSpace).isLength({ max: 150 }).custom(nonEmpty),
        body('content').isString(),
        body('active').custom(checkbox),
        body('videoLink').custom(videoLinkArrValidator)
    ],
    adminPannelController.editTestimonial
)


//-----------------------------------//
// Route for page /admin/producers-
//-----------------------------------//

router.post('/createProducer',

    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('Producer').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').custom(alphaNumericSpace).custom(nonEmpty).isLength({ max: 150 }),

        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('mobile').custom(validMobile),
        body('company').isString(),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),

        body('username').custom(nonEmpty).custom(alphaNumericSpace),
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 }),
        body('confirmPassword').isString().isLength({ min: 6 })
    ],
    adminPannelController.createProducer
)


router.post('/editProducer/profilePic',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('Producer').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 } // 500KB
        }).single('image'),

    adminPannelController.editProducerProfilePic
)

router.post('/editProducer/personal',
    adminAuthorize.authorize,
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').custom(alphaNumericSpace).custom(nonEmpty).isLength({ max: 150 })
    ],
    adminPannelController.editProducerPersonalInfo
)

router.post('/editProducer/contact',
    adminAuthorize.authorize,
    [
        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('mobile').custom(validMobile),
        body('company').isString(),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
    ],
    adminPannelController.editProducerContact
)


router.post('/editProducer/credentials',

    adminAuthorize.authorize,
    [
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 })
    ],
    adminPannelController.editProducerCredentials
)

router.delete('/manageProducers/deleteProducer',

    adminAuthorize.authorize,
    adminPannelController.deleteProducer
)


//-----------------------------------//
// Route for page /admin/guestperts-
//-----------------------------------//
router.post('/createGuestpert',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('guestperts').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').custom(alphaNumericSpace).custom(nonEmpty).isLength({ max: 150 }),
        body('tagLine').custom(alphaNumericSpace).custom(nonEmpty).isLength({ max: 650 }),
        body('primaryExpertise').custom(alphaNumericSpace),
        body('expandedExpertise').custom(alphaNumericSpace).isString(),

        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('city').custom(alphaNumericSpace).custom(nonEmpty),
        body('country').custom(alphaNumericSpace).custom(nonEmpty),
        body('zipCode').isNumeric(),
        body('phone').isMobilePhone(),
        body('state').custom(alphaNumericSpace).custom(nonEmpty),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
        body('rssfeed').custom(urlCheck),

        body('membership').custom(membershipValidation),
        body('membershipExpiryDate').custom(dateValidation),

        body('username').custom(nonEmpty).isAlphanumeric(),
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 }),
        body('confirmPassword').isString().isLength({ min: 6 })
    ],

    adminPannelController.createGuestpert
)

router.post('/editGuestpert/profilePic',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('guestperts').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),

    adminPannelController.editGuestpertProfilePic
)

router.post('/editGuestpert/personal',
    adminAuthorize.authorize,
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').isString().custom(nonEmpty).isLength({ max: 150 }),
        body('tagLine').isString().custom(nonEmpty).isLength({ max: 2000 }),
        body('primaryExpertise').isString(),
        body('expandedExpertise').isString(),
        body('premium').custom(checkbox)
    ],
    adminPannelController.editGuestpertCommon
)

router.post('/editGuestpert/contact',
    adminAuthorize.authorize,
    [
        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('city').custom(alphaNumericSpace).custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('country').custom(alphaNumericSpace).custom(nonEmpty),
        body('zipCode').isNumeric(),
        body('state').custom(alphaNumericSpace).custom(nonEmpty),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
        body('rssfeed').custom(urlCheck),
    ],
    adminPannelController.editGuestpertCommon
)

router.post('/editGuestpert/membership',
    adminAuthorize.authorize,
    [
        body('membership').custom(membershipValidation),
        body('membershipExpiryDate').custom(dateValidation),
        body('active').custom(checkbox)
    ],
    adminPannelController.editGuestpertMembership
)

router.post('/editGuestpert/credentials',
    adminAuthorize.authorize,
    [
        body('email').isEmail(),
        body('password').isString().isLength({ min: 6 }),
        body('confirmPassword').isString().isLength({ min: 6 }).custom(checkPassword)
    ],
    adminPannelController.updateGuestpertCredentials
)

router.post('/editGuestpert/biography',
    adminAuthorize.authorize,
    [
        body('biography').isString()
    ],
    adminPannelController.updateGuestpertBio
)


//-----------------------------------//
// Route for page /frontend/home/slides-
//-----------------------------------//

router.post('/editSlides',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 409600 } // 400 KB
        }).single('image'),
    [
        body('title').custom(alphaNumericSpace).isLength({ max: 30 }),
        body('caption').isString(),
        body('videoLink').custom(urlCheck),
        body('anchor').custom(alphaNumericSpace),
        body('active').custom(checkbox)
    ],
    adminPannelController.editSlides
)


router.post('/editHomePageGuestpertsTags',
    adminAuthorize.authorize,
    [
        body('upcomingAppearanceGuestpert').custom(stringArray),
        body('upcomingAppearanceTitle').custom(stringArray)
    ],
    adminPannelController.editHomePageGuestpertsTags
)

router.post('/editCompanyTags',
    adminAuthorize.authorize,
    [
        body('companyTag').custom(stringArray)
    ],
    adminPannelController.editComapnyTags
)

router.post('/editHomePageHotTopicsTags',
    adminAuthorize.authorize,
    [
        body('hotTopicTag').custom(stringArray)
    ],
    adminPannelController.editHomePageHotTopicsTags
)


router.post('/editContactText',
    adminAuthorize.authorize,
    multer(
        {
            storage: multerSettings('contactPage').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 }
        }).single('image'),
    [
        body('content').isString().custom(nonEmpty)
    ],
    adminPannelController.updateContactText
)



//--//--//--//--//--//--//---//--//--//--//--//---------
// ----- Edit Guestpert Routes ------ //

router.get('/editGuestpertOtherFields/hotTopics',
    adminAuthorize.authorize,
    guestpertPannelController.fetchManageGuestpertHotTopicsPage
)

router.get('/editGuestpertOtherFields/media',
    adminAuthorize.authorize,
    guestpertPannelController.getManageMediaPage
)

router.get('/editGuestpertOtherFields/demoReel',
    adminAuthorize.authorize,
    guestpertPannelController.getManageDemoReelPage
)

router.get('/editGuestpertOtherFields/blogs',
    adminAuthorize.authorize,
    guestpertPannelController.getGuestpertBlogPage
)

router.get('/editGuestpertOtherFields/books',
    adminAuthorize.authorize,
    guestpertPannelController.fetchGuestpertBooksPage
)


// Subscribe Form . No authorization needed
router.post('/newsletterSubscribe',
    [
        body('email').isEmail(),
    ],
    adminPannelController.addNewsletterSubscriber
)

//--------------------------------

router.post('/editEnquiry',
    adminAuthorize.authorize,
    [
        body('media').custom(alphaNumericSpace),
        body('show').custom(alphaNumericSpace),
        body('airDate').custom(alphaNumericSpace),
        body('location').custom(alphaNumericSpace),
        body('topic').custom(alphaNumericSpace),
        body('rating').custom(numeric)
    ],

    adminPannelController.editEnquiry
)

router.post('/createEnquiry',
    adminAuthorize.authorize,
    [
        body('media').custom(alphaNumericSpace),
        body('show').custom(alphaNumericSpace),
        body('airDate').custom(alphaNumericSpace),
        body('location').custom(alphaNumericSpace),
        body('topic').custom(alphaNumericSpace),
        body('rating').custom(numeric)
    ],

    adminPannelController.createEnquiry
)


/***********Create God */
// router.get('/createGod', adminAuthorize.authorizeGodMode, godController.createGod)



module.exports = router;