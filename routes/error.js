const express = require('express');

const router = express.Router();

////////////////////////////////
//***** URL routes *****//
////////////////////////////////

router.get('/serverError', (req, res, next)=>{
    res.render('templates/error/serverError');
})

router.get('/notFound', (req, res, next)=>{
    res.render('templates/error/notFound');
})

router.get('/invalidInput', (req, res, next)=>{
    res.render('templates/error/invalidInputs')
})

module.exports = router;