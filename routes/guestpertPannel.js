const express = require('express');
const { body } = require('express-validator');
const multer = require('multer');

const multerSettings = require('../util/multer');
const { nonEmpty, alphaNumericSpace, checkbox, urlCheck, videoLinkValidator } = require('../util/validator');

const router = express.Router();

const guestpertsPannelController = require('../controllers/guestpertPannel');
const guestpertAuthorize = require('../middleware/guestpert.middleware');


////////////////////////////////
//***** REST API ROUTES *****//
////////////////////////////////

router.get('/manageBlogs/edit', guestpertAuthorize.authorizeRest, guestpertsPannelController.editGuestpertsBlogs);

router.get('/manageBooks/edit',  guestpertAuthorize.authorizeRest, guestpertsPannelController.fetchGuestpertBook);

router.get('/search/tagsList',  guestpertAuthorize.authorizeRest, guestpertsPannelController.searchTagList);

router.get('/manageHotTopics/edit',  guestpertAuthorize.authorizeRest, guestpertsPannelController.fetchAHotTopic);

router.get('/manageMedia/edit',  guestpertAuthorize.authorizeRest, guestpertsPannelController.fetchMedia);

router.get('/searchEnquiries',  guestpertAuthorize.authorizeRest, guestpertsPannelController.getEnquiries)

router.delete('/tempBlogsArr/delete',  guestpertAuthorize.authorizeRest, guestpertsPannelController.delTempBlogsArr)


////////////////////////////////
//***** URL ROUTES *****//
////////////////////////////////

router.get('/dashboard',
    guestpertAuthorize.authorize,
    guestpertAuthorize.dashboardVerification,
    guestpertsPannelController.getGuestpertDashboard
)

router.get('/profileSettings',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getGuestpertProfileSettings
)

router.get('/orders',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getOrdersPage
)

router.get('/blog',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getGuestpertBlogPage
)

router.get('/books',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.fetchGuestpertBooksPage
);

router.get('/demoReel',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getManageDemoReelPage
)

router.get('/media',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getManageMediaPage

)

router.get('/hotTopics',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.fetchManageGuestpertHotTopicsPage
)

router.get('/membership',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getMembershipPage
)

router.get('/inquiries',
    guestpertAuthorize.authorize,
    guestpertAuthorize.verification,
    guestpertsPannelController.getProducerInquiriesPage
)

router.get('/verify',
    
)


////////////////////////////////
//***** POST ROUTES *****//
////////////////////////////////
router.delete('/tempHotTopicsArr/delete',  guestpertAuthorize.authorizeRest, guestpertsPannelController.delTempArray)

router.post('/hotTopicUploadPic',
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
        {
            storage: multerSettings('hotTopics').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 307200 }
        }).single('file'),
    guestpertsPannelController.uploadHotTopicsImage
)


router.post('/editHotTopic',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('category').custom(nonEmpty).custom(alphaNumericSpace),
        body('title').custom(nonEmpty).isString(),
        body('tags').custom(alphaNumericSpace),
        body('active').custom(checkbox),
        body('openingSpeakingPoint').isString(),
        body('content').isString()
    ],
    guestpertsPannelController.editHotTopic
)

router.post('/deleteHotTopic',
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('delId').isMongoId()
    ],
    guestpertsPannelController.deleteHotTopic
)

router.post('/createHotTopic',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('category').custom(nonEmpty).custom(alphaNumericSpace),
        body('title').custom(nonEmpty).isString(),
        body('tags').custom(alphaNumericSpace),
        body('active').custom(checkbox),
        body('openingSpeakingPoint').isString(),
        body('content').isString()
    ],
    guestpertsPannelController.createHotTopic
)

//---****  Guestpert Books ****---//

router.post('/createBooks',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
        {
            storage: multerSettings('books').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 }
        }).single('image'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 200 }).escape('<', '>', '/', '\'', '"'),
        body('published').isDate(),
        body('active').custom(checkbox),
        body('description').isString()
    ],
    guestpertsPannelController.createBook
)

router.post('/editBooks',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
        {
            storage: multerSettings('books').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 204800 }
        }).single('image'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 200 }).escape('<', '>', '/', '\'', '"'),
        body('published').isDate(),
        body('active').custom(checkbox)
    ],
    guestpertsPannelController.editBook
)

router.post('/deleteBook',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    body('bookId').isMongoId(),
    guestpertsPannelController.deleteBook
)


//---****  Guestpert Blogs ****---//

// Blogs
router.post('/uploadBlogPic', 
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
    {
        storage: multerSettings('blogs').fileStorage,
        fileFilter: multerSettings().fileFilter,
        limits: { fileSize: 512000 }
    }).single('file'),
    guestpertsPannelController.uploadBlogPic
)

router.post('/createBlogs',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
        {
            storage: multerSettings('blogs').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 100 }).escape('<', '>', '/', '\'', '"'),
        body('content').isString().custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    guestpertsPannelController.createGuestpertBlogs
)

router.post('/editBlog',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    multer(
        {
            storage: multerSettings('blogs').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),
    [
        body('title').isString().custom(nonEmpty).isLength({ max: 100 }).escape('<', '>', '/', '\'', '"'),
        body('content').isString().custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    guestpertsPannelController.editGuestpertBlogs
)

router.post('/deleteBlog',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    body('blogId').isMongoId().custom(nonEmpty),
    guestpertsPannelController.deleteGuestpertBlog
)

//---**** Guestpert Profile Settings ****---//

router.post('/changeProfilePic',
    guestpertAuthorize.authorize,
    multer(
        {
            storage: multerSettings('guestperts').fileStorage,
            fileFilter: multerSettings().fileFilter,
            limits: { fileSize: 512000 }
        }).single('image'),
    guestpertsPannelController.editGuestpertProfilePic
)

router.post('/editPersonalInfo',
    guestpertAuthorize.authorize,
    [
        body('firstName').custom(alphaNumericSpace).custom(nonEmpty),
        body('lastName').custom(alphaNumericSpace).custom(nonEmpty),
        body('prefix').custom(alphaNumericSpace),
        body('suffix').custom(alphaNumericSpace),
        body('title').isString().custom(nonEmpty).isLength({ max: 150 }),
        body('tagLine').isString().custom(nonEmpty).isLength({ max: 2000 }),
        body('primaryExpertise').isString(),
        body('expandedExpertise').isString()
    ],
    guestpertsPannelController.editGuestpertCommon
)

router.post('/editContactInfo',
    guestpertAuthorize.authorize,
    [
        body('address').isString().escape('<', '>', '/', '\'', '"').custom(nonEmpty),
        body('city').custom(alphaNumericSpace).custom(nonEmpty),
        body('phone').isMobilePhone(),
        body('state').custom(alphaNumericSpace).custom(nonEmpty),
        body('website').custom(urlCheck),
        body('fax').custom(alphaNumericSpace).isString(),
        body('rssfeed').custom(urlCheck),
    ],
    guestpertsPannelController.editGuestpertCommon
)

router.post('/editCredentials',
    guestpertAuthorize.authorize,
    [
        body('oldPassword').isString().custom(nonEmpty),
        body('newPassword').isString().custom(nonEmpty),
        body('confirmPassword').isString().custom(nonEmpty),
    ],
    guestpertsPannelController.editGuestpertCredentials
)


//---**** Guestpert Demo Reels ****---//

router.post('/createDemoReel',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('title').custom(alphaNumericSpace).custom(nonEmpty),
        body('videoLink').custom(videoLinkValidator).custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    guestpertsPannelController.createDemoReel
)

router.post('/editDemoReel',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('title').custom(alphaNumericSpace).custom(nonEmpty),
        body('videoLink').custom(videoLinkValidator).custom(nonEmpty),
        body('active').custom(checkbox)
    ],
    guestpertsPannelController.editDemoReel
)

router.post('/deleteReel',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('reelId').isMongoId()
    ],
    guestpertsPannelController.deleteDemoReel
)


//---**** Media ****---//

router.post('/createMedia',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('appearanceDate').isDate(),
        body('topic').isLength({ max: 100 }).custom(alphaNumericSpace).custom(nonEmpty),
        body('title').isLength({ max: 60 }).custom(alphaNumericSpace),
        body('videoLink').custom(urlCheck),
        body('active').custom(checkbox),
        body('tags').custom(alphaNumericSpace)
    ],
    guestpertsPannelController.createMedia
)

router.post('/editMedia',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('appearanceDate').isDate(),
        body('topic').isLength({ max: 100 }).custom(alphaNumericSpace).custom(nonEmpty),
        body('title').isLength({ max: 30 }).custom(alphaNumericSpace),
        body('videoLink').custom(urlCheck),
        body('active').custom(checkbox),
        body('tags').custom(alphaNumericSpace)
    ],
    guestpertsPannelController.editMedia
)


router.post('/deleteMedia',
    guestpertAuthorize.loggedIn,
    guestpertAuthorize.AdminGuestpertauthorize,
    [
        body('delId').isMongoId()
    ],
    guestpertsPannelController.deleteMedia
)


// ---- Buy Product ---- //

router.post('/buyProduct',
    guestpertAuthorize.authorize,
    body('prodId').isMongoId(),
    guestpertsPannelController.buyProduct
)

router.get('/productPaymentSuccess/:userId/:productId',

    guestpertsPannelController.captureProdPayment
)

router.post('/buyMembership',
    guestpertAuthorize.authorize,
    body('membershipType').isAlpha(),
    guestpertsPannelController.buyMembership
)

router.get('/membershipPaymentSuccess/:userId/:productId',
    guestpertsPannelController.captureMembership
)


module.exports = router;