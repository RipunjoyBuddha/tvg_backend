const cron = require('node-cron')
const addDays = require('add-days')
const formatDate = require('dateformat')
const { delFile } = require('../util/file')
const sgMail = require('@sendgrid/mail');

const tempDb = require('../models/temp.models')
const user = require('../models/users.models')
const emailTemplate = require('../models/email.models')
const email_ids = require('../util/email')

sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);

const sendEmail = ({ email, membership, firstName, lastName }, renewalEmailTemplate, expirationDate) => {

    const msg = {
        to: email,
        from: {
            name: process.env.ADMIN_MAILING_NAME,
            email: process.env.ADMIN_SENDING_EMAIL,
        },
        subject: renewalEmailTemplate.subject,
        html: renewalEmailTemplate.emailContent
            .replace(/\[guestpertName\]/ig, `${firstName} ${lastName}`)
            .replace(/\[membershipTypeName\]/ig, `${membership}`)
            .replace(/\[dateOfExpiration\]/ig, `${formatDate(expirationDate, 'dS mmm yyyy')}`)
    }
    if(process.env.MAILAUTOMATION == 'true'){
        sgMail.send(msg)
    }
}



exports.runDaily = async () => {

    cron.schedule('0 3 * * *', async () => {

        const today = new Date()
        const yesterday = addDays(today, -1).toISOString()
        const week = addDays(today, 7).toISOString()
        const tomorrow = addDays(today, 1).toISOString()
        const threeDays = addDays(today, 3).toISOString()

        const renewalEmailTemplate = await emailTemplate.fetchAEmail(email_ids.membershipRenewNotification)

        // Delete temporary Images from temp database
        const tempArr = await tempDb.fetchLTDate(yesterday)
        tempArr.forEach(cur => {
            cur.imgArr.forEach(c => {
                delFile(c)
            })
        })
        tempDb.delLtDate(yesterday)


        // Send email to all users ending after 7 days
        const weekArr = await user.fetchGuestpertWitTime(formatDate(week, 'yyyy-mm-dd'))
        weekArr.forEach((usr) => {
            sendEmail(usr, renewalEmailTemplate, week)
        })


        // Send email to all users ending after 3 days
        const threeDaysArr = await user.fetchGuestpertWitTime(formatDate(threeDays, 'yyyy-mm-dd'))
        threeDaysArr.forEach(usr => {
            sendEmail(usr, renewalEmailTemplate, threeDays)
        })

        // Send email to all users ending after 1 day
        const tomorrowArr = await user.fetchGuestpertWitTime(formatDate(tomorrow, 'yyyy-mm-dd'))
        tomorrowArr.forEach(usr => {
            sendEmail(usr, renewalEmailTemplate, tomorrow)
        })

        // Membership expired yesterday
        const yesterdayArr = await user.fetchGuestpertWitTime(formatDate(yesterday, 'yyyy-mm-dd'))
        const expiredMembershipEmailTemplate = await emailTemplate.fetchAEmail(email_ids.membershipExpirationNotification)
        
        yesterdayArr.forEach(({ email, firstName, lastName, membership }) => {
            const msg = {
                to: email,
                from: {
                    name: process.env.ADMIN_MAILING_NAME,
                    email: process.env.ADMIN_SENDING_EMAIL,
                },
                subject: expiredMembershipEmailTemplate.subject,
                html: expiredMembershipEmailTemplate.emailContent
                    .replace(/\[guestpertName\]/ig, `${firstName} ${lastName}`)
                    .replace(/\[membershipTypeName\]/ig, `${membership}`)
            }
            if(process.env.MAILAUTOMATION == 'true'){
                sgMail.send(msg)
            }
        })
    })
}