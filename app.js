//*********** Package Imports ***********//
const path = require('path');
const url = require('url');

const express = require('express');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const dotenv = require('dotenv');
const csrf = require('csurf');
const sgMail = require('@sendgrid/mail');

const daily = require('./cron/daily')


sgMail.setApiKey(process.env.SENDGRID_API_KEY)
dotenv.config()
const mongoConnect = require('./util/database').mongoConnect;

const siteInfo = require('./models/settings.models');

const app = express();
const store = new MongoDBStore({
    uri: process.env.MONGODBURI,
    databaseName: 'tvGuestpert',
    collection: 'sessions',
    expires: 1000*60*60*24
});


store.on('error', function(error) {
    // console.log(error);
});

//*********** Setting appropriate headers for API ***********//
app.use((req, res, next) => {
    if(process.env.MODE==='development'){
        res.setHeader('Access-Control-Allow-Origin', '*'); // Domains that can send requests
    }else{
        res.setHeader('Access-Control-Allow-Origin', process.env.DOMAIN); // Domains that can send requests
    }
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type , Authorization');
    next()
})

//*********** Setting View Engine ***********// 
app.set('view engine', 'ejs');
app.set('views', 'views');


//******** Routes ********//
const adminPannelRoutes = require('./routes/adminPannel');
const guestpertPannelRoutes = require('./routes/guestpertPannel');
const producerPannelRoutes = require('./routes/producerPannel')
const frontendRoutes = require('./routes/frontEnd');
const authRoutes = require('./routes/auth');
const errorRoutes = require('./routes/error');


//******** Parse Different Files ********//
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({type: 'application/json'}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use('/css/images', express.static(path.join(__dirname, 'images')));


//******** Configuring Sessions ********//
app.use(session({
    secret: 'my secret',
    resave: false,
    saveUninitialized: false,
    store
}));
app.use(flash());
app.use(csrf());


//******** Setting up csrf ********//
app.use(async (req, res, next)=>{
    
    const site = await siteInfo.fetchSiteSettings();
    const seo = await siteInfo.fetchSeoSettings()
    
    res.locals.seoSettings = seo;
    res.locals.siteSettings = site;
    res.locals.userRole = req.session.user?req.session.user.role:null
    res.locals.csrfToken = req.csrfToken();
    res.locals.currentUrl = url.parse(req.url).path;
    next()
})

//******** Init Routes ********//
app.use(frontendRoutes);
app.use(authRoutes);
app.use('/producer', producerPannelRoutes);
app.use('/guestpert', guestpertPannelRoutes);
app.use('/admin', adminPannelRoutes);
app.use('/error', errorRoutes);

app.use((req, res, next) => {
    if(process.env.MODE === 'production'){
        res.status(404).redirect('/error/notFound')
    }
    next()
})

if(process.env.MODE === 'production'){
    app.use((error, req, res, next) => {
        res.status(500).redirect('/error/serverError');
    });
}

//******** Start Server ********//
mongoConnect(() => {
    app.listen(3000)
    
    if(process.env.MODE === 'production'){
        daily.runDaily()
    }
})